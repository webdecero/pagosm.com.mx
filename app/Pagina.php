<?php

namespace App;

//use Jenssegers\Mongodb\Model as Eloquent;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pagina extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'paginas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['clave','titulo','tituloLargo','descripcionCorta','contenido'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];



}


