<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Transaccion extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'transaccion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'referencia',
        'response',
        
        'foliocpagos',
        'cd_response',
        'cd_error',
        'time',
        'date',
        'nb_company',
        'nb_merchant',
        'nb_street',
        'tp_operation',
        'voucher',
        'voucher_comercio',
        'voucher_cliente',
        'friendly_response',
        
        'aut',
        'error',
        'ccName',
        'amount',
        'ccNum',
        'type',
        
        'precioInicial', 'precioDescuento', 'precioFinal', 'monedaInicial', 'monedaFinal', 'exchangeRate',
        'urlResponse',
        'xmla',
        'xmlResponse',
        'webpay',
        'coloquios', 'descuentos', 'isBeca', 'isVentanilla',
        'version',
		'donativo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function evento() {
        return $this->belongsTo('App\Evento');
    }

    public function pago() {
        return $this->belongsTo('App\Pago');
    }

    public function donativos() {
        return $this->hasOne('App\Donativo');
    }

}
