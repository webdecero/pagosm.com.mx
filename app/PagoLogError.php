<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PagoLogError extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'pagoLogError';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    
          
    
    protected $fillable = ['tipo','log' ,'respuesta' ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    
    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function Evento() {
        return $this->belongsTo('App\Evento');
    }

    
}
