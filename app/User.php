<?php

namespace App;

use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
//use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'users';


    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        "nombre",
        "apellidoPaterno",
        "apellidoMaterno",
        "email",
        "genero",
        "fechaNacimiento",
        "pais",
        "estado",
        "codigoPostal",
        "ciudad",
        "delegacion",
        "colonia",
        "direccion",
        "empresa",
        "sector",
        "cargo",
        "areaDesempeno",
        "claveTrabajo",
		"cenep",
        "nivelEscolar",
        "especifica",
        "donativo",
        "eventos"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getNombreCompletoAttribute($value ) {

       $nombre  = $this->attributes['nombre']  ;
       $nombre  .= ' ' .$this->attributes['apellidoPaterno']  ;
       $nombre  .= (isset($this->attributes['apellidoMaterno']))? ' ' .$this->attributes['apellidoMaterno']: '';
       
        return $nombre;
        
    }

    public function donativos() {
        return $this->hasMany('App\Donativo');
    }

    public function pagos() {
        return $this->hasMany('App\Pago');
    }

    public function pagosError() {
        return $this->hasMany('App\PagoLogError');
    }

    public function transacciones() {
        return $this->hasMany('App\Transaccion');
    }

}
