<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Donativo extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'donativos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        "nombreDonativo",
        "razonSocialDonativo",
        "emailDonativo",
        "rfcDonativo",
        "codigoPaisDonativo",
        "ladaDonativo",
        "telefonoDonativo",
        "direccionDonativo",
        "numExtDonativo",
        "numIntDonativo",
        "coloniaDonativo",
        "delegacionDonativo",
        "ciudadDonativo",
        "codigoPostalDonativo",
        "estadoDonativo",
        "paisDonativo",
        "estatus",
        "referencia",
		"tipoTarjeta",
		"usoComprobante",
		"moneda",
		"date"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function pago() {
        return $this->belongsTo('App\Pago');
    }

    public function transaccion() {
        return $this->belongsTo('App\Transaccion');
    }
    
    public function evento() {
        return $this->belongsTo('App\Evento');
    }

}
