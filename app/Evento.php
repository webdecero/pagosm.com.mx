<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDate;

class Evento extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'eventos';
	protected $dates = array('streamingFechaInicio', 'streamingFechaFinal');
	protected $casts = [
		'streamingActiva' => 'boolean',
		'orden' => 'integer',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'titulo',
		'tituloLargo',
		'descripcionCorta',
		'descripcion',
		'ubicacion',
		'codigo',
		'slug',
		'url',
		'email',
		'imagen',
		'precio',
		'moneda',
		'estatus',
		'limite',
		'fechaInicio',
		'fechaFinal',
		'instrucciones',
		'comprobante',
		'imagenComprobante',
		'categoria',
		'orden',
		'streamingActiva',
		'streaming',
		'streamingFechaInicio',
		'streamingFechaFinal',
		'semilla'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function pagos() {
		return $this->hasMany('App\Pago');
	}

	public function pagosError() {
		return $this->hasMany('App\PagoLogError');
	}

	public function descuentoPais() {
		return $this->hasMany('App\DescuentoPais');
	}

	public function descuentoFecha() {
		return $this->hasMany('App\DescuentoFecha');
	}

	public function descuentoCodigo() {
		return $this->hasMany('App\DescuentoCodigo');
	}

	public function coloquios() {
		return $this->hasMany('App\Coloquio');
	}

	public function transacciones() {
		return $this->hasMany('App\Transaccion');
	}

	public function donativos() {
		return $this->hasMany('App\Donativo');
	}

	public function setEstatusAttribute($value) {

		if ($value == TRUE || $value == 'true' || $value == 'TRUE' || $value == 1 || $value == '1') {
			$this->attributes['estatus'] = TRUE;
		} else {
			$this->attributes['estatus'] = FALSE;
		}
	}

	public function setFechaInicioAttribute($value) {



		$this->attributes['fechaInicio'] = new MongoDate(strtotime($value));
	}

	public function setFechaFinalAttribute($value) {


		$this->attributes['fechaFinal'] = new MongoDate(strtotime($value));
	}

	public function fechaInicioFormato($patern = 'Y-m-d H:i') {


		return date($patern, $this->attributes['fechaInicio']->sec);
	}

	public function fechaFinalFormato($patern = 'Y-m-d H:i') {



		return date($patern, $this->attributes['fechaFinal']->sec);
	}

	public function setStreamingActivaAttribute($value) {

		if ($value === TRUE || $value === 'true' || $value === 'TRUE' || $value === 1 || $value === '1') {
			$this->attributes['streamingActiva'] = TRUE;
		} else {
			$this->attributes['streamingActiva'] = FALSE;
		}
	}

	public function setStreamingFechaInicioAttribute($value) {



		$this->attributes['streamingFechaInicio'] = new MongoDate(strtotime($value));
	}

	public function setStreamingFechaFinalAttribute($value) {


		$this->attributes['streamingFechaFinal'] = new MongoDate(strtotime($value));
	}

}
