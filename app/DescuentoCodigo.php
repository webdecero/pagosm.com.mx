<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDate;

class DescuentoCodigo extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'descuentoCodigo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    
          
    protected $dates = array('fechaInicio', 'fechaFinal');
    
    protected $fillable = ['titulo', 'porcentaje', 'fechaInicio', 'fechaFinal', 'codigo', 'aplicado', 'tipo', 'descripcion' ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    
      public function setAplicadoAttribute($value) {

        if ($value == TRUE || $value == 'true' || $value == 'TRUE' || $value == 1 || $value == '1') {
            
            $this->attributes['aplicado'] = TRUE;
        } else {
            $this->attributes['aplicado'] = FALSE;
        }
    }
    
    
    

    public function setFechaInicioAttribute($value) {



        $this->attributes['fechaInicio'] = new MongoDate(strtotime($value));
    }

    public function setFechaFinalAttribute($value) {


        $this->attributes['fechaFinal'] = new MongoDate(strtotime($value));
    }
    
    
    public function evento() {
        return $this->belongsTo('App\Evento');
    }

    
    public function user() {
        return $this->belongsTo('App\User');
    }
    

    
}
