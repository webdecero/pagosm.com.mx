<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDate;
class DescuentoFecha extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'descuentoFecha';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    
    protected $dates = array('fechaInicio', 'fechaFinal');
    
    protected $fillable = ['titulo', 'porcentaje', 'fechaInicio', 'fechaFinal', 'descripcion', 'estatus', 'tipo' ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    
    
    public function evento() {
        return $this->belongsTo('App\Evento');
    }

    
     
    
    public function setEstatusAttribute($value) {

        if ($value == TRUE || $value == 'true' || $value == 'TRUE' || $value == 1 || $value == '1') {
            $this->attributes['estatus'] = TRUE;
        } else {
            $this->attributes['estatus'] = FALSE;
        }
    }
    
    
    

    public function setFechaInicioAttribute($value) {



        $this->attributes['fechaInicio'] = new MongoDate(strtotime($value));
    }

    public function setFechaFinalAttribute($value) {


        $this->attributes['fechaFinal'] = new MongoDate(strtotime($value));
    }
    
    
    
}
