<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use MongoDate;



class Coloquio extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'coloquio';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['titulo', 'fechaDiaTexto', 'fechaDia', 'fechaInicio', 'fechaFinal', 'capacidad', 'lugar', 'descripcion', 'estatus'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Eloquent: Mutators
     * @see https://laravel.com/docs/5.2/eloquent-mutators
     *
     * @param mixed $value Fomato a asignacion de parametros
     */
    public function setEstatusAttribute($value) {

        if ($value == TRUE || $value == 'true' || $value == 'TRUE' || $value == 1 || $value == '1') {
            $this->attributes['estatus'] = TRUE;
        } else {
            $this->attributes['estatus'] = FALSE;
        }
    }
    
        public function setFechaDiaAttribute($value) {


        $this->attributes['fechaDiaTexto'] = $value;
        $this->attributes['fechaDia'] = new MongoDate(strtotime($value));
    }
    

    public function setFechaInicioAttribute($value) {

        $value = $this->attributes['fechaDia']->toDateTime()->format("d-m-Y") . ' ' . $value;


        $this->attributes['fechaInicio'] = new MongoDate(strtotime($value));
    }

    public function setFechaFinalAttribute($value) {

        $value = $this->attributes['fechaDia']->toDateTime()->format("d-m-Y") . ' ' . $value;
        $this->attributes['fechaFinal'] = new MongoDate(strtotime($value));
    }



    public function evento() {
        return $this->belongsTo('App\Evento');
    }
    
    
    
    
    public static function getFechasColoquio($idEvento = null) {
        
        
        $query = [

            
            [
                '$match' => [
                      'estatus'=>true,
                      'evento_id'=>$idEvento
                ]
            ],
            [
                '$group' => [
                    '_id' => '$fechaInicio',
                    'coloquios' => [
                        '$push' => [
                              '_id' => '$_id',
                              'titulo' => '$titulo',
                              'lugar' => '$lugar',
                              'fechaInicio' => '$fechaInicio'
                        ]
                    ]
                ]
            ],
             [
                '$sort' => [
                      '_id'=>1
                     
                ]
            ]
        ];

        if ($idEvento == NULL) {
            unset($query['$match']['evento_id']);
        }

        $coloquios = self::raw(function($collection) use ($query) {
                    return $collection->aggregate($query);
                });


        return $coloquios["result"];
    }
    
    
      public function fechaInicioFormato($patern = 'Y-m-d H:i') {


        return date($patern, $this->attributes['fechaInicio']->sec);
    }


    public function fechaFinalFormato($patern = 'Y-m-d H:i') {


        
        return date($patern, $this->attributes['fechaFinal']->sec);
    }

   
    
}
