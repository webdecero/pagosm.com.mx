<?php

namespace App\Http\Middleware;

use Closure;
//Providers
use Auth;

class ValideEditRoute {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        $user = Auth::user();
        if ($user->rol !='editor' && $user->rol !='admin') {
            return redirect()->route("usuario.panel")->with([
                        'error' => trans('mensajes.session.privilegios'),
                    ]);
        }



        return $next($request);
    }

}
