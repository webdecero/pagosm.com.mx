<?php

namespace App\Http\Controllers\Currencylayer;

use Config;
use HttpClient;
use Storage;

class Currencylayer {

    public function __construct() {
        $this->url = (Config::has('currencylayer.url')) ? Config::get('currencylayer.url') : null;

        $this->access_key = (Config::has('currencylayer.access_key')) ? Config::get('currencylayer.access_key') : null;

        $this->currencies = (Config::has('currencylayer.currencies')) ? Config::get('currencylayer.currencies') : null;

        $this->format = (Config::has('currencylayer.format')) ? Config::get('currencylayer.format') : null;


        $this->fileData = 'currencylayer.txt';
    }

    public function live($currencies = 'MXN') {
       

        $request = $this->_createRest('live', [
            'currencies' => $currencies
        ]);

        $content = $this->_lazyReuqest($request);
        
        $response = json_decode($content, true);


        return $response;
    }

    private function _createRest($method, $params = []) {


        $request['url'] = $this->url . $method;
        $request['params'] = $params;
        $request['params']['access_key'] = $this->access_key;
        $request['params']['format'] = $this->format;

        return $request;
    }

    public function convertUSDMXN($to = 1) {

        $currencies = 'MXN';

        $response = $this->live($currencies);
        
        
        return $response['quotes']['USDMXN'] * $to;
    }

    private function _lazyReuqest($request) {

        $fileData = $this->fileData;
        $content = null;
        
        $content = Storage::get($fileData);

        if (Storage::exists($fileData)) {

            $filelastModified = Storage::lastModified($fileData);

            $current_time = time();

            if (( $current_time - $filelastModified) > (60 * 60)) {
                $response = HttpClient::get($request);
                $content = $response->content();
                Storage::put($fileData, $response->content());
            }else{
                $content = Storage::get($fileData);
            }
        } else {
            $response = HttpClient::get($request);
            $content = $response->content();
            Storage::put($fileData, $response->content());
            
            
        }


        return $content;
    }

}
