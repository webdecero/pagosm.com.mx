<?php

namespace App\Http\Controllers;

use MongoRegex;

class Utilidades {

    public static function sortBySubkey(&$array, $subkey, $sortType = SORT_ASC) {
        foreach ($array as $subarray) {
            $keys[] = $subarray[$subkey];
        }

        array_multisort($keys, $sortType, $array);
    }

    public static function slug($string, $slug = '-', $maxlength = 1024) {
        $stringStrip = strip_tags($string);
        $seo = strtolower(trim(preg_replace('~[^0-9a-z]+~i', $slug, html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($stringStrip, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), $slug));

        if (strlen($seo) > $maxlength) {
            $seo = substr($seo, 0, $maxlength);
            $pos = (int) strrpos($seo, $slug); //ultima posicion de la busqueda
            if ($pos) {
                $seo = substr($seo, 0, $pos);
            }
        }

        return $seo;
    }

    public static function explodeTags($stringTags, $delimiter = ",", $character_mask = " \t\n\r\0\x0B,.") {

        $temp = explode($delimiter, trim($stringTags, $character_mask));

        foreach ($temp as $key => $value) {

            $temp[$key] = trim($value, $character_mask);
        }
        return  array_unique($temp);
    }
    
    public static function generateQuery($inputs = [], $searchable = [], $root = true) {

        $query = [];

        foreach ($inputs as $input => $value) {

            if (array_key_exists($input, $searchable) && $value) {

                if (is_array($searchable[$input])) {
                    $inputsTemp = [];

                    foreach ($searchable[$input] as $k => $v) {
                        $inputsTemp[$k] = $value;
                    }

                    $query['$or'][] = self::generateQuery($inputsTemp, $searchable[$input], false);
                } else if ($searchable[$input] == 'contains') {

                    if ($root) {
                        $query[$input] = [

                            '$regex' => new MongoRegex("/" . $value . "/i")
                        ];
                    } else {
                        $query['$or'][][$input] = [

                            '$regex' => new MongoRegex("/" . $value . "/i")
                        ];
                    }
                } else if ($searchable[$input] == 'equal') {
                   

                    if (in_array($value, ['true', 'false'])) {
                        
                        $query[$input] = ($value == 'true' ) ? true : false;
                    }else if (is_array($value)){
                        
                        $query[$input]['$in'] = $value;
                        
                    } else {

                        $query[$input] = (is_numeric($value)) ? (float) $value : $value;
                    }
                }
            }
        }

        return $query;
    }

}
