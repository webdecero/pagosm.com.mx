<?php

namespace App\Http\Controllers\Manager;

//Providers
use Auth;
use MongoId;
use Excel;
use DB;
//use DatePeriod;
//Models
use App\Donativo;
use App\Evento;
use App\Pago;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;

class DonativoController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        'referencia' => 'referencia',
        'agrupacion' => 'agrupacion',
        'numero_pedido_cliente' => 'numero_pedido_cliente',
        'solicitante' => 'solicitante',
        'nombreDonativo' => 'nombre_completo',
        'razonSocialDonativo' => 'razon_social',
        'direccionDonativo' => 'calle',
        'coloniaDonativo' => 'distrito (Colonia o Barrio)',
        'numExtDonativo' => 'numero',
        'numIntDonativo' => 'numero Interno ',
        'codigoPostalDonativo' => 'cp',
        'delegacionDonativo' => 'poblacion (Delegación o Municipio)',
        'rfcDonativo' => 'rfc',
        'zona_transporte' => 'zona_transporte',
        'paisDonativo' => 'pais',
        'estadoDonativo' => 'region (Estado o Departamento)',
        'emailDonativo' => 'email',
        'material' => 'material',
        'cantidad' => 'cantidad',
        'descuento' => 'descuento',
        'motivo_pedido' => 'motivo_pedido',
        'dest_mercaderia' => 'dest_mercaderia',
        'resp_consigna' => 'resp_consigna',
        'condicion_pago' => 'condicion_pago',
        'via_pago' => 'via_pago',
        'ciudadDonativo' => 'ciudad',
        'codigoPaisDonativo' => 'codigo_pais',
        'ladaDonativo' => 'lada',
        'telefonoDonativo' => 'telefono',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
        'usoComprobante' => 'Uso Comprobante',
        'tipoTarjeta' => 'Tipo Tarjeta',
        'moneda' => 'Moneda',
        'date' => 'Fecha de pago',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'referencia' => 'referencia',
        'agrupacion' => 'agrupacion',
        'numero_pedido_cliente' => 'numero_pedido_cliente',
        'solicitante' => 'solicitante',
        'nombreDonativo' => 'nombre_completo',
        'razonSocialDonativo' => 'razon_social',
        'direccionDonativo' => 'calle',
        'coloniaDonativo' => 'distrito (Colonia o Barrio)',
        'numExtDonativo' => 'numero',
        'numIntDonativo' => 'numero Interno ',
        'codigoPostalDonativo' => 'cp',
        'delegacionDonativo' => 'poblacion (Delegación o Municipio)',
        'rfcDonativo' => 'rfc',
        'zona_transporte' => 'zona_transporte',
        'paisDonativo' => 'pais',
        'estadoDonativo' => 'region (Estado o Departamento)',
        'emailDonativo' => 'email',
        'material' => 'material',
        'cantidad' => 'cantidad',
        'descuento' => 'descuento',
        'motivo_pedido' => 'motivo_pedido',
        'dest_mercaderia' => 'dest_mercaderia',
        'resp_consigna' => 'resp_consigna',
        'condicion_pago' => 'condicion_pago',
        'via_pago' => 'via_pago',
        'ciudadDonativo' => 'ciudad',
        'codigoPaisDonativo' => 'codigo_pais',
        'ladaDonativo' => 'lada',
        'telefonoDonativo' => 'telefono',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
        'usoComprobante' => 'Uso Comprobante',
        'tipoTarjeta' => 'Tipo Tarjeta',
        'moneda' => 'Moneda',
        'date' => 'Fecha de pago',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTable.donativo', [$item['_id']]);
        } else if ($field == 'numero_pedido_cliente') {

            $record = 'PEDIDO+' . $item['agrupacion'];
        } else if ($field == 'solicitante' || $field == 'resp_consigna') {

            $record = $item['evento']->codigo;
        } else if ($field == 'zona_transporte') {

            $record = 'MX031';
        } else if ($field == 'material') {

            $record = '834787';
        } else if ($field == 'cantidad') {

            $record = 1;
        } else if ($field == 'motivo_pedido') {

            $record = 'MVP';
        } else if ($field == 'dest_mercaderia') {

            $record = NULL;
        } else if ($field == 'condicion_pago') {

            $record = 'XP00';
        } else if ($field == 'via_pago') {


            if ($item['pago']->isVentanilla) {

                $record = "Ventanilla";
            } else {
                $record = "OnLine";
            }
        } else if ($field == 'descuento') {

            $record = 0;
        }














        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'referencia' => 'contains',
                'nombreDonativo' => 'contains',
                'razonSocialDonativo' => 'contains',
                'emailDonativo' => 'contains',
                'rfcDonativo' => 'contains'
            ],
            'evento_id' => 'equal',
        ];



        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'donativos');





        foreach ($cursor as $item) {

            $record = [];

            $item['evento'] = Evento::find($item['evento_id']);
            $item['pago'] = Pago::find($item['pago_id']);


            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'donativos');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_donativo_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Donativo', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'donativos') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            $item['evento'] = Evento::find($item['evento_id']);
            $item['pago'] = Pago::find($item['pago_id']);

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = Evento::all();



        return view('admin.donativoPanel', $data);
    }

    public function scriptPostventanilla() {



//        $pagos = Pago::where('isVentanilla', true)->where('response', "approved")->where('donativo', 'exists', true)->get()->toArray();
//
//
//        $refsPagos = [];
//        foreach ($pagos as $pago) {
//            $refsPagos[] = $pago['referencia'];
//        }

        $fecha = new \DateTime('2018-01-11T22:45:00.000+0000');

        $fechalimit = new \DateTime('2018-01-12T00:43:50.901+0000');
        $cursor = Donativo::where('created_at', '>=', $fecha)->where('created_at', '<', $fechalimit)->get();



        $data = [];


        foreach ($cursor as $item) {



            $fecha= Pago::find($item['pago_id'])->created_at->toDateTimeString() ;
            
            
        
            
            
           $item->created_at =  new \MongoDate(strtotime($fecha));
           
           $item->updated_at =  new \MongoDate(strtotime($fecha));
           
           
           $item->save();
           
           
            
//            $item['evento'] = Evento::find($item['evento_id'])->codigo;
//            $item['pago'] = Pago::find($item['pago_id'])->id;




            $data[] = $item;
        }


dd($data);

        $fileName = 'reporte_conciliacion_' . date('Y_m_d_H_i_s');

        Excel::create($fileName, function($excel) use($data) {

            $excel->sheet('Donativos', function($sheet) use($data) {



                $sheet->fromArray($data, null, 'A1', TRUE, FALSE);
            });
        })->export('xls');





        echo 'exito';
    }

}
