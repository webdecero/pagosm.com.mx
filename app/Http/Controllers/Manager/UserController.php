<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use App;
use Auth;
use MongoId;
use Excel;
use DB;
use File;
use ImageInter;
//Models
use App\User;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;

class UserController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        "nombre" => "nombre",
        "apellidoPaterno" => "apellidoPaterno",
        "apellidoMaterno" => "apellidoMaterno",
        "email" => "email",
        "genero" => "genero",
        "fechaNacimiento" => "fechaNacimiento",
        "pais" => "pais",
        "estado" => "estado",
        "codigoPostal" => "codigoPostal",
        "ciudad" => "ciudad",
        "delegacion" => "delegacion",
        "colonia" => "colonia",
        "direccion" => "direccion",
        "empresa" => "empresa",
        "sector" => "sector",
        "cargo" => "cargo",
        "areaDesempeno" => "areaDesempeno",
        "claveTrabajo" => "claveTrabajo",
        "nivelEscolar" => "nivelEscolar",
        "especifica" => "especifica",
        'rol' => 'Rol',
        'estatus' => 'Estatus',
        'lang' => 'Idioma',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        "nombre" => "nombre",
        "apellidoPaterno" => "apellidoPaterno",
        "apellidoMaterno" => "apellidoMaterno",
        "email" => "email",
        "genero" => "genero",
        "fechaNacimiento" => "fechaNacimiento",
        "pais" => "pais",
        "estado" => "estado",
        "codigoPostal" => "codigoPostal",
        "ciudad" => "ciudad",
        "delegacion" => "delegacion",
        "colonia" => "colonia",
        "direccion" => "direccion",
        "empresa" => "empresa",
        "sector" => "sector",
        "cargo" => "cargo",
        "areaDesempeno" => "areaDesempeno",
        "claveTrabajo" => "claveTrabajo",
        "nivelEscolar" => "nivelEscolar",
        "especifica" => "especifica",
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'estatus') {

            $record = ($item[$field]) ? 'Activo' : 'Inactivo';
        } else if ($field == 'opciones') {



            $record = "<a href=" . route('usuario.formEdit', [$item['_id']->{'$id'}]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            if ($item['estatus']) {
                $record .= "<a href=" . route('usuario.elimina', [$item['_id']->{'$id'}]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {
                $record .= "<a href=" . route('usuario.elimina', [$item['_id']->{'$id'}]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Recuperar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTableUsuarios(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'nombre' => 'contains',
                'apellidoPaterno' => 'contains',
                'apellidoMaterno' => 'contains',
                'email' => 'contains',
                'pais' => 'contains'
            ],
            'rol' => 'equal',
            'estatus' => 'equal',
        ];

        $query = [];

        $data = [];

        $inputs = $request->all();




        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];



        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'users');

        foreach ($cursor as $item) {

            $record = [];

            if ($excel) {


                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'users');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {


//             dd($data);
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_usuarios_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Usuarios', function($sheet) use($data, $headersExcel) {
                    // Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'users') {

        $query = [

            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function viewLogin() {

        $data = array();


        return view('admin.login', $data);
    }

    public function postLogin(Request $request) {

        $rules = array(
            'email' => array('required', 'email'),
            'password' => array('required', 'min:6'),
        );

        $input = $request->all();

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {


            return redirect()->route("home")->withErrors($validator)->with([
                        'error' => trans('mensajes.usuario.noEncontrado')
                    ])->withInput($request->except('password'));
        } else {

            $user = array(
                'email' => $input['email'],
                'password' => $input['password'],
                'estatus' => TRUE,
                'rol' => 'admin'
            );


            $checkRecord = FALSE;
            if (isset($input['recuerdame'])) {
                $checkRecord = TRUE;
            }

            if (Auth::attempt($user, $checkRecord)) {


                return redirect()->route('usuario.panel');
            } else {

                return redirect()->route("home")->withErrors($validator)->with([
                            'error' => trans('mensajes.usuario.noEncontrado')
                        ])->withInput($request->except('password'));
            }
        }
    }

    public function getLogout() {
        Auth::logout();
        return redirect()->route('home');
    }

    public function getUserPanel() {

        $data = array();

        $data['user'] = Auth::user();

        return view('admin.userPanel', $data);
    }

    public function getAddUserForm() {

        $data = array();

        $data['user'] = Auth::user();

        return view('admin.userAddForm', $data);
    }

    public function postGuarda(Request $request) {
        $input = $request->all();
        $rules = array(
            'foto' => array('required'),
            'nombre' => array('required'),
            'email' => array('required', 'email', 'unique:users,email'),
            'password' => array('required', 'min:6'),
            'rol' => array('required'),
            'genero' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("usuario.form")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {




            if (!empty($input['foto'])) {


                $file = $request->file('foto');


                if (!$request->file('foto')->isValid()) {
                    return redirect()->route("usuario.form")->withErrors($validator)->with([
                                'error' => trans('mensajes.registro.incompleto')
                    ]);
                }


                $folder = "fotos";
                $format = ".jpg";
                $nameFile = uniqid();

                $originalFoto = $folder . DIRECTORY_SEPARATOR . $nameFile . $format;
                $thumbFoto = $folder . DIRECTORY_SEPARATOR . $nameFile . '_t' . $format;




                $img = ImageInter::make($file);

                $img->widen(450)->encode('jpg', 90)->save($originalFoto);



                $img = ImageInter::make($file);

                $img->heighten(100)->crop(100, 100)->encode('jpg', 90)->save($thumbFoto);

                if (!File::exists($originalFoto)) {
                    return redirect()->route("usuario.form")->withErrors($validator)->with([
                                'error' => "Carga de imagen Incompleta"
                    ]);
                }
            }



            $user = new User;
            $user->nombre = $input['nombre'];
            $user->email = $input['email'];
            $user->password = bcrypt($input['password']);
            $user->rol = $input['rol'];
            $user->genero = $input['genero'];

            $user->descripcion = $input['descripcion'];

            $user->estatus = TRUE;

            $user->manager = TRUE;


            if (!empty($input['foto'])) {


                $user->originalFoto = $originalFoto;
                $user->thumbFoto = $thumbFoto;
            }
            $user->lang = App::getLocale();


            $user->HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];


            $user->ip = $request->ip();




            $user->save();

            return redirect()->route("usuario.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getEditUserForm($id = false) {


        $isValid = MongoId::isValid($id);
        $record = User::find($id);

        if (!$isValid || !isset($record->_id)) {
            return redirect()->route("usuario.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        } else {

            $data = array();
            $data['user'] = Auth::user();
            $data['_id'] = $id;
            $data['record'] = $record;

            return view('admin.userEditForm', $data);
        }


        $data = array();

        $data['user'] = Auth::user();

        return view('admin.userEditForm', $data);
    }

    public function postEdit(Request $request) {
        $input = $request->all();
        $rules = array(
            '_id' => array('required'),
            'nombre' => array('required'),
            'password' => array('min:6'),
            'rol' => array('required'),
            'genero' => array('required'),
            'estatus' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("usuario.panel")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
            ]);
        } else {


            $isValid = MongoId::isValid($input['_id']);
            $user = User::find($input['_id']);

            if (!$isValid || !isset($user->_id)) {
                return redirect()->route("usuario.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }




            if (!empty($input['foto'])) {


                $file = $request->file('foto');

                if (!$request->file('foto')->isValid()) {
                    return redirect()->route("usuario.panel")->withErrors($validator)->with([
                                'error' => trans('mensajes.registro.incompleto')
                    ]);
                }


                $folder = "fotos";
                $format = ".jpg";
                $nameFile = uniqid();

                $originalFoto = $folder . DIRECTORY_SEPARATOR . $nameFile . $format;
                $thumbFoto = $folder . DIRECTORY_SEPARATOR . $nameFile . '_t' . $format;




                $img = ImageInter::make($file);

                $img->widen(450)->encode('jpg', 90)->save($originalFoto);



                $img = ImageInter::make($file);

                $img->heighten(100)->crop(100, 100)->encode('jpg', 90)->save($thumbFoto);

                if (!File::exists($originalFoto)) {
                    return redirect()->route("usuario.panel")->withErrors($validator)->with([
                                'error' => "Carga de imagen Incompleta"
                    ]);
                }

                $user->originalFoto = $originalFoto;
                $user->thumbFoto = $thumbFoto;
            }





            $user->nombre = $input['nombre'];
            if (!empty($input['password'])) {

                $user->password = bcrypt($input['password']);
            }


            $user->manager = TRUE;


            $user->rol = $input['rol'];
            $user->genero = $input['genero'];

            $user->descripcion = $input['descripcion'];

            $estatus = ($input['estatus']) ? TRUE : FALSE;

            $user->estatus = $estatus;

            $user->lang = App::getLocale();


            $user->HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];


            $user->ip = $request->ip();


            $user->save();

            return redirect()->route("usuario.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getDelete($id = false) {

        $authUser = Auth::user();

        $isValid = MongoId::isValid($id);
        $user = User::findOrFail($id);

        if (!$isValid || !isset($user->_id) || $authUser->_id == $user->_id) {
            return redirect()->route("usuario.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($user->estatus) {

            $user->estatus = FALSE;
        } else {
            $user->estatus = TRUE;
        }



        $user->save();

        return redirect()->route("usuario.panel")->with([
                    'mensaje' => trans('mensajes.operacion.correcta'),
        ]);
    }

    public function getPoliticaView() {


        $data['user'] = Auth::user();



        return view('front.panelPoliticas', $data);
    }

    public function postAceptaPolitica(Request $request) {
        $input = $request->all();
        $rules = array(
            '_id' => array('required'),
            'politica' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("politica.panel")->withErrors($validator)->with([
                        'error' => trans('mensajes.operacion.incorrecta'),
            ]);
        } else {


            $isValid = MongoId::isValid($input['_id']);
            $user = User::find($input['_id']);

            if (!$isValid || !isset($user->_id)) {
                return redirect()->route("politica.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }

            $user->politica = true;



            $user->save();

            return redirect()->route("usuario.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

}
