<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use App;
use Auth;
use MongoId;
use Excel;
use DB;
use DateInterval;
//use DatePeriod;
//Models
use App\Coloquio;
use App\Evento;
use App\Pago;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\DateRange;

class ColoquioController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'Título',
        'descripcion' => 'Descripción',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'capacidad' => 'capacidad',
        'numPersonas'=> 'Numero de personas registradas',
        'lugar' => 'lugar',
        'estatus' => 'estatus',
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'titulo' => 'Título',
        'descripcion' => 'Descripción',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'capacidad' => 'capacidad',
        'numPersonas'=> 'Numero de personas registradas',
        'lugar' => 'lugar',
        'estatus' => 'estatus',
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTableColoquio', [$item['_id']->{'$id'}]);
        } else if ($field == 'evento') {


            $evento = Evento::find($item['evento_id']);

            $record = $evento->titulo;
        }else if ($field == 'numPersonas') {


            $record = Pago::whereIn('coloquios',[$item['_id']->{'$id'}])->count();

            
        } else if ($field == 'opciones') {



            $record = "<a href=" . route('coloquio.formEdit', [$item['_id']->{'$id'}]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            if ($item['estatus']) {
                $record .= "<a href=" . route('coloquio.elimina', [$item['_id']->{'$id'}]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {
                $record .= "<a href=" . route('coloquio.elimina', [$item['_id']->{'$id'}]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Recuperar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTableColoquio(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'titulo' => 'contains',
                'descripcion' => 'contains',
                'lugar' => 'contains',
            ],
            'estatus' => 'equal',
            'evento_id' => 'equal',
        ];

        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'coloquio');





        foreach ($cursor as $item) {

            $record = [];

            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'coloquio');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_coloquio_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Coloquio', function($sheet) use($data, $headersExcel) {
                    // Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'coloquio') {

        $query = [

            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getColoquioPanel() {

        $data = array();


        $data['eventos'] = Evento::all();
        $data['user'] = Auth::user();




        return view('admin.coloquioPanel', $data);
    }

    public function getAddColoquioForm($eventoId) {



        $rules = array(
            'eventoId' => array('required', 'exists:eventos,_id'),
        );


        $validator = Validator::make(['eventoId' => $eventoId], $rules);

        if ($validator->fails()) {
            return redirect()->route("coloquio.panel")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
            ]);
        }




        $data = array();



        $evento = Evento::find($eventoId);
        $data['user'] = Auth::user();



        $evento->rangoDateTime = new DateRange($evento->fechaInicio->toDateTime(), $evento->fechaFinal->toDateTime(), DateInterval::createFromDateString("+1 day"));

        $data['evento'] = $evento;
        return view('admin.coloquioAddForm', $data);
    }

    public function postGuarda(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required', 'unique:coloquio,titulo'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'capacidad' => array('required'),
            'lugar' => array('required'),
            'descripcion' => array('required'),
            'eventoId' => array('required', 'exists:eventos,_id'),
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("coloquio.form")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {


            $evento = Evento::findOrFail($input['eventoId']);


            $coloquio = new Coloquio;

            $coloquio = $this->_storeColoquio($request, $coloquio);

            $evento->coloquios()->save($coloquio);


            return redirect()->route("coloquio.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getEditColoquioForm($id = false) {


        $coloquio = Coloquio::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($coloquio->_id)) {
            return redirect()->route("coloquio.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();
        $data['_id'] = $id;



        $evento = $coloquio->evento()->first();



        $evento->rangoDateTime = new DateRange($evento->fechaInicio->toDateTime(), $evento->fechaFinal->toDateTime(), DateInterval::createFromDateString("+1 day"));


        $data['evento'] = $evento;

        $data['coloquio'] = $coloquio;

        return view('admin.coloquioEditForm', $data);
    }

    public function postEdit(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'capacidad' => array('required'),
            'lugar' => array('required'),
            'descripcion' => array('required'),
            'eventoId' => array('required', 'exists:eventos,_id'),
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("coloquio.formEdit", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $coloquio = Coloquio::find($input['_id']);

            if (!$isValid || !isset($coloquio->_id)) {
                return redirect()->route("coloquio.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeColoquio($request, $coloquio);


            return redirect()->route("coloquio.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getDelete($id = false) {


        $isValid = MongoId::isValid($id);
        $coloquio = Coloquio::findOrFail($id);

        if (!$isValid || !isset($coloquio->_id)) {
            return redirect()->route("coloquio.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($coloquio->estatus) {

            $coloquio->estatus = FALSE;
        } else {
            $coloquio->estatus = TRUE;
        }
        $coloquio->save();

//        $coloquio->delete();

        return redirect()->route("coloquio.panel")->with([
                    'mensaje' => trans('mensajes.operacion.correcta'),
        ]);
    }

//    Utilities


    private function _storeColoquio(Request $request, $coloquio) {


        $input = $request->all();



        $coloquio->fill($input);



        $coloquio->save();



        return $coloquio;
    }

}
