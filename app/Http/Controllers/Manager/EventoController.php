<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
use MongoId;
use Excel;
use DB;
//use DatePeriod;
//Models
use App\Evento;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;

class EventoController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'titulo',
        'tituloLargo' => 'tituloLargo',
        'descripcionCorta' => 'descripcionCorta',
        'descripcion' => 'descripcion',
        'ubicacion' => 'ubicacion',
        'codigo' => 'codigo',
        'slug' => 'slug',
        'url' => 'url',
        'email' => 'email',
        'imagen' => 'imagen',
        'precio' => 'precio',
        'moneda' => 'moneda',
        'estatus' => 'estatus',
        'limite' => 'limite',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'instrucciones' => 'instrucciones',
        'comprobante' => 'comprobante',
        'imagenComprobante' => 'imagenComprobante',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'titulo' => 'titulo',
        'tituloLargo' => 'tituloLargo',
        'descripcionCorta' => 'descripcionCorta',
        'descripcion' => 'descripcion',
        'ubicacion' => 'ubicacion',
        'codigo' => 'codigo',
        'slug' => 'slug',
        'url' => 'url',
        'email' => 'email',
        'imagen' => 'imagen',
        'precio' => 'precio',
        'moneda' => 'moneda',
        'estatus' => 'estatus',
        'limite' => 'limite',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTableEvento', [$item['_id']]);
        }else if ($field == 'nuevoColoquio') {


            
            
             $record = "<a href=" . route('coloquio.form', ['eventoId' => (string)$item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Nuevo coloquio' > <i class='fa fa-plus'></i> </a>";
            
            
            
        } else if ($field == 'opciones') {



            $record = "<a href=" . route('evento.formEdit', [$item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            if ($item['estatus']) {
                $record .= "<a href=" . route('evento.elimina', [$item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {
                $record .= "<a href=" . route('evento.elimina', [$item['_id']]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Recuperar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'titulo' => 'contains',
                'descripcion' => 'contains',
                'descripcionCorta' => 'contains',
                'tituloLargo' => 'contains',
                'lugar' => 'contains',
                'codigo' => 'contains',
            ],
            'estatus' => 'equal',
            'moneda' => 'equal',
            'categoria' => 'equal',
        ];




        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'eventos');





        foreach ($cursor as $item) {

            $record = [];

            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'eventos');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_evento_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Evento', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'eventos') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();
  
		
		$data['categorias'] =  Evento::groupBy('categoria')->get(['categoria'])->toArray();

		
        $data['user'] = Auth::user();




        return view('admin.eventoPanel', $data);
    }

    public function getAddForm() {




        $data = array();


        $data['user'] = Auth::user();


        return view('admin.eventoAddForm', $data);
    }

    public function postGuarda(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required', 'unique:eventos,titulo'),
            'tituloLargo' => array('required', 'unique:eventos,tituloLargo'),
            'descripcionCorta' => array('required'),
            'descripcion' => array('required'),
            'codigo' => array('required', 'unique:eventos,codigo'),
            'imagen' => array('required'),
            'precio' => array('required'),
			 'categoria' => array('required'),
            'orden' => array('required'),
            'imagen' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'instrucciones' => array('required'),
            'comprobante' => array('required'),
        );




        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("evento.form")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {



            $evento = new Evento;

            $evento = $this->_storeEvento($request, $evento);




            $evento->slug = Utilidades::slug($evento->titulo);



            $evento->save();



            return redirect()->route("evento.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getEditForm($id = false) {


        $evento = Evento::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($evento->_id)) {
            return redirect()->route("evento.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();


        $data['evento'] = $evento;

        return view('admin.eventoEditForm', $data);
    }

    public function postEdit(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required'),
            'tituloLargo' => array('required'),
            'descripcionCorta' => array('required'),
            'descripcion' => array('required'),
            'codigo' => array('required'),
            'imagen' => array('required'),
            'precio' => array('required'),
            'imagen' => array('required'),
			 'categoria' => array('required'),
            'orden' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'instrucciones' => array('required'),
            'comprobante' => array('required'),
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("evento.formEdit", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $evento = Evento::find($input['_id']);

            if (!$isValid || !isset($evento->_id)) {
                return redirect()->route("evento.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeEvento($request, $evento);


            return redirect()->route("evento.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getDelete($id = false) {


        $isValid = MongoId::isValid($id);
        $evento = Evento::findOrFail($id);

        if (!$isValid || !isset($evento->_id)) {
            return redirect()->route("evento.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($evento->estatus) {

            $evento->estatus = FALSE;
        } else {
            $evento->estatus = TRUE;
        }
        $evento->save();

//        $evento->delete();

        return redirect()->route("evento.panel")->with([
                    'mensaje' => trans('mensajes.operacion.correcta'),
        ]);
    }

//    Utilities


    private function _storeEvento(Request $request, $evento) {


        $input = $request->all();



        $evento->fill($input);



        $evento->save();



        return $evento;
    }

}
