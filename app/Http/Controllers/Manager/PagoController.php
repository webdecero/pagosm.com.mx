<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
use MongoId;
use Excel;
use DB;
//use DatePeriod;
//Models
use App\Pago;
use App\Evento;
use App\Coloquio;
use App\User;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;

class PagoController extends DataTableController {

    private $user;
//    	
//    Evento Id	
//    Nombre del evento		
//    Precio inicial	
//    Moneda inicial	
//    
//    
//    
//    Descuento por código	
//    Código de descuento	
//    Descuento por geolocalización	
//    
//    Descuento por fecha	
//    
//    Cantidad descontada	
//    
//    Tasa de cambio	
//    
//    Precio final	
//    
//    Moneda final	
//    
//    Fecha	
//    
//    Estatus de pago	
//    
//    Tipo de pago	
//    Respuesta	
//    Autorización del banco	
//    Error	
//    Titular	
//    Terminación de tarjeta	
//    Tipo de tarjeta	
//    Monto	
//    
//    
//    
//    Id de usuario 
//    Nombre del usuario	
//    Apellido paterno	
//    Apellido materno	
//    Email	
//    Género	
//    Fecha de nacimiento	
//    Calle	
//    Colonia	
//    Delegación o municipio	
//    Ciudad	
//    Código postal	
//    Estado	
//    País	
//    Institución, Organización o Empresa en la que labora	
//    Sector	
//    Cargo	
//    Área de desempeño	
//    Clave del Centro de Trabajo	Nivel escolar	
//    Especifa (en caso de que seleccione OTRO)


    private $fieldsDetails = [
        '_id' => 'ID',
        'evento_id' => 'Evento Id',
        'evento-titulo' => 'Evento',
        'precioInicial' => 'Precio Inicial',
        'monedaInicial' => 'Moneda Inicial',
        'referencia' => 'Referencia',
        'response' => 'Respuesta',
        'isVentanilla' => 'Es Ventanilla',
        'aut' => 'Num autorización',
        'error' => 'Error',
        'ccName' => 'Tarjetahabiente',
        'amount' => 'Monto tarjeta',
        'ccNum' => 'Tarjeta',
        'type' => 'Tipo',
        'precioInicial' => 'Precio Inicial',
        'monedaInicial' => 'Moneda Inicial',
        'precioDescuento' => 'Precio con descuento',
        'exchangeRate' => 'Tasa de cambio',
        'precioFinal' => 'Precio Final',
        'monedaFinal' => 'Moneda Final',
        'coloquios' => 'Coloquios',
        'descuentos' => 'Descuentos',
        'codigoDescuento' => 'Código Descuento',
        'tituloDescuento' => 'Título Descuento',
        'user_id' => 'User ID',
        'user-nombre' => 'nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'user-genero' => 'Genero',
        'user-fechaNacimiento' => 'Fecha Nacimiento',
        'user-pais' => 'Pais',
        'user-estado' => 'Estado',
        'user-codigoPostal' => 'Codigo Postal',
        'user-ciudad' => 'Ciudad',
        'user-delegacion' => 'Delegacion',
        'user-colonia' => 'Colonia',
        'user-direccion' => 'Direccion',
        'user-empresa' => 'Empresa',
        'user-sector' => 'Sector',
        'user-cargo' => 'Cargo',
        'user-areaDesempeno' => 'Area Desempeno',
        'user-claveTrabajo' => 'Clave Trabajo',
        'user-nivelEscolar' => 'Nivel Escolar',
        'user-especifica' => 'Especifica',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'evento_id' => 'Evento Id',
        'evento-titulo' => 'Evento',
        'precioInicial' => 'Precio Inicial',
        'monedaInicial' => 'Moneda Inicial',
        'referencia' => 'Referencia',
        'response' => 'Respuesta',
        'isVentanilla' => 'Es Ventanilla',
        'aut' => 'Num autorización',
        'error' => 'Error',
        'ccName' => 'Tarjetahabiente',
        'amount' => 'Monto tarjeta',
        'ccNum' => 'Tarjeta',
        'type' => 'Tipo',
        'precioDescuento' => 'Precio Descuento',
        'exchangeRate' => 'Tasa de cambio',
        'precioFinal' => 'Precio Final',
        'monedaFinal' => 'Moneda Final',
        'coloquios' => 'Coloquios',
        'descuentos' => 'Descuentos',
        'codigoDescuento' => 'Código Descuento',
        'tituloDescuento' => 'Título Descuento',
        'user_id' => 'User ID',
        'user-nombre' => 'Nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'user-genero' => 'Genero',
        'user-fechaNacimiento' => 'Fecha Nacimiento',
        'user-pais' => 'Pais',
        'user-estado' => 'Estado',
        'user-codigoPostal' => 'Codigo Postal',
        'user-ciudad' => 'Ciudad',
        'user-delegacion' => 'Delegacion',
        'user-colonia' => 'Colonia',
        'user-direccion' => 'Direccion',
        'user-empresa' => 'Empresa',
        'user-sector' => 'Sector',
        'user-cargo' => 'Cargo',
        'user-areaDesempeno' => 'Area Desempeno',
        'user-claveTrabajo' => 'Clave Trabajo',
        'user-nivelEscolar' => 'Civel Escolar',
        'user-especifica' => 'Especifica',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTable.pago', [$item['_id']]);
        } else if (strpos($field, 'evento-') !== FALSE) {


            $part = explode("-", $field);

            $record = $item['evento']->{$part[1]};
        } else if (strpos($field, 'user-') !== FALSE) {


            $part = explode("-", $field);



            if ($part[1] == 'fechaNacimiento') {


                $record = isset($item['user']->{$part[1]}) ? date('Y-m-d', $item['user']->{$part[1]}->sec) : '';
            } else {



                $record = isset($item['user']->{$part[1]}) ? $item['user']->{$part[1]} : '';
            }
        } else if ($field == 'opciones') {


            $record = "<a href=" . route('page.comprobante', ['idPago' => (string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Comprobante de Pago' > <i class='fa fa-download'></i> </a>";

            $record .= "<a href=" . route('page.comprobante.enviar', ['idPago' => (string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Enviar Comprobante de Pago' > <i class='fa fa-envelope'></i> </a>";
        } else if ($field == 'coloquios' && isset($item[$field])) {



            $coloquios = Coloquio::wherein('_id', $item['coloquios'])->get();

            $data = [];
            foreach ($coloquios as $coloquio) {
                $data[] = $coloquio->titulo;
            }

            $record = implode(',', $data);
        } else if ($field == 'descuentos' && isset($item['descuentos'])) {

            $data = [];
            foreach ($item['descuentos'] as $descuento) {
                $data[] = $descuento['porcentaje'].'%';
            }

            $record = implode(',', $data);
            
        } else if ($field == 'codigoDescuento' ) {

            $codigoDescuento  = false;
            
            foreach ($item['descuentos'] as $descuento) {
                
               if($descuento['tipo'] == "descuentoCodigo"){
                   
                   $codigoDescuento = TRUE;
                   break;
                   
               }
            }

            $record = ($codigoDescuento)? $descuento['codigo']:'';
            
        } 
        else if ($field == 'tituloDescuento' ) {

            $codigoDescuento  = false;
            
            foreach ($item['descuentos'] as $descuento) {
                
               if($descuento['tipo'] == "descuentoCodigo"){
                   
                   $codigoDescuento = TRUE;
                   break;
                   
               }
            }

            $record = ($codigoDescuento)? $descuento['titulo']:'';
            
        } 
        else if ($field == 'response' && isset($item[$field])) {

            switch ($item[$field]) {
                case 'approved':
                    $record = 'aprobado';
                    break;
                case 'denied':
                    $record = 'denegado';
                    break;
                case 'error':
                    $record = 'error';
                    break;
                default:
                    $record = $item[$field];
            }
        }else if ($field == 'isVentanilla' && isset($item[$field])) {

            $record = ($item[$field])?'Verdadero':'Falso';
            
            
        }
        





        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);





        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'referencia' => 'contains',
                'precio' => 'contains',
                'aut' => 'contains',
                'ccName' => 'contains',
                'amount' => 'contains',
                'ccNum' => 'contains',
                'type' => 'contains'
            ],
            'response' => 'equal',
            'evento_id' => 'equal',
            'isVentanilla' => 'equal',
            'isBeca' => 'equal',
        ];








        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'pagos');





        foreach ($cursor as $item) {

            $record = [];




            $item['user'] = User::find($item['user_id']);
            $item['evento'] = Evento::find($item['evento_id']);





            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'pagos');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_pago_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Pago', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'pagos') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {


            $item['user'] = User::find($item['user_id']);
            $item['evento'] = Evento::find($item['evento_id']);

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = Evento::all();



        return view('admin.pagoPanel', $data);
    }

    public function getVentnillaForm($id = false) {


        $pago = Pago::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($pago->_id) || $pago->response != 'incompleta') {
            return redirect()->route("pago.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }




        $data = array();

        $data['user'] = Auth::user();
        $data['pago'] = $pago;


        return view('admin.ventanillaForm', $data);
    }

}
