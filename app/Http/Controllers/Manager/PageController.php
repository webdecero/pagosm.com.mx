<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
//Models
use App\Pagina;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Utilidades;

class PageController extends Controller {

    public function getEditForm($clave) {

        $pagina = Pagina::where('clave', '=', $clave)->first();


        if (!isset($pagina->_id)) {
            return redirect()->route('registros.panel')->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();

        $data['pagina'] = $pagina;


        return view('admin.pageEditForm' . $pagina->clave, $data);
    }

    public function postEdit(Request $request) {

        $input = $request->all();

        $rules = array(
            'clave' => array('required', "exists:paginas,clave"),
//            'contenido' => array('required'),
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
            ]);
        } else {


            $pagina = Pagina::where('clave', '=', $input['clave'])->first();

            if (!isset($pagina->_id)) {
                return back()->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }


            $pagina->fill($input);

            switch ($input['clave']) {
                case 'Subsea':
                case 'Midstream':
                case 'Drilling':

                    $arrays = [
                        'boton' => $input['boton'],
                        'archivo' => $input['archivo']
                    ];

                    $pagina->descargas = Utilidades::combine_keys_with_arrays(array_keys($input['boton']), $arrays);


                    foreach ($input['tituloFecha'] as $value) {

                        $input['tituloFechaSlug'][] = Utilidades::slug($value);
                    }


                    $arrays = [
                        'tituloFecha' => $input['tituloFecha'],
                        'tituloFechaSlug' => $input['tituloFechaSlug'],
                        'inicioFecha' => $input['inicioFecha'],
                        'finalFecha' => $input['finalFecha'],
                        'contenidoFecha' => $input['contenidoFecha']
                    ];

                    $pagina->calendario = Utilidades::combine_keys_with_arrays(array_keys($input['tituloFecha']), $arrays);


                    $pagina->contacto = $input['contacto'];





                    break;
                case 'UKTrade':

                    $pagina->emailAdmin = $input['emailAdmin'];

                    break;
            }

            $pagina->save();

            return redirect()->route('paginas.formEdit', ['clave' => $input['clave']])->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

}
