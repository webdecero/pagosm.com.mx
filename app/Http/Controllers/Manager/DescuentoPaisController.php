<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
use MongoId;
use Excel;
use DB;
//use DatePeriod;
//Models
use App\DescuentoPais;
use App\Evento;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;

class DescuentoPaisController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'titulo',
        'descripcion' => 'descripcion',
        'pais' => 'pais',
        'evento' => 'evento',
        'porcentaje' => 'porcentaje',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'estatus' => 'estatus',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    
    
    private $fieldsDetailsExcel = [
             '_id' => 'ID',
        'titulo' => 'titulo',
        'descripcion' => 'descripcion',
        'pais' => 'pais',
        'evento' => 'evento',
        'porcentaje' => 'porcentaje',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'estatus' => 'estatus',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTable.descuento.pais', [$item['_id']]);
        } else if ($field == 'evento') {



            $evento = Evento::find($item['evento_id']);



            $record = $evento->titulo;
        } else if ($field == 'opciones') {



            $record = "<a href=" . route('descuento.pais.formEdit', [$item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            if ($item['estatus']) {
                $record .= "<a href=" . route('descuento.pais.elimina', [$item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {
                $record .= "<a href=" . route('descuento.pais.elimina', [$item['_id']]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Recuperar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'nombre' => 'contains',
                'descripcion' => 'contains',
                'porcentaje' => 'contains',
            ],
            'estatus' => 'equal',
            'evento_id' => 'equal',
            'pais' => 'equal',
        ];




        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'descuentoPais');





        foreach ($cursor as $item) {

            $record = [];

            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'descuentoPais');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_descuentoPais_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('DescuentoPais', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'descuentoPais') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = Evento::all();



        return view('admin.descuentoPaisPanel', $data);
    }

    public function getAddForm() {




        $data = array();



        $data['user'] = Auth::user();
        $data['eventos'] = Evento::all();


        return view('admin.descuentoPaisAddForm', $data);
    }

    public function postGuarda(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required'),
            'descripcion' => array('required'),
            'pais' => array('required'),
            'porcentaje' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'eventoId' => array('required', 'exists:eventos,_id'),
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("descuento.pais.form")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $evento = Evento::findOrFail($input['eventoId']);

            $descuentoPais = new DescuentoPais;

            $descuentoPais = $this->_storeDescuentoPais($request, $descuentoPais);


            $evento->descuentoPais()->save($descuentoPais);


            $descuentoPais->save();

            return redirect()->route("descuento.pais.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getEditForm($id = false) {


        $descuentoPais = DescuentoPais::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($descuentoPais->_id)) {
            return redirect()->route("descuento.pais.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();
        $data['eventos'] = Evento::all();


        $data['descuentoPais'] = $descuentoPais;

        return view('admin.descuentoPaisEditForm', $data);
    }

    public function postEdit(Request $request) {

        $input = $request->all();



        $rules = array(
               '_id' => array('required', 'exists:descuentoPais,_id'),
            'titulo' => array('required'),
            'descripcion' => array('required'),
            'porcentaje' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("descuento.pais.formEdit", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $descuentoPais = DescuentoPais::find($input['_id']);

            if (!$isValid || !isset($descuentoPais->_id)) {
                return redirect()->route("descuento.pais.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeDescuentoPais($request, $descuentoPais);


            return redirect()->route("descuento.pais.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getDelete($id = false) {


        $isValid = MongoId::isValid($id);
        $descuentoPais = DescuentoPais::findOrFail($id);

        if (!$isValid || !isset($descuentoPais->_id)) {
            return redirect()->route("descuento.pais.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($descuentoPais->estatus) {

            $descuentoPais->estatus = FALSE;
        } else {
            $descuentoPais->estatus = TRUE;
        }
        $descuentoPais->save();

//        $descuentoPais->delete();

        return redirect()->route("descuento.pais.panel")->with([
                    'mensaje' => trans('mensajes.operacion.correcta'),
        ]);
    }

//    Utilities


    private function _storeDescuentoPais(Request $request, $descuentoPais) {


        $input = $request->all();



        $descuentoPais->fill($input);

        $descuentoPais->tipo = 'descuentoPais';

        $descuentoPais->save();



        return $descuentoPais;
    }

}
