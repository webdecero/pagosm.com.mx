<?php

namespace App\Http\Controllers\Manager;


//Providers
use Validator;
use File;
use ImageInter;
//Models
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller {

    public function load(Request $request) {


        $input = $request->all();
        $rules = array(
            'image' => array('required'),
            'path' => array('required'),
            'format' => array('required'),
            'quality' => array('required'),
            'w' => array('required'),
            'h' => array('required')
        );

        $validator = Validator::make($input, $rules);


        $response = [];

        if ($validator->fails()) {

            $response['success'] = false;
            $response['messages'] = $validator->messages()->all();
        } else if (!$request->file('image')->isValid()) {

            $response['success'] = false;
            $response['messages'] = 'Imagen no valida';
        } else {


            $file = $request->file('image');


            $folder = $input['path'];
            $format = '.' . $input['format'];
            $nameFile = uniqid();


            $original = $folder . DIRECTORY_SEPARATOR . $nameFile . $format;


            $img = ImageInter::make($file);

            $img->fit((int)$input['w'], (int)$input['h'])->encode($input['format'], (int) $input['quality'])->save($original);


            if (File::exists($original)) {

                $response['success'] = true;
                $response['data'] = $original;
            } else {
                $response['success'] = false;
                $response['messages'] = "No se encontro imagen";
            }
        }


        return response()->json($response);
    }
    
    
      
    public function uploadFilesFit($files, $urlPath, $w, $h, $prefx = '',  $encode = 'jpg', $quality = 80 ) {

        $folder = $urlPath;
        $format = "." . $encode;

        $imgs = [];


        if (!is_array($files)) {
            $files = [$files];
        }



        foreach ($files as $file) {

            if (empty($file)) {
                $imgs[]['url'] = NULL;
            } else {


                $nameFile = uniqid().$prefx;


                $original = $folder . DIRECTORY_SEPARATOR . $nameFile . $format;

                $img = ImageInter::make($file);

                $img->fit($w, $h)->encode($encode, $quality);

//                $img->widen($input['width'])->crop((int) $input['cropWidth'], (int) $input['cropHeight'], (int) $input['cropX'], (int) $input['cropY'])->encode('jpg', 80);

                $img->save($original);
    
                $imgs[]['url'] = $original;
            }
        }


        return $imgs;
    }

    public function getDataUrlThumbnail($url, $widen = 300, $encode = 'jpg', $quality = 70) {

        return (string) (string) ImageInter::make($url)->widen($widen)->encode($encode, $quality)->encode('data-url');
    }

}
