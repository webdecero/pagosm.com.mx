<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
use MongoId;
use Excel;
use DB;
//use DatePeriod;
//Models
use App\DescuentoCodigo;
use App\Evento;
use App\Pago;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;

class DescuentoCodigoController extends DataTableController {

    private $user = null;
    private $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'titulo',
        'descripcion' => 'descripcion',
        'codigo' => 'codigo',
        'evento' => 'evento',
        'porcentaje' => 'porcentaje',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'aplicado' => 'Estatus Código',
        'user-nombre' => 'Nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'titulo' => 'titulo',
        'descripcion' => 'descripcion',
        'codigo' => 'codigo',
        'evento' => 'evento',
        'porcentaje' => 'porcentaje',
        'fechaInicio' => 'fechaInicio',
        'fechaFinal' => 'fechaFinal',
        'aplicado' => 'Estatus Código',
        'user-nombre' => 'Nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTable.descuento.codigo', [$item['_id']]);
        } else if ($field == 'evento') {



            $evento = Evento::find($item['evento_id']);



            $record = $evento->titulo;
        } else if ($field == 'opciones') {



            if ($item['aplicado'] == FALSE) {
                $record = "<a href=" . route('descuento.codigo.formEdit', [$item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

                $record .= "<a href=" . route('descuento.codigo.elimina', [$item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            }
        } else if (strpos($field, 'user-') !== FALSE && $item['aplicado'] === TRUE) {


            $pago = Pago::whereRaw(['descuentos.codigo' => $item['codigo']])->first();


            $user = $pago->user()->first();


            $part = explode("-", $field);

            $record = isset($user->{$part[1]}) ? $user->{$part[1]} : '';
        } else if ($field == 'aplicado' && isset($item[$field])) {

            $record = ($item[$field]) ? 'Aplicado' : 'No Aplicado';
        }


        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'nombre' => 'contains',
                'descripcion' => 'contains',
                'porcentaje' => 'contains',
                'codigo' => 'contains',
            ],
            'evento_id' => 'equal',
            'titulo' => 'equal',
            'aplicado' => 'equal',
        ];




        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'descuentoCodigo');





        foreach ($cursor as $item) {

            $record = [];

            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'descuentoCodigo');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_descuentoCodigo_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('DescuentoCodigo', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'descuentoCodigo') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = Evento::all();


        $data['descuentoCodigo'] = DescuentoCodigo::groupBy('titulo')->get();


        return view('admin.descuentoCodigoPanel', $data);
    }

    public function getAddForm() {




        $data = array();



        $data['user'] = Auth::user();
        $data['eventos'] = Evento::all();


        return view('admin.descuentoCodigoAddForm', $data);
    }

    public function postGuarda(Request $request) {

        $input = $request->all();


        $rules = array(
            'titulo' => array('required'),
            'descripcion' => array('required'),
            'numero' => array('required'),
            'porcentaje' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
            'eventoId' => array('required', 'exists:eventos,_id'),
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("descuento.codigo.form")->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $evento = Evento::findOrFail($input['eventoId']);


            $numero = (int) $input['numero'];


            for ($i = 0; $i < $numero; $i++) {


                $descuentoCodigo = new DescuentoCodigo;

                $descuentoCodigo->codigo = $this->_generateCodigo();

                $descuentoCodigo = $this->_storeDescuentoCodigo($request, $descuentoCodigo);


                $evento->descuentoCodigo()->save($descuentoCodigo);


                $descuentoCodigo->save();
            }



            return redirect()->route("descuento.codigo.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getEditForm($id = false) {


        $descuentoCodigo = DescuentoCodigo::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($descuentoCodigo->_id)) {
            return redirect()->route("descuento.codigo.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();
        $data['eventos'] = Evento::all();


        $data['descuentoCodigo'] = $descuentoCodigo;

        return view('admin.descuentoCodigoEditForm', $data);
    }

    public function postEdit(Request $request) {

        $input = $request->all();



        $rules = array(
            '_id' => array('required', 'exists:descuentoCodigo,_id'),
            'titulo' => array('required'),
            'descripcion' => array('required'),
            'porcentaje' => array('required'),
            'fechaInicio' => array('required'),
            'fechaFinal' => array('required'),
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("descuento.codigo.formEdit", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $descuentoCodigo = DescuentoCodigo::find($input['_id']);

            if (!$isValid || !isset($descuentoCodigo->_id)) {
                return redirect()->route("descuento.codigo.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }


            $this->_storeDescuentoCodigo($request, $descuentoCodigo);


            return redirect()->route("descuento.codigo.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
    }

    public function getDelete($id = false) {


        $isValid = MongoId::isValid($id);
        $descuentoCodigo = DescuentoCodigo::findOrFail($id);

        if (!$isValid || !isset($descuentoCodigo->_id)) {
            return redirect()->route("descuento.codigo.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

//        if ($descuentoCodigo->estatus) {
//
//            $descuentoCodigo->estatus = FALSE;
//        } else {
//            $descuentoCodigo->estatus = TRUE;
//        }
//        $descuentoCodigo->save();

        $descuentoCodigo->delete();

        return redirect()->route("descuento.codigo.panel")->with([
                    'mensaje' => trans('mensajes.operacion.correcta'),
        ]);
    }

    public function postFiltros(Request $request) {

        $input = $request->all();



        $rules = array(
            
            'idEvento' => array('required', 'exists:descuentoCodigo,evento_id'),
            
        );



        $validator = Validator::make($input, $rules);

        $response = [];

        if ($validator->fails()) {

            $response['success'] = false;
            $response['messages'] = $validator->messages();
        } else {



          
            $data = DescuentoCodigo::where('evento_id', $input['idEvento'])->groupBy('titulo')->get()->toArray();



            $response['success'] = true;
            $response['data'] = $data;
            $response['messages'] = trans('mensajes.registro.exito');
        }

        return response()->json($response);
    }

//    Utilities


    private function _storeDescuentoCodigo(Request $request, $descuentoCodigo) {


        $input = $request->all();



        $descuentoCodigo->fill($input);



        $descuentoCodigo->tipo = 'descuentoCodigo';

        $descuentoCodigo->save();



        return $descuentoCodigo;
    }

    private function _generateCodigo() {


        do {


            $codigo = mb_strtolower(str_random(6), 'UTF-8');

            $descuentoCodigo = DescuentoCodigo::where('codigo', $codigo)->first();
        } while (isset($descuentoCodigo->_id));



        return $codigo;
    }

}
