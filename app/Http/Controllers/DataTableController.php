<?php

namespace App\Http\Controllers;

//Providers

use MongoRegex;
use DB;
//Models
//Helpers and Class
use App\Http\Controllers\Controller;

class DataTableController extends Controller {

    protected $skip = null;
    protected $limit = null;
    protected $order = null;
    protected $startDate = null;
    protected $endDate = null;
   

    protected function getSkip() {

        return $this->skip;
    }

    protected function setSkip($skip) {

        $this->skip = $skip;
    }

    protected function getLimit() {

        return $this->limit;
    }

    protected function setLimit($limit) {

        $this->limit = $limit;
    }

    protected function getOrder() {

        return $this->order;
    }

    protected function setOrder($order) {

        $this->order = $order;
    }

    protected function generateOrder($orders = [], $columns = []) {

        $order = [];

        foreach ($orders as $o) {

            if (isset($columns[$o['column']]) && !empty($columns[$o['column']]['data'])) {

                $column = $columns[$o['column']]['data'];

                $dir = ( $o['dir'] == 'asc' ) ? 1 : -1;

                $order[] = [

                    $column => $dir
                ];
            }
        }

        $this->setOrder($order);
    }

    protected function paginate($page = false, $perPage = false) {

        if ($page && $perPage) {

            $skip = ($page - 1) * $perPage;

            $limit = $perPage;

            $this->setSkip($skip);

            $this->setLimit($limit);
        }
    }

    protected function generateQuery($inputs = [], $searchable = [], $root = true) {

        $query = [];

        foreach ($inputs as $input => $value) {

            if (array_key_exists($input, $searchable) && $value) {

                if (is_array($searchable[$input])) {
                    $inputsTemp = [];

                    foreach ($searchable[$input] as $k => $v) {
                        $inputsTemp[$k] = $value;
                    }

                    $query['$or'][] = $this->generateQuery($inputsTemp, $searchable[$input], false);
                } else if ($searchable[$input] == 'contains') {

                    if ($root) {
                        $query[$input] = [

                            '$regex' => new MongoRegex("/" . $value . "/i")
                        ];
                    } else {
                        $query['$or'][][$input] = [

                            '$regex' => new MongoRegex("/" . $value . "/i")
                        ];
                    }
                } else if ($searchable[$input] == 'equal') {
                   

                    if (in_array($value, ['true', 'false'])) {
                        
                        $query[$input] = ($value == 'true' ) ? true : false;
                    }else if (is_array($value)){
                        
                        $query[$input]['$in'] = $value;
                        
                    } else {

                        $query[$input] = (is_numeric($value)) ? (float) $value : $value;
                    }
                }
            }
        }

        return $query;
    }

    protected function _getConsultReport($query = [], $paginate = true, $count = false, $order = false, $collectionName = 'users') {


        if ($count) {

            $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

                return $collection->count($query);
            });
        } else {

            $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

                return $collection->find($query);
            });
        }
        
        

        if ($paginate) {

            $skip = $this->getSkip();

            $limit = $this->getLimit();

            if ($skip != null) {

                $cursor->skip($skip);
            }

            if ($limit != null) {

                $cursor->limit($limit);
            }
        }

        if ($order) {

            $orders = $this->getOrder();

            if (is_array($orders))
                $cursor->sort($orders);
        }

        return $cursor;
    }

   
    protected function formatRecord($field, $item) {

        $record = '';

        if (($field == '_id' || $field == 'id') && isset($item['_id'])) {

            $record = $item['_id']->{'$id'};
        } else if (in_array($field, ['created_at', 'updated_at','fechaNacimiento', 'fecha', 'fechaInicio', 'fechaFinal']) && isset($item[$field])) {

            $record = date('Y-m-d H:i', $item[$field]->sec);
        } else if (isset($item[$field])) {

            $record = $item[$field];
        }

        return $record;
    }
    

  

}
