<?php

namespace App\Http\Controllers\WebPay;
use Config;
class Encrypt
{



    public function __construct()
    {
        $this->key = (Config::has('webPay.key')) ? Config::get('webPay.key') : null;

        $this->payType = (Config::has('webPay.payType')) ? Config::get('webPay.payType') : null;

    }

    public function generateXmla($resource)
    {

        $xml = '<xml>';

        $xml .= '<tpPago>'.$this->payType.'</tpPago>';

        $xml .= '<amount>'.number_format($resource['precioFinal'], 2, '.', '').'</amount>';

        $xml .= '<urlResponse>'.$resource['urlResponse'].'</urlResponse>';

        $xml .= '<referencia>'.$resource['referencia'].'</referencia>';

        $xml .= '<moneda>'.$resource['monedaFinal'].'</moneda>';

        $xml .= '<date_hour>'.date("Y-m-d\TH:i:sP").'</date_hour>';


        $xmla = $this->encrypt($xml);

        return $xmla;
    }

    public function encrypt($plaintext)
    {

        $key128 = $this->key;

        $plaintext = Encrypt::pkcs5_pad($plaintext, mcrypt_get_block_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_CBC));

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv_size = mcrypt_enc_get_iv_size($cipher);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_RANDOM);

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        if (mcrypt_generic_init($cipher, hex2bin($key128), $iv) != -1)
        {
            $cipherText = mcrypt_generic($cipher, $plaintext );
            mcrypt_generic_deinit($cipher);
            $b64ciphertext = base64_encode($iv.$cipherText);
            $b64ciphertext = bin2hex($b64ciphertext);
          return $b64ciphertext;
        }
        return "";
    }

    private static function pkcs5_pad ($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

}