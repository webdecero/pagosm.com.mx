<?php

namespace App\Http\Controllers\WebPay;
use Config;

class WebPay
{

    public function __construct()
    {
        $this->urlWebPay = (Config::has('webPay.urlWebPay')) ? Config::get('webPay.urlWebPay') : null;

        $this->idCompany = (Config::has('webPay.idCompany')) ? Config::get('webPay.idCompany') : null;

        $this->xmlm = (Config::has('webPay.xmlm')) ? Config::get('webPay.xmlm') : null;
    }

    public function generateWebPay($xmla = false , $xmle = false)
    {

        $webPay = '';

        $webPay .= $this->urlWebPay.'?';

        $webPay .= 'id_company='.$this->idCompany;

        $webPay .= '&';

        $webPay .= 'xmlm='.$this->xmlm;

        $webPay .= '&';

        $webPay .= 'xmla='.$xmla;

        if($xmle)
        {

            $webPay .= '&';

            $webPay .= 'xmle='.$xmle;
        }

        return $webPay;

    }
}