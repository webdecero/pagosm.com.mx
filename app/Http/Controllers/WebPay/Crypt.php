<?php
namespace App\Http\Controllers\WebPay;
  /**
   *  Implementacion de algoritmo AES
   *  @author: Mercadotecnia, Ideas y Tecnologia
   *  @date  : 2014
   */

    class AESEncriptacion{

      public static function encriptar($plaintext, $key128)
      {
        $plaintext = AESEncriptacion::pkcs5_pad($plaintext, mcrypt_get_block_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_CBC));

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv_size = mcrypt_enc_get_iv_size($cipher);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_RANDOM);

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        if (mcrypt_generic_init($cipher, hex2bin($key128), $iv) != -1)
        {
            $cipherText = mcrypt_generic($cipher, $plaintext );
            mcrypt_generic_deinit($cipher);
            $b64ciphertext = base64_encode($iv.$cipherText);
            $b64ciphertext = bin2hex($b64ciphertext);
          return $b64ciphertext;
        }
        return "";
      }


      public static function desencriptar($encodedInitialData, $key)
      {
         $encodedInitialData =  hex2bin($encodedInitialData);
        $encodedInitialData =  base64_decode($encodedInitialData);
         $iv = substr($encodedInitialData,0,16);
        $encodedInitialData = mb_substr($encodedInitialData,16);

        $cypher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        if (mcrypt_generic_init($cypher, hex2bin($key), $iv) != -1)
        {
          $decrypted = mdecrypt_generic($cypher, $encodedInitialData);
          mcrypt_generic_deinit($cypher);
          mcrypt_module_close($cypher);
          return AESEncriptacion::pkcs5_unpad($decrypted);
        }

        return "";
      }


      private static function pkcs5_pad ($text, $blocksize)
      {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
      }

      private static function pkcs5_unpad($text)
      {
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;
        return substr($text, 0, -1 * $pad);
      }
    }

