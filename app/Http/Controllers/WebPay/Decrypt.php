<?php

namespace App\Http\Controllers\WebPay;
use Config;
class Decrypt
{

    public function __construct()
    {
         $this->key = (Config::has('webPay.key')) ? Config::get('webPay.key') : null;
    }

    public function decrypt($encodedInitialData)
      {

        $key = $this->key;

         $encodedInitialData =  hex2bin($encodedInitialData);

        $encodedInitialData =  base64_decode($encodedInitialData);
         $iv = substr($encodedInitialData,0,16);
         mb_internal_encoding('ISO-8859-1');
        $encodedInitialData = mb_substr($encodedInitialData,16);

        $cypher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        if (mcrypt_generic_init($cypher, hex2bin($key), $iv) != -1)
        {
          $decrypted = mdecrypt_generic($cypher, $encodedInitialData);
          mcrypt_generic_deinit($cypher);
          mcrypt_module_close($cypher);
          return Decrypt::pkcs5_unpad($decrypted);
        }

        return "";
      }

      private static function pkcs5_unpad($text)
      {
        $pad = ord($text{strlen($text)-1});
        if ($pad > strlen($text)) return false;
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;
        return substr($text, 0, -1 * $pad);
      }


}