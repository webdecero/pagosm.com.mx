<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use ImageInter;


use GeoIP;
//Models
use App\Evento;


class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        setlocale(LC_ALL, 'es_MX');
        date_default_timezone_set('America/Mexico_City');
    }

    public function sortBySubkey(&$array, $subkey, $sortType = SORT_ASC) {
        foreach ($array as $subarray) {
            $keys[] = $subarray[$subkey];
        }

        array_multisort($keys, $sortType, $array);
    }

    public function uploadFilesFit($files, $urlPath, $w, $h, $prefx = '', $encode = 'jpg', $quality = 80) {

        $folder = $urlPath;
        $format = "." . $encode;

        $imgs = [];


        if (!is_array($files)) {
            $files = [$files];
        }



        foreach ($files as $file) {

            if (empty($file)) {
                $imgs[]['url'] = NULL;
            } else {


                $nameFile = uniqid() . $prefx;


                $original = $folder . DIRECTORY_SEPARATOR . $nameFile . $format;

                $img = ImageInter::make($file);

                $img->fit($w, $h)->encode($encode, $quality);

//                $img->widen($input['width'])->crop((int) $input['cropWidth'], (int) $input['cropHeight'], (int) $input['cropX'], (int) $input['cropY'])->encode('jpg', 80);

                $img->save($original);

                $imgs[]['url'] = $original;
            }
        }


        return $imgs;
    }

    public function getDataUrlThumbnail($url, $widen = 300, $encode = 'jpg', $quality = 70) {

        return (string) (string) ImageInter::make($url)->widen($widen)->encode($encode, $quality)->encode('data-url');
    }
    
    
    
    protected function _aplicarDescuentos(Evento $evento) {

        $descuentosAplicados = [];
        $isBeca = FALSE;


        $precio = (int) $evento->precio;

        $now = new \DateTime();


        $geoIP = GeoIP::getLocation();


        if ($geoIP['default'] != TRUE) {


            $descuentoPais = $evento->descuentoPais()->where('fechaInicio', '<', $now)->where('fechaFinal', '>', $now)->where('pais', '=', $geoIP['isoCode'])->where('estatus', '=', true)->orderBy('fechaInicio', 'asc')->get();


            foreach ($descuentoPais as $descuento) {

                if ($precio <= 0) {

                    $precio = 0;
                    $isBeca = TRUE;
                    break;
                }

                $precioTemp = ($precio * (int) $descuento->porcentaje) / 100;
                $precio = $precio - $precioTemp;

                $descuentoArray = $descuento->toArray();

                $descuentoArray['cantidadDescontada'] = $precioTemp;


                $descuentoArray['precio'] = $precio;

                $descuentosAplicados[] = $descuentoArray;
            }
        }







        $descuentoFecha = $evento->descuentoFecha()->where('fechaInicio', '<', $now)->where('fechaFinal', '>', $now)->where('estatus', '=', true)->orderBy('fechaInicio', 'asc')->get();


        foreach ($descuentoFecha as $descuento) {

            if ($precio <= 0) {

                $precio = 0;
                $isBeca = TRUE;
                break;
            }

            $precioTemp = ($precio * (int) $descuento->porcentaje) / 100;
            $precio = $precio - $precioTemp;

            $descuentoArray = $descuento->toArray();

            $descuentoArray['cantidadDescontada'] = $precioTemp;


            $descuentoArray['precio'] = $precio;

            $descuentosAplicados[] = $descuentoArray;
        }




        return [
            "precio" => $precio,
            "isBeca" => $isBeca,
            "descuentosAplicados" => $descuentosAplicados
        ];



    }
    
    
    

}
