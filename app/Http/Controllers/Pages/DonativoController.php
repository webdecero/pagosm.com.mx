<?php

namespace App\Http\Controllers\Pages;

//Providers
use Validator;
use Auth;
use Agent;
use GeoIP;
use MongoDate;
//Models
use App\User;
use App\Donativo;
use App\Pago;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DonativoController extends Controller {

    public function updateDonativos() {


        $donativos = Donativo::all();

        foreach ($donativos as $donativo) {
			
			
            $pago = $donativo->pago()->first();
			

            if ($pago != NULL) {

				
                $donativo->tipoTarjeta = $pago->type;
                $donativo->usoComprobante = 'Gastos en general';
                $donativo->moneda = 'MXN';
                $donativo->date = $pago->date;
                $donativo->save();


                
            }
        }
    }

    public function viewInicio() {

        $data = array();

        $user = Auth::user();


        $pagos = $user->pagos()->get();


        $now = new \DateTime();


        foreach ($pagos as $pago) {

//            $pago->aceptaDonativo = $this->_valideDonativo($pago);


            $evento = $pago->evento()->first();

            if (!isset($evento->streamingActiva) || $evento->streamingActiva == FALSE) {

                $pago->streamingActiva = FALSE;
            } else {

                $pago->streamingActiva = TRUE;
                $pago->streamingEvento = $evento;

                if ($now < $evento->streamingFechaInicio || $now > $evento->streamingFechaFinal) {

                    $pago->streamingMostrarFecha = TRUE;
                } else {

                    $pago->streamingMostrarFecha = FALSE;

                 
                }
            }
        }



        $data['pagos'] = $pagos;

        $data['user'] = $user;

        return view('sm.historial', $data);
    }

    public function getDonativoForm($idPago = NULL) {

        $data = array();

        $data['user'] = Auth::user();


        $data['pago'] = Pago::find($idPago);

        return view('sm.donativo', $data);
    }

    public function postDonativo(Request $request) {


        $input = $request->all();



        $rules = array(
            'idPago' => array('required', 'exists:pagos,_id'),
            'nombreDonativo' => array('required'),
            'razonSocialDonativo' => array('required'),
            'emailDonativo' => array('required', 'email'),
            'rfcDonativo' => array('required'),
            'telefonoDonativo' => array('required'),
            'direccionDonativo' => array('required'),
            'numExtDonativo' => array('required')
        );





        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("page.donativo", [ 'idPago' => $input['idPago']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $pago = Pago::find($input['idPago']);

            $valid = $this->_valideDonativo($pago);


            if (!$valid) {
                return redirect()->route("page.donativo", [ 'idPago' => $input['idPago']])->with([
                            'mensaje' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }




            $user = Auth::user();

            $evento = $pago->evento()->first();
            $transaccion = $pago->transaccion()->first();


            $donativoCount = Donativo::all()->count();

            $donativo = new Donativo;

            $donativo->fill($input);

            $donativo->agrupacion = ((int) $donativoCount + 1);

            $donativo->save();




            $pago->donativos()->save($donativo);
            $pago->aceptaDonativo = FALSE;

            $pago->save();



            $transaccion->donativos()->save($donativo);


            $evento->donativos()->save($donativo);
            $user->donativos()->save($donativo);







            return redirect()->route('page.historial')->with([
                        'mensaje' => trans('mensajes.donativo.correcta')
                    ])->withInput($request->except('password'));
        }
    }

    private function _valideDonativo($pago) {


        $now = new \DateTime();

        $response = FALSE;



        $diff = $now->diff($pago->updated_at);


        if (isset($pago->aceptaDonativo) && $pago->aceptaDonativo == FALSE) {

            $response = FALSE;
        } else if ($diff->d > 3 || (isset($pago->isBeca) && $pago->isBeca == TRUE)) {



            $response = FALSE;

            $pago->aceptaDonativo = FALSE;

            $pago->save();
        } else {


            $response = TRUE;
        }


        return $response;
    }

}
