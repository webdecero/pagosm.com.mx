<?php

namespace App\Http\Controllers\Pages;

//Providers
use Validator;
use App;
use Auth;
use Agent;
use GeoIP;
use MongoDate;
//Models
use App\User;
use App\Donativo;
use App\Evento;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistroController extends Controller {

	public function viewInicio($idEvento = NULL) {

		$data = array();

		$data['user'] = Auth::user();
		$data['idEvento'] = $idEvento;


		return view('sm.registro', $data);
	}

	public function postGuarda(Request $request) {


		$input = $request->all();



		$rules = array(
			'nombre' => array('required'),
			'apellidoPaterno' => array('required'),
			'apellidoMaterno' => array('required'),
			'genero' => array('required'),
			'pais' => array('required'),
			'email' => array('required', 'email', 'unique:users,email'),
			'password' => array('required', 'min:6'),
			'empresa' => array('required'),
			'cargo' => array('required'),
		);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			return redirect()->route("page.registro")->withErrors($validator)->with([
						'error' => trans('mensajes.registro.incompleto'),
					])->withInput($request->except('password'));
		} else {




			$user = new User;


			$user->estatus = TRUE;

			$user = $this->_storeRegistro($request, $user);



			$userAuth = array(
				'email' => $input['email'],
				'password' => $input['password'],
				'estatus' => TRUE
			);


			if (Auth::attempt($userAuth, TRUE)) {


				if (!empty($input['idEvento'])) {
					return redirect()->route('page.comprar', ['idEvento' => $input['idEvento']])->with([
								'mensaje' => trans('mensajes.operacion.correcta')
							])->withInput($request->except('password'));
				} else {
					return redirect()->route('page.inicio')->with([
								'mensaje' => trans('mensajes.operacion.correcta')
							])->withInput($request->except('password'));
				}
			} else {

				return redirect()->route("page.registro")->withErrors($validator)->with([
							'error' => trans('mensajes.usuario.noEncontrado')
						])->withInput($request->except('password'));
			}
		}
	}

	public function postLogin(Request $request) {

		$rules = array(
			'email' => array('required', 'email'),
			'password' => array('required', 'min:6'),
		);

		$input = $request->all();

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {


			return redirect()->route("page.inicio")->withErrors($validator)->with([
						'errorLogin' => trans('mensajes.usuario.noEncontrado')
					])->withInput($request->except('password'));
		} else {

			$user = array(
				'email' => $input['email'],
				'password' => $input['password'],
				'estatus' => TRUE
			);


			if (Auth::attempt($user, TRUE)) {

				if ($request->session()->has('back_url')) {

					$value = $request->session()->pull('back_url');


					return redirect($value)->with([
								'mensaje' => trans('mensajes.usuario.login')
					]);
				}



				return redirect()->route('page.inicio', ['#eventos'])->with([
							'mensaje' => trans('mensajes.usuario.login')
						])->withInput($request->except('password'));
			} else {

				return redirect()->route("page.inicio")->withErrors($validator)->with([
							'errorLogin' => trans('mensajes.usuario.noEncontrado')
						])->withInput($request->except('password'));
			}
		}
	}

	public function postRecovery(Request $request) {

		$rules = array(
			'email' => array('required', 'email', 'exists:users,email')
		);

		$input = $request->all();

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {


			return redirect()->route("page.inicio")->withErrors($validator)->with([
						'errorRecovery' => trans('mensajes.usuario.noEncontrado')
					])->withInput($request->except('password'));
		} else {



			$input['password'] = str_random(6);



			$user = User::where('email', $input['email'])->first();
			$user->password = bcrypt($input['password']);


			$user->save();


			\Mail::send('sm.email.recuperar', $input, function ($message) use ($input) {


				$message->to($input['email'])->subject('Recuperar Contraseña');
			});




			return redirect()->route('page.inicio')->with([
						'errorLogin' => trans('mensajes.usuario.recuperar')
					])->withInput($request->except('password'));
		}
	}

	public function getLogout() {
		Auth::logout();
		return redirect()->route("page.inicio");
	}

	private function _storeRegistro(Request $request, User $user) {

		$input = $request->all();



		$user->fill($input);


		$user->fechaNacimiento = new MongoDate(strtotime($input['fechaNacimiento']));
		$user->password = bcrypt($input['password']);
		$user->rol = 'user';



		$user->languages = Agent::languages();

		$platform = Agent::platform();
		$user->platform = $platform;
		$user->platformVersion = Agent::version($platform);


		$browser = Agent::browser();
		$user->browser = $browser;
		$user->browserVersion = Agent::version($browser);


		$user->isDesktop = Agent::isDesktop();
		$user->isRobot = Agent::isRobot();
		$user->device = Agent::device();

		$user->HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];


		$user->ip = $request->ip();

		$user->ips = $request->ips();

		$user->geoIP = GeoIP::getLocation();


		$user->save();






		return $user;
	}

}
