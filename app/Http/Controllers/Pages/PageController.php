<?php

namespace App\Http\Controllers\Pages;

//Providers

use Auth;
//Models
use App\Evento;
use App\Pagina;
//Helpers and Class
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller {

    public function viewInicio() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = [];
        $eventos = Evento::where('estatus', '=', true)->orderBy('orden', 'desc')->get();


        $data['categorias'] = Evento::groupBy('categoria')->get(['categoria'])->toArray();
        


        foreach ($eventos as $evento) {

            $descuentosEvento = $this->_aplicarDescuentos($evento);

//        dd($descuentosEvento['descuentosAplicados']);

            $evento->descuentosAplicados = $descuentosEvento['descuentosAplicados'];

            $evento->precioDescuento = $descuentosEvento['precio'];

            $data['eventos'][] = $evento;
        }


        $pagina = Pagina::where('clave', 'Banner')->first();

        $data[$pagina->clave] = $pagina->toArray();




        return view('sm.inicio', $data);
    }

    public function viewAviso() {

        $data = array();

        $data['user'] = Auth::user();


        return view('sm.aviso', $data);
    }

    public function viewTerminos() {

        $data = array();

        $data['user'] = Auth::user();


        return view('sm.terminos', $data);
    }

    public function viewRecibo() {

        $data = array();

        $data['user'] = Auth::user();

        $pagina = Pagina::where('clave', 'Recibo')->first();

        $data[$pagina->clave] = $pagina->toArray();

        return view('sm.infoRecibos', $data);
    }

    public function viewStreaming($idPago) {

        $data = array();

        $data['user'] = Auth::user();


        $now = new \DateTime();

        $user = Auth::user();


        $pago = $user->pagos()->find($idPago);


        if ($pago == NULL) {

            return back()->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }

        $evento = $pago->evento()->where('streamingFechaInicio', '<', $now)->where('streamingFechaFinal', '>', $now)->first();

        if ($evento == NULL) {

            return back()->with([
                        'error' => 'Transmisión fuera de horario',
            ]);
        }


        $data['evento'] = $evento;

        return view('sm.streaming', $data);
    }

    public function enviarEmailContacto(Request $request) {

        $input = $request->all();

        \Mail::send('sm.email.contacto', ['input' => $input], function ($m) {
            $m->to('contacto@fundacion-sm.com')->subject('Contacto');
        });

        return back()->with([
                    'mensaje' => "Mensaje enviado con éxito",
        ]);
    }

}
