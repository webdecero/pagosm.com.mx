<?php

namespace App\Http\Controllers\Pages;

//Providers
use Validator;
use Auth;
//use DateInterval;
use MongoId;
use Parser;
use GeoIP;
//Models
use App\Evento;
use App\Coloquio;
use App\Pago;
use App\PagoLogError;
use App\Transaccion;
use App\User;
use App\DescuentoCodigo;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\WebPay\VerificationCode;
use App\Http\Controllers\WebPay\Encrypt;
use App\Http\Controllers\WebPay\Decrypt;
use App\Http\Controllers\WebPay\WebPay;
use App\Http\Controllers\Currencylayer\Currencylayer;

//use App\Http\Controllers\DateRange;

class ComprarController extends Controller {

    public function viewInicio($idEvento) {

        $data = array();


        $data['user'] = Auth::user();

        $isValid = MongoId::isValid($idEvento);

        $evento = Evento::where('_id', $idEvento)->where('estatus', true)->first();

        if (!$isValid || !isset($evento->_id)) {
            return redirect()->route("page.inicio")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data['fechasColoquio'] = Coloquio::getFechasColoquio($idEvento);


        $descuentosEvento = $this->_aplicarDescuentos($evento);

//        dd($descuentosEvento['descuentosAplicados']);

        $data['descuentosAplicados'] = $descuentosEvento['descuentosAplicados'];

        $evento->precioDescuento = $descuentosEvento['precio'];


        $cl = new Currencylayer();

        $data['USD'] = $cl->convertUSDMXN();



        $data['evento'] = $evento;

        return view('sm.comprar', $data);
    }

    public function dispatcherPagar(Request $request) {

        $input = $request->all();

        $data = array();

        $rules = array(
            'idEvento' => array('required', 'exists:eventos,_id'),
            'codigoDescuento' => array('exists:descuentoCodigo,codigo'),
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
                    ])->withInput($request->except('password'));
        } else {



            $user = Auth::user();
            $data['user'] = $user;

            $isValid = MongoId::isValid($input['idEvento']);

            $evento = Evento::find($input['idEvento']);


            if (!$isValid || !isset($evento->_id)) {
                return redirect()->route("page.inicio")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }

            $pago = $user->pagos()->where('evento_id', $evento->_id)->first();

            if (isset($pago->_id)) {
                return back()->with([
                            'error' => trans('mensajes.webpay.comprado'),
                ]);
            }


            if (isset($input['codigoDescuento']) && !empty($input['codigoDescuento'])) {

                $now = new \DateTime();

                $descuentoCodigo = $evento->descuentoCodigo()->where('codigo', $input['codigoDescuento'])->where('fechaInicio', '<', $now)->where('fechaFinal', '>', $now)->where('aplicado', '=', FALSE)->first();




                if (!isset($descuentoCodigo->_id)) {
                    return back()->with([
                                'error' => trans('mensajes.webpay.codigoError'),
                    ]);
                }

                $resource['codigoDescuento'] = $input['codigoDescuento'];
            }


            if (isset($input['coloquios'])&&!empty($input['coloquios'])) {

                $validaColoquio = $this->_validaColoquios($input['coloquios']);

                if ($validaColoquio == FALSE) {
                    return back()->with([
                                'error' => trans('mensajes.webpay.coloquio'),
                    ]);
                }
                $resource['coloquios'] = $input['coloquios'];
            }

            $resource['idEvento'] = $evento->_id;
            $resource['precioInicial'] = $evento->precio;
            $resource['monedaInicial'] = $evento->moneda;
            $resource['referencia'] = $this->_getUnicVerificationCode();

            return redirect()->route("page.pagar")->with($resource)->withInput($request->except('password'));
        }
    }

    public function viewPagar(Request $request) {

        $input = $request->session()->all();


        $rules = array(
            'idEvento' => array('required', 'exists:eventos,_id'),
            'referencia' => array('required'),
            'precioInicial' => array('required'),
            'monedaInicial' => array('required'),
            'codigoDescuento' => array('exists:descuentoCodigo,codigo')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->with([
                        'error' => trans('mensajes.webpay.refresh'),
                    ])->withInput($request->except('password'));
        } else {


            $data = array();


            $user = Auth::user();


            $evento = Evento::find($input['idEvento']);


//            Aplicar  Descuentos


            if (isset($input['codigoDescuento'])) {

                $descuentosEvento = $this->_aplicarDescuentoCodigo($evento, $input['codigoDescuento']);
            } else {
                $descuentosEvento = $this->_aplicarDescuentos($evento);
            }


//            dd($descuentosEvento['descuentosAplicados']);

            $input['descuentos'] = $descuentosEvento['descuentosAplicados'];

            $input['precioDescuento'] = $descuentosEvento['precio'];


            $input['isBeca'] = $descuentosEvento['isBeca'];



            if ($input['monedaInicial'] != 'MXN') {

                $input = $this->_conversionDivisas($input);
            } else {
                $input['precioFinal'] = $input['precioDescuento'];
                $input['monedaFinal'] = $input['monedaInicial'];
            }


            $transaccion = new Transaccion($input);

            $transaccion->save();


            if ($input['isBeca'] != TRUE) {



                $input['urlResponse'] = route('page.recibo', ['idTransaccion' => $transaccion->_id]);


                $encrypt = new Encrypt();
                $xmla = $encrypt->generateXmla($input);

                $webPay = new WebPay();
                $webpay = $webPay->generateWebPay($xmla);



                $transaccionData = ['xmla' => $xmla, 'webpay' => $webpay, 'response' => 'incompleta', 'urlResponse' => $input['urlResponse']];

                $transaccion->fill($transaccionData);

                $transaccion->save();



                $evento->transacciones()->save($transaccion);
                $user->transacciones()->save($transaccion);



                $data['user'] = $user;
                $data['evento'] = $evento;
                $data['transaccion'] = $transaccion;
                $data['webpay'] = $webpay;


                return view('sm.pagar', $data);
            } else {


                $transaccion->response = 'approved';

                $transaccion->save();


                $evento->transacciones()->save($transaccion);
                $user->transacciones()->save($transaccion);


                $pago = $this->_addTransaccionToPagoApproved($transaccion);

                $this->_codigoDescuentoAplicado($pago);

                $data['user'] = $user;
                $data['pago'] = $pago;
                $data['evento'] = $evento;
                $data['transaccion'] = $transaccion;

                $pdf = $this->_generateComprobantePdf($pago);

                if ($pdf !== NULL) {

                    $path = storage_path('PDF/comprobante_' . $pago->id . '.pdf');

                    $pdf->save($path);

                    $this->_sendEmailComprobantePdf($pago, $path);
                }

                return view('sm.beca', $data);
            }
        }
    }

    public function responseRecibo(Request $request, $idTransaccion) {

        $data = [];

        $transaccion = Transaccion::find($idTransaccion);
        $evento = $transaccion->evento()->first();
        $user = Auth::user();



        $data['evento'] = $evento;

//        if ($idTransaccion == '571963469d54b11b5f5dad52') {
//            $pay = [ "referencia" => "334910507089",
//                "response" => "approved",
//                "aut" => "658309",
//                "error" => null,
//                "ccName" => "Uriel Acosta Hernandez",
//                "ccNum" => "4197",
//                "amount" => "0.1",
//                "type" => "CREDITO/BANAMEX/MASTERCARD",
//                "xmlResponse" => "xml"
//            ];
//        } else {

        $respuesta = $request->get('respuesta');
        $pay = $this->_getDecodeResponseService($respuesta, $user, $evento);
//        }


        if ($pay == NULL) {

            $data['success'] = false;
            $data['messages'] = trans('mensajes.webpay.noRespuesta');
        } elseif (is_array($pay)) {


            $transaccion = Transaccion::where('referencia', $pay['referencia'])->where('response', 'incompleta')->where('_id', $idTransaccion)->first();

            if (!isset($transaccion->id)) {
                $data['success'] = false;
                $data['messages'] = trans('mensajes.webpay.utilizado');
            } else {


                $pay['isVentanilla'] = FALSE;


                $transaccion->fill($pay);

                $transaccion->save();


                switch ($pay['response']) {
                    case 'error':
                    case 'denied':

                        $data['success'] = false;

                        $data['messages'] = trans('mensajes.webpay.' . $pay['response']);

                        $data['error'] = isset($pay['error']) ? $pay['error'] : '';

                        break;
                    case 'approved':

                        $data['success'] = true;

                        $data['data'] = $pay;

                        $data['messages'] = trans('mensajes.webpay.approved');


                        break;
                }


                if ($pay['response'] == 'approved') {

                    $pago = $this->_addTransaccionToPagoApproved($transaccion);

                    $data['pago'] = $pago;



                    $this->_codigoDescuentoAplicado($pago);



                    $pdf = $this->_generateComprobantePdf($pago);

                    if ($pdf !== NULL) {

                        $path = storage_path('PDF/comprobante_' . $pago->id . '.pdf');

                        $pdf->save($path);

                        $this->_sendEmailComprobantePdf($pago, $path);
                    }
                }
            }
        }

        return view('sm.recibo', $data);
    }

    public function getInstrucciones($referencia) {


        $rules = array(
            'referencia' => array('required', 'exists:transaccion,referencia'),
        );

        $input = array(
            'referencia' => $referencia
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("page.inicio", ['#eventos'])->withErrors($validator)->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        } else {


            $transaccion = Transaccion::where('referencia', $referencia)->where('response', 'incompleta')->first();


            $evento = $transaccion->evento()->first();
            $user = $transaccion->user()->first();


            return \PDF::loadView('sm.pdf.instrucciones', ['transaccion' => $transaccion, 'evento' => $evento, 'user' => $user])->download($referencia . '_instrucciones.pdf');
        }
    }

    public function getComprobante($idPago) {


        $rules = array(
            'idPago' => array('required', 'exists:pagos,_id')
        );

        $input = array(
            'idPago' => $idPago
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("page.inicio", ['#eventos'])->withErrors($validator)->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        } else {


            $pago = Pago::where('_id', $idPago)->where('response', 'approved')->first();

            $pdf = $this->_generateComprobantePdf($pago);

            if (!isset($pago->id) || $pdf == NULL) {
                return redirect()->route("page.inicio", ['#eventos'])->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }

            return $pdf->download($pago->id . '_comprobante.pdf');
        }
    }

    public function sendComprobante($idPago) {


        $rules = array(
            'idPago' => array('required', 'exists:pagos,_id')
        );

        $input = array(
            'idPago' => $idPago
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("page.inicio", ['#eventos'])->withErrors($validator)->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        } else {


            $pago = Pago::where('_id', $idPago)->where('response', 'approved')->first();


            $pdf = $this->_generateComprobantePdf($pago);


            if ($pdf !== NULL) {

                $path = storage_path('PDF/comprobante_' . $pago->id . '.pdf');

                $pdf->save($path);
                
               $envio = $this->_sendEmailComprobantePdf($pago, $path, 'sm.email.comprobante', 'Reenvío de comprobante CILELIJ');
               
               
               if($envio){
                    return back()->with([
                        'mensaje' => 'El comprobante ha sido enviado',
                ]);
               }else{
                   return back()->with([
                        'error' => trans('mensajes.operacion.incorrecta'),
                ]);
               }
               

               

                
            }
        }
    }

    private function _sendEmailComprobantePdf($pago, $path, $view = 'sm.email.comprobante', $subject = 'Confirmación') {


        $evento = $pago->evento()->first();
        $user = $pago->user()->first();
        
        $coloquios = null;
        
        
        if(isset($pago->coloquios)&& !empty($pago->coloquios)  ){
            
           $coloquios = Coloquio::wherein('_id', $pago->coloquios)->get();
            
         }
        
        Return \Mail::send($view, ['pago' => $pago, 'evento' => $evento, 'user' => $user, 'coloquios' => $coloquios ], function ($m) use ($user, $path, $subject) {

                    $m->attach($path);

                    $m->to($user->email, $user->nombre)->subject($subject);
                });
    }

    private function _generateComprobantePdf($pago) {

        if ($pago->response == 'approved') {
            $evento = $pago->evento()->first();
            $user = $pago->user()->first();
            
             $coloquios = null;
        
        
        if(isset($pago->coloquios)&& !empty($pago->coloquios)  ){
            
           $coloquios = Coloquio::wherein('_id', $pago->coloquios)->get();
            
         }
         

            return \PDF::loadView('sm.pdf.comprobante', ['pago' => $pago, 'evento' => $evento, 'user' => $user, 'coloquios' => $coloquios]);
        } else {
            return NULL;
        }
    }

    private function _aplicarDescuentoCodigo(Evento $evento, $codigo) {

        $descuentosAplicados = [];
        $isBeca = FALSE;


        $precio = (int) $evento->precio;

        $now = new \DateTime();




        $descuento = $evento->descuentoCodigo()->where('codigo', $codigo)->where('fechaInicio', '<', $now)->where('fechaFinal', '>', $now)->where('aplicado', '=', FALSE)->first();


        if ($descuento->porcentaje >= 100) {
            $isBeca = TRUE;
        }



        $precioTemp = ($precio * (int) $descuento->porcentaje) / 100;
        $precio = $precio - $precioTemp;

        $descuentoArray = $descuento->toArray();

        $descuentoArray['cantidadDescontada'] = $precioTemp;

        $descuentoArray['precio'] = $precio;

        $descuentosAplicados[] = $descuentoArray;


        return [
            "precio" => $precio,
            "isBeca" => $isBeca,
            "descuentosAplicados" => $descuentosAplicados
        ];
    }

    private function _conversionDivisas($input) {

        $cl = new Currencylayer();

        $USD = $cl->convertUSDMXN();

        $input['exchangeRate'] = $USD;

        $input['precioFinal'] = $input['precioDescuento'] * $USD;

        $input['monedaFinal'] = 'MXN';

        return $input;
    }

    private function _getUnicVerificationCode() {



        $verificationCode = new VerificationCode();
        $referencia = $verificationCode->generateCode();


        $exists = Transaccion::where('referencia', $referencia)->first();

        while (!empty($exists->id)) {
            $referencia = $verificationCode->generateCode();
            $exists = Transaccion::where('referencia', $referencia)->first();
        }


        return $referencia;
    }

    private function _getDecodeResponseService($respuesta, User $user, Evento $evento) {



        $decrypt = new Decrypt();
        $xml = $decrypt->decrypt($respuesta);
        $xmlClose = '</xml>';

        $pos = strpos($xml, $xmlClose);

        if ($pos === false) {
            $xml = $xml . $xmlClose;
        }



        try {


            $pay = Parser::xml($xml);
            $pay['xmlResponse'] = $xml;
        } catch (\Nathanmac\Utilities\Parser\Exceptions\ParserException $ex) {

            $pay = null;

            $errorLog = new PagoLogError([
                'tipo' => $ex->getMessage(),
                'log' => $xml
            ]);
            $user->pagosError()->save($errorLog);

            $evento->pagosError()->save($errorLog);
        }


        return $pay;
    }

    private function _addTransaccionToPagoApproved(Transaccion $transaccion) {

        $transaccionArray = $transaccion->toArray();

        $evento = $transaccion->evento()->first();
        $user = $transaccion->user()->first();


        $pago = new Pago();


        $pago->fill($transaccionArray);

        $pago->save();


        $pago->transaccion()->save($transaccion);


        $user->pagos()->save($pago);

        $evento->pagos()->save($pago);



        return $pago;
    }

    private function _calcularDescuento($precio, $descuentos) {

        if (!empty($descuentos)) {
            foreach ($descuentos as $descuento) {

                if ($precio <= 0) {
                    return 0;
                }

                $precioTemp = ($precio * (int) $descuento->porcentaje) / 100;
                $precio = $precio - $precioTemp;
            }
        }

        return $precio;
    }

    private function _codigoDescuentoAplicado($pago) {

        $response = false;

        if (isset($pago->descuentos)) {

            foreach ($pago->descuentos as $descuento) {

                if ($descuento['tipo'] == 'descuentoCodigo') {

                    $descuentoCodigo = DescuentoCodigo::find($descuento['_id']);

                    $descuentoCodigo->aplicado = TRUE;

                    $descuentoCodigo->save();

                    $response = true;

                    break;
                }
            }
        }

        return $response;
    }

    private function _validaColoquios($coloquios) {

        $response = TRUE;

        foreach ($coloquios as $coloquioId) {

            $numRegistros = (int) Pago::whereIn('coloquios', [$coloquioId])->count();

            $coloquio = Coloquio::find($coloquioId);

            $capacidad = (int) $coloquio->capacidad;

            if ($capacidad > 0 && $numRegistros >= $capacidad) {

                $coloquio->estatus = FALSE;
                $coloquio->save();

                $response = FALSE;
            }
        }

        return $response;
    }

}
