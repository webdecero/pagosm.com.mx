<?php

namespace App\Http\Controllers\Manager;

//Providers
use Validator;
use Auth;
use MongoId;
use Excel;
use DB;
use Storage;
//use DatePeriod;
//Models
use App\Transaccion;
use App\Evento;
use App\Coloquio;
use App\Pago;
use App\User;
//Helpers and Class
use Illuminate\Http\Request;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\Utilidades;
use App\Http\Controllers\Pages\ComprarController;

class TransaccionController extends DataTableController {

    private $fieldsDetails = [
        '_id' => 'ID',
        'referencia' => 'Referencia',
        'response' => 'Respuesta',
        'isVentanilla' => 'Es Ventanilla',
        'evento_id' => 'Evento Id',
        'evento-titulo' => 'Evento',
        'precioInicial' => 'Precio Inicial',
        'monedaInicial' => 'Moneda Inicial',
        'user_id' => 'User ID',
        'user-nombre' => 'Nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'user-genero' => 'Genero',
        'user-pais' => 'Pais',
        'aut' => 'Num autorización',
        'error' => 'Error',
        'ccName' => 'Tarjetahabiente',
        'amount' => 'Monto tarjeta',
        'ccNum' => 'Tarjeta',
        'type' => 'Tipo',
        'precioDescuento' => 'Precio Descuento',
        'precioFinal' => 'Precio Final',
        'monedaFinal' => 'Moneda Final',
        'exchangeRate' => 'Tasa de cambio',
        'coloquios' => 'Coloquios',
        'descuentos' => 'Descuentos',
        'codigoDescuento' => 'Código Descuento',
        'tituloDescuento' => 'Título Descuento',
        'archivoVentanilla' => 'Archivo Ventanilla',
        'comentarioVentanilla' => 'Comentario Ventanilla',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    private $fieldsDetailsExcel = [
        '_id' => 'ID',
        'referencia' => 'Referencia',
        'response' => 'Respuesta',
        'isVentanilla' => 'Es Ventanilla',
        'evento_id' => 'Evento Id',
        'evento-titulo' => 'Evento',
        'precioInicial' => 'Precio Inicial',
        'monedaInicial' => 'Moneda Inicial',
        'user_id' => 'User ID',
        'user-nombre' => 'Nombre',
        'user-apellidoPaterno' => 'Apellido Paterno',
        'user-apellidoMaterno' => 'Apellido Materno',
        'user-email' => 'Email',
        'user-genero' => 'Genero',
        'user-pais' => 'Pais',
        'aut' => 'Num autorización',
        'error' => 'Error',
        'ccName' => 'Tarjetahabiente',
        'amount' => 'Monto tarjeta',
        'ccNum' => 'Tarjeta',
        'type' => 'Tipo',
        'precioDescuento' => 'Precio Descuento',
        'precioFinal' => 'Precio Final',
        'monedaFinal' => 'Moneda Final',
        'exchangeRate' => 'Tasa de cambio',
        'coloquios' => 'Coloquios',
        'descuentos' => 'Descuentos',
        'codigoDescuento' => 'Código Descuento',
        'tituloDescuento' => 'Título Descuento',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'url_details') {

            $record = route('dataTable.transaccion', [$item['_id']]);
        } else if (strpos($field, 'evento-') !== FALSE) {


            $part = explode("-", $field);

            $record = $item['evento']->{$part[1]};
        } else if (strpos($field, 'user-') !== FALSE) {


            $part = explode("-", $field);


            if ($part[1] == 'fechaNacimiento') {

                $record = isset($item['user']->{$part[1]}) ? date('Y-m-d', $item['user']->{$part[1]}->sec) : '';
            } else {

                $record = isset($item['user']->{$part[1]}) ? $item['user']->{$part[1]} : '';
            }
        } else if ($field == 'coloquios' && isset($item[$field])) {



            $coloquios = Coloquio::wherein('_id', $item['coloquios'])->get();

            $data = [];
            foreach ($coloquios as $coloquio) {
                $data[] = $coloquio->titulo;
            }

            $record = implode(',', $data);
        } else if ($field == 'descuentos') {

            $data = [];
            foreach ($item['descuentos'] as $descuento) {
                $data[] = $descuento['porcentaje'] . '%';
            }

            $record = implode(',', $data);
        } else if ($field == 'opciones') {

            if ($item['response'] == "incompleta") {


                $record = "<a href=" . route('transaccion.ventanilla', [(string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Pago ventanilla' > <i class='fa fa-cog'></i> </a>";
            }
        } else if ($field == 'codigoDescuento') {

            $codigoDescuento = false;

            foreach ($item['descuentos'] as $descuento) {

                if ($descuento['tipo'] == "descuentoCodigo") {

                    $codigoDescuento = TRUE;
                    break;
                }
            }

            $record = ($codigoDescuento) ? $descuento['codigo'] : '';
        } else if ($field == 'tituloDescuento') {

            $codigoDescuento = false;

            foreach ($item['descuentos'] as $descuento) {

                if ($descuento['tipo'] == "descuentoCodigo") {

                    $codigoDescuento = TRUE;
                    break;
                }
            }

            $record = ($codigoDescuento) ? $descuento['titulo'] : '';
        } else if ($field == 'response' && isset($item[$field])) {

            switch ($item[$field]) {
                case 'approved':
                    $record = 'aprobado';
                    break;
                case 'denied':
                    $record = 'denegado';
                    break;
                case 'error':
                    $record = 'error';
                    break;
                default:
                    $record = $item[$field];
            }
        } else if ($field == 'isVentanilla' && isset($item[$field])) {

            $record = ($item[$field]) ? 'Verdadero' : 'Falso';
        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {

        $record = $this->formatRecord($field, $item);


        return $record;
    }

    public function dataTable(Request $request, $id = false, $excel = false) {

        $searchable = [
            'query' => [
                '_id' => 'contains',
                'referencia' => 'contains',
                'precio' => 'contains',
                'aut' => 'contains'
            ],
            'response' => 'equal',
            'evento_id' => 'equal',
        ];



        $query = [];

        $data = [];

        $inputs = $request->all();



        $draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;

        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns'])))
            $this->generateOrder($inputs['order'], $inputs['columns']);

        $fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];




        if (!$id) {

            $this->paginate($page, $perPage);

            $query = $this->generateQuery($inputs, $searchable);
        } else {

            $isValid = MongoId::isValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json($data);
            }
        }

        $cursor = $this->_getConsultReport($query, true, false, true, 'transaccion');





        foreach ($cursor as $item) {

            $record = [];


            $item['user'] = User::find($item['user_id']);
            $item['evento'] = Evento::find($item['evento_id']);

            if ($excel) {

                foreach ($this->fieldsDetailsExcel as $k => $field) {

                    if ($field) {

                        $record[$field] = $this->formatRecordExcel($k, $item);
                    }
                }
            } else {

                foreach ($fields as $k => $fieldArray) {

                    $field = trim($fieldArray['data']);

                    if ($field) {

                        $record[$field] = $this->formatRecord($field, $item);
                    }
                }
            }

            $data[] = $record;
        }

        $recordsFiltered = $this->_getConsultReport($query, false, true, false, 'transaccion');

        $recordsTotal = count($data);

        $return = [
            "draw" => $draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];

        if ($excel) {
            $headersExcel = array_values($this->fieldsDetailsExcel);

            $fileName = 'reporte_transaccion_' . date('Y_m_d_H_i_s');

            Excel::create($fileName, function($excel) use($data, $headersExcel) {

                $excel->sheet('Transaccion', function($sheet) use($data, $headersExcel) {
// Manipulate first row
                    $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration) 
                    $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
                });
            })->export('xls');
        } else {
            return response()->json($return);
        }
    }

    private function getRecordById($id, $collectionName = 'transaccion') {

        $query = [
            '_id' => new MongoId($id)
        ];

        $cursor = DB::collection($collectionName)->raw(function($collection) use ($query) {

            return $collection->find($query);
        });

        $data = [];

        foreach ($cursor as $item) {

            $record = [];

            $item['user'] = User::find($item['user_id']);
            $item['evento'] = Evento::find($item['evento_id']);

            foreach ($this->fieldsDetails as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecord($k, $item);
                }
            }

            $data = $record;
        }

        return $data;
    }

    public function getPanel() {

        $data = array();

        $data['user'] = Auth::user();

        $data['eventos'] = Evento::all();



        return view('admin.transaccionPanel', $data);
    }

    public function getVentnillaForm($id = false) {


        $transaccion = Transaccion::findOrFail($id);

        $isValid = MongoId::isValid($id);

        if (!$isValid || !isset($transaccion->_id) || $transaccion->response != 'incompleta') {
            return redirect()->route("transaccion.panel")->with([
                        'error' => trans('mensajes.operacion.noEncotrado'),
            ]);
        }


        $data = array();

        $data['user'] = Auth::user();
        $data['transaccion'] = $transaccion;


        return view('admin.ventanillaForm', $data);
    }

    public function postVentnilla(Request $request) {

        $input = $request->all();


        $rules = array(
            '_id' => array('required', 'exists:transaccion,_id'),
            'archivoVentanilla' => array('required')
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("transaccion.ventanilla", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $transaccion = Transaccion::find($input['_id']);

            if (!$isValid || $transaccion->response != "incompleta") {
                return redirect()->route("transaccion.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }



            $extension = $request->file('archivoVentanilla')->extension();

            $path = 'comprobante/' . $transaccion->_id . '.' . $extension;



            Storage::put(
                    $path, file_get_contents($request->file('archivoVentanilla')->getRealPath())
            );

            $transaccion->archivoVentanilla = $path;
            $transaccion->comentarioVentanilla = $input['comentarioVentanilla'];


            $transaccion->isVentanilla = true;
            $transaccion->response = 'approved';
            $transaccion->save();



            $evento = $transaccion->evento()->first();
            $user = $transaccion->user()->first();

            $transaccionArray = $transaccion->toArray();



            $pago = new Pago();


            $pago->fill($transaccionArray);

            $pago->save();


            $pago->transaccion()->save($transaccion);


            $user->pagos()->save($pago);

            $evento->pagos()->save($pago);

            if (isset($pago->donativo)) {

                $comprar = new ComprarController();

                $comprar->addDonativoToPago($pago);
            }



            return redirect()->route("transaccion.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }
        
        
    }
    
    
    public function scriptProcedure(Request $request) {

        $input = $request->all();


        $rules = array(
            '_id' => array('required', 'exists:transaccion,_id'),
            'archivoVentanilla' => array('required')
        );



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route("transaccion.ventanilla", ['id' => $input['_id']])->withErrors($validator)->with([
                        'error' => trans('mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {

            $isValid = MongoId::isValid($input['_id']);
            $transaccion = Transaccion::find($input['_id']);

            if (!$isValid || $transaccion->response != "incompleta") {
                return redirect()->route("transaccion.panel")->with([
                            'error' => trans('mensajes.operacion.noEncotrado'),
                ]);
            }



            $extension = $request->file('archivoVentanilla')->extension();

            $path = 'comprobante/' . $transaccion->_id . '.' . $extension;



            Storage::put(
                    $path, file_get_contents($request->file('archivoVentanilla')->getRealPath())
            );

            $transaccion->archivoVentanilla = $path;
            $transaccion->comentarioVentanilla = $input['comentarioVentanilla'];


            $transaccion->isVentanilla = true;
            $transaccion->response = 'approved';
            $transaccion->save();



            $evento = $transaccion->evento()->first();
            $user = $transaccion->user()->first();

            $transaccionArray = $transaccion->toArray();



            $pago = new Pago();


            $pago->fill($transaccionArray);

            $pago->save();


            $pago->transaccion()->save($transaccion);


            $user->pagos()->save($pago);

            $evento->pagos()->save($pago);

            if (isset($pago->donativo)) {

                $comprar = new ComprarController();

                $comprar->addDonativoToPago($pago);
            }



            return redirect()->route("transaccion.panel")->with([
                        'mensaje' => trans('mensajes.registro.exito'),
            ]);
        }

}
