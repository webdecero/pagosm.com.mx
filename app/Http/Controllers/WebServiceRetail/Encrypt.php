<?php

namespace App\Http\Controllers\WebServiceRetail;

use Config;

class Encrypt {

    public function __construct($semilla) {
        
        $this->key = (Config::has("webServiceRetail.{$semilla}.key")) ? Config::get("webServiceRetail.{$semilla}.key") : null;
        $this->data0 = (Config::has("webServiceRetail.{$semilla}.data0")) ? Config::get("webServiceRetail.{$semilla}.data0") : null;


        $this->id_company = (Config::has("webServiceRetail.{$semilla}.id_company")) ? Config::get("webServiceRetail.{$semilla}.id_company") : null;
        $this->id_branch = (Config::has("webServiceRetail.{$semilla}.id_branch")) ? Config::get("webServiceRetail.{$semilla}.id_branch") : null;
        $this->country = (Config::has("webServiceRetail.{$semilla}.country")) ? Config::get("webServiceRetail.{$semilla}.country") : null;
        $this->user = (Config::has("webServiceRetail.{$semilla}.user")) ? Config::get("webServiceRetail.{$semilla}.user") : null;
        $this->pwd = (Config::has("webServiceRetail.{$semilla}.pwd")) ? Config::get("webServiceRetail.{$semilla}.pwd") : null;


        $this->merchant = (Config::has("webServiceRetail.{$semilla}.merchant")) ? Config::get("webServiceRetail.{$semilla}.merchant") : null;
        $this->tp_operation = (Config::has("webServiceRetail.{$semilla}.tp_operation")) ? Config::get("webServiceRetail.{$semilla}.tp_operation") : null;
        $this->crypto = (Config::has("webServiceRetail.{$semilla}.crypto")) ? Config::get("webServiceRetail.{$semilla}.crypto") : null;
        $this->type = (Config::has("webServiceRetail.{$semilla}.type")) ? Config::get("webServiceRetail.{$semilla}.type") : null;
        $this->version = (Config::has("webServiceRetail.{$semilla}.version")) ? Config::get("webServiceRetail.{$semilla}.version") : null;
        $this->url = (Config::has("webServiceRetail.{$semilla}.url")) ? Config::get("webServiceRetail.{$semilla}.url") : null;
        
        
    }

    public function generateEncryptXml($resource) {

//        $xml = '<xml>';
        

        $xml .= '<VMCAMEXM>';
        $xml .= '<business>';

        $xml .= '<id_company>' . $this->id_company . '</id_company>';
        $xml .= '<id_branch>' . $this->id_branch . '</id_branch>';
        $xml .= '<country>' . $this->country . '</country>';
        $xml .= '<user>' . $this->user . '</user>';
        $xml .= '<pwd>' . $this->pwd . '</pwd>';

        $xml .= '</business>';

        $xml .= '<transacction>';
        
        $xml .= '<merchant>' . $this->merchant . '</merchant>';
        $xml .= '<reference>' . $resource['referencia'] . '</reference>';
        $xml .= '<tp_operation>' . $this->tp_operation . '</tp_operation>';
        $xml .= '<creditcard>';
        $xml .= '<crypto>' . $this->crypto . '</crypto>';
        $xml .= '<type>' . $this->type . '</type>';
        $xml .= '<name>' . $resource['name'] . '</name>';
        $xml .= '<number>' . $resource['number'] . '</number>';
        $xml .= '<expmonth>' . $resource['expmonth'] . '</expmonth>';
        $xml .= '<expyear>' . $resource['expyear'] . '</expyear>';
        $xml .= '<cvv-csc>' . $resource['cvv'] . '</cvv-csc>';
        $xml .= '</creditcard>';
        $xml .= '<amount>'.number_format($resource['precioFinal'], 2, '.', '').'</amount>';
        $xml .= '<currency>'.$resource['monedaFinal'].'</currency>';
        $xml .= '<usrtransacction>'.$resource['idTransaccion'].'</usrtransacction>';
//        $xml .= '<version>' . $this->version . '</version>';
        
        $xml .= '</transacction>';
  $xml .= '</VMCAMEXM>';


        $xmla = $this->encrypt($xml);

        return $xmla;
    }

    public function encrypt($plaintext) {

        $key128 = $this->key;

        $plaintext = Encrypt::pkcs5_pad($plaintext, mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
        $iv_size = mcrypt_enc_get_iv_size($cipher);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_RANDOM);

        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        if (mcrypt_generic_init($cipher, hex2bin($key128), $iv) != -1) {
            $cipherText = mcrypt_generic($cipher, $plaintext);
            mcrypt_generic_deinit($cipher);
            $b64ciphertext = base64_encode($iv . $cipherText);
            $b64ciphertext = bin2hex($b64ciphertext);
            return $b64ciphertext;
        }
        return "";
    }

    private static function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

}
