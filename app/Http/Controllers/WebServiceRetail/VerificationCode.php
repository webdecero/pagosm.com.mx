<?php

namespace App\Http\Controllers\WebServiceRetail;


class VerificationCode {

//    protected $transaction;

    private $divisor = 10;
    private $minuendo = 10;



    public function generateCode($key ='', $length = 11) {
        /* Número de Referencia */
        $referencia = $this->generateRandom($key, $length);

        /* 1. De derecha a izquierda se multiplican cada uno de los dígitos de la referencia por los ponderadores 2

          y 1 alternativamente, siempre iniciando la secuencia con el número 2 aun cuando el número a

          multiplicar sea 0 deberá tomarse en cuenta. */


        $reverse = strrev($referencia);

        $sum = 0;

        for ($s = 0; $s < strlen($reverse); $s++) {
            $d = $reverse[$s];

            if ($s % 2 == 0) {
                $d = $d * 2;
            }
            $d = (string) $d;

            for ($dd = 0; $dd < strlen($d); $dd++) {
                $sum += $d[$dd];
            }
        }

        /* 3. El resultado de la suma indicada en el punto 2 se divide entre el Divisor. */

        $residuo = $sum % $this->divisor;



        /* 4. El residuo de la división del punto 3 se le resta al Minuendo. */

        $value = abs($this->minuendo - $residuo);

        /* Nota: Si el digito verificador sale 10 se tomará como dígito verificador el número 0 (cero) */

        if ($value == 10) {
            $verificator = 0;
        } else {
            $verificator = $value;
        }

        /* 5. El valor absoluto del resultado de la resta del punto 4 se le suma a la Constante 1. */

        $referencia = $referencia . $verificator;

        return $referencia;
    }

    private function generateRandom($key, $length) {
        $random_length = $length - strlen($key);

        $init = pow(10, ($random_length - 1));

        $end = pow(10, $random_length) - 1;

        $random = rand($init, $end);

        $code = $key . $random;

        return $code;
    }

}
