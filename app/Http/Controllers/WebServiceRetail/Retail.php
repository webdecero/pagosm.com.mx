<?php

namespace App\Http\Controllers\WebServiceRetail;

use Config;
use App\Http\Controllers\WebServiceRetail\AESEncriptacion;

class Retail {

    public function __construct($semilla) {

        $this->key = (Config::has("webServiceRetail.{$semilla}.key")) ? Config::get("webServiceRetail.{$semilla}.key") : null;
        $this->data0 = (Config::has("webServiceRetail.{$semilla}.data0")) ? Config::get("webServiceRetail.{$semilla}.data0") : null;


        $this->id_company = (Config::has("webServiceRetail.{$semilla}.id_company")) ? Config::get("webServiceRetail.{$semilla}.id_company") : null;
        $this->id_branch = (Config::has("webServiceRetail.{$semilla}.id_branch")) ? Config::get("webServiceRetail.{$semilla}.id_branch") : null;
        $this->country = (Config::has("webServiceRetail.{$semilla}.country")) ? Config::get("webServiceRetail.{$semilla}.country") : null;
        $this->user = (Config::has("webServiceRetail.{$semilla}.user")) ? Config::get("webServiceRetail.{$semilla}.user") : null;
        $this->pwd = (Config::has("webServiceRetail.{$semilla}.pwd")) ? Config::get("webServiceRetail.{$semilla}.pwd") : null;


        $this->merchant = (Config::has("webServiceRetail.{$semilla}.merchant")) ? Config::get("webServiceRetail.{$semilla}.merchant") : null;
        $this->tp_operation = (Config::has("webServiceRetail.{$semilla}.tp_operation")) ? Config::get("webServiceRetail.{$semilla}.tp_operation") : null;
        $this->crypto = (Config::has("webServiceRetail.{$semilla}.crypto")) ? Config::get("webServiceRetail.{$semilla}.crypto") : null;
        $this->type = (Config::has("webServiceRetail.{$semilla}.type")) ? Config::get("webServiceRetail.{$semilla}.type") : null;
        $this->version = (Config::has("webServiceRetail.{$semilla}.version")) ? Config::get("webServiceRetail.{$semilla}.version") : null;
        $this->url = (Config::has("webServiceRetail.{$semilla}.url")) ? Config::get("webServiceRetail.{$semilla}.url") : null;
    }

    public function generateEncryptXml($resource) {


        $xml = '<VMCAMEXM>';
        $xml .= '<business>';

        $xml .= '<id_company>' . $this->id_company . '</id_company>';
        $xml .= '<id_branch>' . $this->id_branch . '</id_branch>';
        $xml .= '<country>' . $this->country . '</country>';
        $xml .= '<user>' . $this->user . '</user>';
        $xml .= '<pwd>' . $this->pwd . '</pwd>';

        $xml .= '</business>';

        $xml .= '<transacction>';

        $xml .= '<merchant>' . $this->merchant . '</merchant>';
        $xml .= '<reference>' . $resource['referencia'] . '</reference>';
        $xml .= '<tp_operation>' . $this->tp_operation . '</tp_operation>';
        $xml .= '<creditcard>';
        $xml .= '<crypto>' . $this->crypto . '</crypto>';
        $xml .= '<type>' . $this->type . '</type>';
        $xml .= '<name>' . $resource['name'] . '</name>';
        $xml .= '<number>' . $resource['number'] . '</number>';
        $xml .= '<expmonth>' . $resource['expmonth'] . '</expmonth>';
        $xml .= '<expyear>' . $resource['expyear'] . '</expyear>';
        $xml .= '<cvv-csc>' . $resource['cvv'] . '</cvv-csc>';
        $xml .= '</creditcard>';
        $xml .= '<amount>' . number_format($resource['precioFinal'], 2, '.', '') . '</amount>';
        $xml .= '<currency>' . $resource['monedaFinal'] . '</currency>';
        $xml .= '<usrtransacction>' . $resource['idTransaccion'] . '</usrtransacction>';
//        $xml .= '<version>' . $this->version . '</version>';

        $xml .= '</transacction>';
        $xml .= '</VMCAMEXM>';


        $encryptXml = AESEncriptacion::encriptar($xml, $this->key);


        $xmlSend = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlSend .= '<pgs>';
        $xmlSend .= '<data0>' . $this->data0 . '</data0>';
        $xmlSend .= '<data>' . $encryptXml . '</data>';
        $xmlSend .= '</pgs>';



//        $xmla = AESEncriptacion::desencriptar($xmla, $this->key);

        return $xmlSend;
    }
    
    
    public function decryptXml($encodedInitialData) {
        
        
        return AESEncriptacion::desencriptar($encodedInitialData, $this->key);
        
        
    }
    
    
    public function generateConsultXml($resource) {
    


        $xml = '<user>' . $this->user . '</user>';
        $xml .= '<pwd>' . $this->pwd . '</pwd>';
        $xml .= '<id_company>' . $this->id_company . '</id_company>';
        $xml .= '<date>' . $resource['date'] . '</date>';
        $xml .= '<id_branch>' . $this->id_branch . '</id_branch>';
        $xml .= '<reference>' . $resource['referencia'] . '</reference>';


        $encryptXml = AESEncriptacion::encriptar($xml, $this->key);


        $xmlSend = '<?xml version="1.0" encoding="UTF-8"?>';
        $xmlSend .= '<pgs>';
        $xmlSend .= '<data0>' . $this->data0 . '</data0>';
        $xmlSend .= '<data>' . $encryptXml . '</data>';
        $xmlSend .= '</pgs>';



//        $xmla = AESEncriptacion::desencriptar($xmla, $this->key);

        return $xmlSend;
    }
    
    
    
    

}
