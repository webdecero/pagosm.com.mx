<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['middleware' => ['web'], 'namespace' => 'Pages'], function () {
    
    
    
    
    
    Route::get('sendxml', function () {

        $xmlSend = '<?xml version="1.0" encoding="UTF-8"?><pgs><data0>9265654606</data0><data>0WYT+pIQ2oxumH5fCqWgXQo4CdNeVjyJpJhYulRMA9HCq0IgKRorLQZKf+ EbdLvdr7X680SiWA/bukttvfdpgupFndSzaYnj4Y4J7Y9ETSh0GGJJ37Ok3XzOsB500HYgMsvWNyNqWFlr3bAvBLKFB4lIA Pr1ygwKrDN96/8GIJ+/+4gSD632UhxtbKSb2rFs1hLNhWZANaNOF8/f3apMgbC/KiUdATFiMPBZAzjFdv9iKqCR6GSHzjsJ MHKyfvq17pcMQEX/fnP2RXeslvqT3S/rl914cMYzCiE5P2QPGkHfMMv/CLqX38OVxr74c5kXrYl/P4ixPRfSOvA2sQDLlVYX J1/Zm93FnT8/B3Wcvd1SVxxxu6exXK8r6/2P+un8cmZ4IjsA31INlIaTiFl+0rgQ4pjSSFoclz47wmnBZjD0bm0zzGcsN1Frb4L xSYewm96Z5NTZEKrn61l7GUGfjOsyg4lQVfBh1gtkRXB+ELTOzkpLdaugGiCSKzt/lj9UmHllm0p0fP9bYHQijo+YWQo+md CPcrXmKryD4rJe9xIW7RCz9vI6tHg4q/ngiGSI4lR5tpjdJCujcU1XNXneaA5wcGiZf9XZ8v/pqT9MN2EJ0TJQgESCc35PDn OUzAyd60s/SRhutRdu4UbwQVJ/uTOpMlQgTz9hBkUBjR8iMAXpGkcKUssvhcFtTts/+yWKqCR+o1X6FossGkByFiwr4UG xoOoTRB81D/wooYW+ofjqTfkRkHlOIpc0nJZ/PbHkdrzNwe/wfReg9T8wZQ6Eb1f2qJHjguq+a591/Q50jHcolhAz0eo5gP20k GQkaTKZ</data></pgs>';


        $query = http_build_query(
                array(
                    'xml' => $xmlSend,
//                    'origen' => 'Servidor SM interfase'
                )
        );


        $requestsend = [
            'url' => 'https://qa3.mitec.com.mx/pgs/cobroXml',
            'content' => $query,
//            'headers' => [
//                "Content-type: application/x-www-form-urlencoded;charset=\"utf-8\"",
//                "Accept: text/plain",
//                "Cache-Control: no-cache",
//                "Pragma: no-cache",
//                "Content-length: " . strlen($query)
//            ]
        ];




        $response = \HttpClient::post($requestsend);
//        $headers = $response->headers();




        $respuesta = $response->content();
        
        
             $decrypt = new \App\Http\Controllers\WebServiceRetail\Retail('devqa');
             
            $xmlDecrypt = $decrypt->decryptXml($respuesta);


        dd($xmlDecrypt);
    });







    Route::get('curl', function () {



        $xmlSend = '<?xml version="1.0" encoding="UTF-8"?><pgs><data0>9265654606</data0><data>0WYT+pIQ2oxumH5fCqWgXQo4CdNeVjyJpJhYulRMA9HCq0IgKRorLQZKf+ EbdLvdr7X680SiWA/bukttvfdpgupFndSzaYnj4Y4J7Y9ETSh0GGJJ37Ok3XzOsB500HYgMsvWNyNqWFlr3bAvBLKFB4lIA Pr1ygwKrDN96/8GIJ+/+4gSD632UhxtbKSb2rFs1hLNhWZANaNOF8/f3apMgbC/KiUdATFiMPBZAzjFdv9iKqCR6GSHzjsJ MHKyfvq17pcMQEX/fnP2RXeslvqT3S/rl914cMYzCiE5P2QPGkHfMMv/CLqX38OVxr74c5kXrYl/P4ixPRfSOvA2sQDLlVYX J1/Zm93FnT8/B3Wcvd1SVxxxu6exXK8r6/2P+un8cmZ4IjsA31INlIaTiFl+0rgQ4pjSSFoclz47wmnBZjD0bm0zzGcsN1Frb4L xSYewm96Z5NTZEKrn61l7GUGfjOsyg4lQVfBh1gtkRXB+ELTOzkpLdaugGiCSKzt/lj9UmHllm0p0fP9bYHQijo+YWQo+md CPcrXmKryD4rJe9xIW7RCz9vI6tHg4q/ngiGSI4lR5tpjdJCujcU1XNXneaA5wcGiZf9XZ8v/pqT9MN2EJ0TJQgESCc35PDn OUzAyd60s/SRhutRdu4UbwQVJ/uTOpMlQgTz9hBkUBjR8iMAXpGkcKUssvhcFtTts/+yWKqCR+o1X6FossGkByFiwr4UG xoOoTRB81D/wooYW+ofjqTfkRkHlOIpc0nJZ/PbHkdrzNwe/wfReg9T8wZQ6Eb1f2qJHjguq+a591/Q50jHcolhAz0eo5gP20k GQkaTKZ</data></pgs>';

        $url = 'https://qa3.mitec.com.mx/pgs/cobroXml';

//        $url = "http://webdecero.com/api/v1/curl";

        $query = http_build_query(
                array(
                    'xml' => $xmlSend,
//                    'origen' => 'Servidor SM interfase'
                )
        );


        $headers = array(
            "Content-type: application/x-www-form-urlencoded;charset=\"utf-8\"",
            "Accept: text/plain",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: " . strlen($query),
        );






        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



        $output = curl_exec($ch);

        $info = curl_getinfo($ch);

        dd($output);

        curl_close($ch);
    });





    Route::get('sendxml', function () {

        $xmlSend = '<?xml version="1.0" encoding="UTF-8"?><pgs><data0>9265654606</data0><data>0WYT+pIQ2oxumH5fCqWgXQo4CdNeVjyJpJhYulRMA9HCq0IgKRorLQZKf+ EbdLvdr7X680SiWA/bukttvfdpgupFndSzaYnj4Y4J7Y9ETSh0GGJJ37Ok3XzOsB500HYgMsvWNyNqWFlr3bAvBLKFB4lIA Pr1ygwKrDN96/8GIJ+/+4gSD632UhxtbKSb2rFs1hLNhWZANaNOF8/f3apMgbC/KiUdATFiMPBZAzjFdv9iKqCR6GSHzjsJ MHKyfvq17pcMQEX/fnP2RXeslvqT3S/rl914cMYzCiE5P2QPGkHfMMv/CLqX38OVxr74c5kXrYl/P4ixPRfSOvA2sQDLlVYX J1/Zm93FnT8/B3Wcvd1SVxxxu6exXK8r6/2P+un8cmZ4IjsA31INlIaTiFl+0rgQ4pjSSFoclz47wmnBZjD0bm0zzGcsN1Frb4L xSYewm96Z5NTZEKrn61l7GUGfjOsyg4lQVfBh1gtkRXB+ELTOzkpLdaugGiCSKzt/lj9UmHllm0p0fP9bYHQijo+YWQo+md CPcrXmKryD4rJe9xIW7RCz9vI6tHg4q/ngiGSI4lR5tpjdJCujcU1XNXneaA5wcGiZf9XZ8v/pqT9MN2EJ0TJQgESCc35PDn OUzAyd60s/SRhutRdu4UbwQVJ/uTOpMlQgTz9hBkUBjR8iMAXpGkcKUssvhcFtTts/+yWKqCR+o1X6FossGkByFiwr4UG xoOoTRB81D/wooYW+ofjqTfkRkHlOIpc0nJZ/PbHkdrzNwe/wfReg9T8wZQ6Eb1f2qJHjguq+a591/Q50jHcolhAz0eo5gP20k GQkaTKZ</data></pgs>';


        $query = http_build_query(
                array(
                    'xml' => $xmlSend,
//                    'origen' => 'Servidor SM interfase'
                )
        );


        $requestsend = [
            'url' => 'https://qa3.mitec.com.mx/pgs/cobroXml',
            'content' => $query,
//            'headers' => [
//                "Content-type: application/x-www-form-urlencoded;charset=\"utf-8\"",
//                "Accept: text/plain",
//                "Cache-Control: no-cache",
//                "Pragma: no-cache",
//                "Content-length: " . strlen($query)
//            ]
        ];




        $response = \HttpClient::post($requestsend);
//        $headers = $response->headers();




        $respuesta = $response->content();
        
        
             $decrypt = new \App\Http\Controllers\WebServiceRetail\Retail('devqa');
             
            $xmlDecrypt = $decrypt->decryptXml($respuesta);


        dd($xmlDecrypt);
    });


    Route::get('decrypt', function () {
        
        
        
        
        
        

     
             $decrypt = new \App\Http\Controllers\WebServiceRetail\Retail('devqa');
             
            $xmlDecrypt = $decrypt->decryptXml($respuesta);


        dd($xmlDecrypt);
      
      

            $pay = Parser::xml($xml);
            
            $params = array_keys($pay);
            
            $str ='';
            foreach ($params as $value) {
                $str.="'". $value."', \n";
            }
            
            echo $str;
        
        
        
        
    });





//
//    Route::get('conversion', function () {
//
//        $request = [
//            'url' => 'http://apilayer.net/api/live',
//            'params' => [
//                'access_key' => '79e5714e65f43c063764059c38a242ad',
//                'currencies' => 'MXN',
//                'format' => 1
//            ]
//        ];
//
//        $response = HttpClient::get($request);
//
//        $content = $response->content();
//
//        dd($content);
//    });
//    Route::get('ip', function () {
//     $ip = GeoIP::getLocation();
//     
//     dd($ip);
//});
});

