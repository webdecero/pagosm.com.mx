<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::group(['middleware' => ['web'], 'namespace' => 'Pages'], function () {


//    Home
    Route::get('/', ['as' => 'page.inicio', 'uses' => 'PageController@viewInicio']);


//    Registro

    Route::get('registro/{idEvento?}', ['as' => 'page.registro', 'uses' => 'RegistroController@viewInicio']);
    Route::post('registro/guarda', ['as' => 'page.usuario.guarda', 'uses' => 'RegistroController@postGuarda']);


    Route::post('login/valid', ['as' => 'page.usuario.login', 'uses' => 'RegistroController@postLogin']);
    Route::get('logout', ['as' => 'page.usuario.logout', 'uses' => 'RegistroController@getLogout']);

    Route::post('login/recuperar', ['as' => 'page.usuario.recuperar', 'uses' => 'RegistroController@postRecovery']);


    Route::get('eventos/{slug}', ['as' => 'page.evento', 'uses' => 'RegistroController@viewInicio']);


    Route::get('aviso-privacidad', ['as' => 'page.aviso', 'uses' => 'PageController@viewAviso']);
    Route::get('terminos-condiciones', ['as' => 'page.terminos', 'uses' => 'PageController@viewTerminos']);


    Route::get('recibo-donativo', ['as' => 'page.recibo.info', 'uses' => 'PageController@viewRecibo']);


    Route::post('contacto/form', ['as' => 'page.contaco.form', 'uses' => 'PageController@enviarEmailContacto']);





    Route::group(['middleware' => ['auth']], function () {


        Route::get('comprar/{idEvento}', ['as' => 'page.comprar', 'uses' => 'ComprarController@viewInicio']);


        Route::post('pagar/dispatcher', ['as' => 'page.dispatcher.pagar', 'uses' => 'ComprarController@dispatcherPagar']);


        Route::get('pagar', ['as' => 'page.pagar', 'uses' => 'ComprarController@viewPagar']);


        Route::post('recibo/{idTransaccion}', ['as' => 'page.recibo', 'uses' => 'ComprarController@responseRecibo']);


        Route::get('instrucciones/{referencia}', ['as' => 'page.instrucciones', 'uses' => 'ComprarController@getInstrucciones']);


        Route::get('comprobante/{idPago}', ['as' => 'page.comprobante', 'uses' => 'ComprarController@getComprobante']);


        Route::get('comprobante/enviar/{idPago}', ['as' => 'page.comprobante.enviar', 'uses' => 'ComprarController@sendComprobante']);
    });



    Route::group(['prefix' => 'compras', 'middleware' => ['auth']], function () {


        Route::get('/', ['as' => 'page.historial', 'uses' => 'DonativoController@viewInicio']);

        Route::get('donativo/{idPago}', ['as' => 'page.donativo', 'uses' => 'DonativoController@getDonativoForm']);

        Route::post('donativo', ['as' => 'page.donativo.guarda', 'uses' => 'DonativoController@postDonativo']);

       

        Route::get('streaming/{idPago}', ['as' => 'page.streaming', 'uses' => 'PageController@viewStreaming']);
    });


// Route::get('donativos/update', ['as' => 'page.donativos.update', 'uses' => 'DonativoController@updateDonativos']);
//Route::get('soap/{idTransaccion}', ['as' => 'page.callBackRecibo', 'uses' => 'ComprarController@callBackRecibo']);

    
    
    
});






Route::group(['prefix' => 'manager', 'middleware' => ['web'], 'namespace' => 'Manager'], function () {



//Login
    Route::get('/', ['as' => 'home', 'uses' => 'UserController@viewLogin']);

    Route::post('login/valid', ['as' => 'login', 'uses' => 'UserController@postLogin']);

    Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@getLogout']);



//Usuarios

    Route::group(['prefix' => 'usuario', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'usuario.panel', 'uses' => 'UserController@getUserPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTableUsuarios', 'uses' => 'UserController@dataTableUsuarios'));


        Route::get('crea', ['as' => 'usuario.form', 'uses' => 'UserController@getAddUserForm']);

        Route::post('guarda', ['as' => 'usuario.guarda', 'uses' => 'UserController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'usuario.formEdit', 'uses' => 'UserController@getEditUserForm']);

        Route::post('actualiza', ['as' => 'usuario.actualiza', 'uses' => 'UserController@postEdit']);


        Route::get('elimina/{id}', ['as' => 'usuario.elimina', 'uses' => 'UserController@getDelete']);
    });



//Newsletter

    Route::group(['prefix' => 'newsletter', 'middleware' => [ 'authManager', 'editRoute']], function () {

        Route::get('panel', ['as' => 'newsletter.panel', 'uses' => 'NewsletterController@getPanel']);

        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTableNewsletter', 'uses' => 'NewsletterController@dataTable'));

        Route::get('elimina/{id}', ['as' => 'newsletter.elimina', 'uses' => 'NewsletterController@getDelete']);


        Route::post('newsletter/add', ['as' => 'newsletter.add', 'uses' => 'NewsletterController@postAdd']);
    });





    //Coloquio

    Route::group(['prefix' => 'coloquio', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'coloquio.panel', 'uses' => 'ColoquioController@getColoquioPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTableColoquio', 'uses' => 'ColoquioController@dataTableColoquio'));


        Route::get('crea/{eventoId}', ['as' => 'coloquio.form', 'uses' => 'ColoquioController@getAddColoquioForm']);

        Route::post('guarda', ['as' => 'coloquio.guarda', 'uses' => 'ColoquioController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'coloquio.formEdit', 'uses' => 'ColoquioController@getEditColoquioForm']);

        Route::post('actualiza', ['as' => 'coloquio.actualiza', 'uses' => 'ColoquioController@postEdit']);

        Route::get('elimina/{id}', ['as' => 'coloquio.elimina', 'uses' => 'ColoquioController@getDelete']);
    });



    //Evento

    Route::group(['prefix' => 'evento', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'evento.panel', 'uses' => 'EventoController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTableEvento', 'uses' => 'EventoController@dataTable'));


        Route::get('crea', ['as' => 'evento.form', 'uses' => 'EventoController@getAddForm']);

        Route::post('guarda', ['as' => 'evento.guarda', 'uses' => 'EventoController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'evento.formEdit', 'uses' => 'EventoController@getEditForm']);

        Route::post('actualiza', ['as' => 'evento.actualiza', 'uses' => 'EventoController@postEdit']);

        Route::get('elimina/{id}', ['as' => 'evento.elimina', 'uses' => 'EventoController@getDelete']);
    });





    //Descuento Fecha

    Route::group(['prefix' => 'descuento/fecha', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'descuento.fecha.panel', 'uses' => 'DescuentoFechaController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.descuento.fecha', 'uses' => 'DescuentoFechaController@dataTable'));


        Route::get('crea', ['as' => 'descuento.fecha.form', 'uses' => 'DescuentoFechaController@getAddForm']);

        Route::post('guarda', ['as' => 'descuento.fecha.guarda', 'uses' => 'DescuentoFechaController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'descuento.fecha.formEdit', 'uses' => 'DescuentoFechaController@getEditForm']);

        Route::post('actualiza', ['as' => 'descuento.fecha.actualiza', 'uses' => 'DescuentoFechaController@postEdit']);

        Route::get('elimina/{id}', ['as' => 'descuento.fecha.elimina', 'uses' => 'DescuentoFechaController@getDelete']);
    });





    //Descuento Pais

    Route::group(['prefix' => 'descuento/pais', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'descuento.pais.panel', 'uses' => 'DescuentoPaisController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.descuento.pais', 'uses' => 'DescuentoPaisController@dataTable'));


        Route::get('crea', ['as' => 'descuento.pais.form', 'uses' => 'DescuentoPaisController@getAddForm']);

        Route::post('guarda', ['as' => 'descuento.pais.guarda', 'uses' => 'DescuentoPaisController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'descuento.pais.formEdit', 'uses' => 'DescuentoPaisController@getEditForm']);

        Route::post('actualiza', ['as' => 'descuento.pais.actualiza', 'uses' => 'DescuentoPaisController@postEdit']);

        Route::get('elimina/{id}', ['as' => 'descuento.pais.elimina', 'uses' => 'DescuentoPaisController@getDelete']);
    });





    //Descuento Codigo

    Route::group(['prefix' => 'descuento/codigo', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'descuento.codigo.panel', 'uses' => 'DescuentoCodigoController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.descuento.codigo', 'uses' => 'DescuentoCodigoController@dataTable'));


        Route::get('crea', ['as' => 'descuento.codigo.form', 'uses' => 'DescuentoCodigoController@getAddForm']);

        Route::post('guarda', ['as' => 'descuento.codigo.guarda', 'uses' => 'DescuentoCodigoController@postGuarda']);

        Route::get('edita/{id}', ['as' => 'descuento.codigo.formEdit', 'uses' => 'DescuentoCodigoController@getEditForm']);

        Route::post('actualiza', ['as' => 'descuento.codigo.actualiza', 'uses' => 'DescuentoCodigoController@postEdit']);

        Route::get('elimina/{id}', ['as' => 'descuento.codigo.elimina', 'uses' => 'DescuentoCodigoController@getDelete']);


        Route::post('filtro', ['as' => 'descuento.codigo.filtro', 'uses' => 'DescuentoCodigoController@postFiltros']);
    });







    //Transaccion

    Route::group(['prefix' => 'transaccion', 'middleware' => ['authManager', 'editRoute']], function() {

        
        Route::get('script', ['as' => 'transaccion.script', 'uses' => 'TransaccionController@scriptPostventanilla']);
        Route::get('panel', ['as' => 'transaccion.panel', 'uses' => 'TransaccionController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.transaccion', 'uses' => 'TransaccionController@dataTable'));

        Route::get('ventanilla/{id}', ['as' => 'transaccion.ventanilla', 'uses' => 'TransaccionController@getVentnillaForm']);

        Route::post('ventanilla/actualiza', ['as' => 'transaccion.ventanilla.actualiza', 'uses' => 'TransaccionController@postVentnilla']);
    });



    //Pago

    Route::group(['prefix' => 'pago', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('panel', ['as' => 'pago.panel', 'uses' => 'PagoController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.pago', 'uses' => 'PagoController@dataTable'));
    });



    //Donativo

    Route::group(['prefix' => 'donativo', 'middleware' => ['authManager', 'editRoute']], function() {
        
        Route::get('script', ['as' => 'donativo.script', 'uses' => 'DonativoController@scriptPostventanilla']);

        Route::get('panel', ['as' => 'donativo.panel', 'uses' => 'DonativoController@getPanel']);


        Route::post('dataTable/{id?}/{excel?}', array('as' => 'dataTable.donativo', 'uses' => 'DonativoController@dataTable'));
    });



    //Paginas

    Route::group(['prefix' => 'paginas', 'middleware' => ['authManager', 'editRoute']], function() {

        Route::get('formEdit/{clave}', ['as' => 'paginas.formEdit', 'uses' => 'PageController@getEditForm']);

        Route::post('actualiza', ['as' => 'paginas.actualiza', 'uses' => 'PageController@postEdit']);
    });




    //Image

    Route::group(['prefix' => 'image', 'middleware' => ['authManager']], function() {

        Route::post('load', ['as' => 'image.load', 'uses' => 'ImageController@load']);
    });
});

