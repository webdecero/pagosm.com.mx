<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pago extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'pagos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'referencia',
        'response',
        
        'foliocpagos',
        'cd_response',
        'cd_error',
        'time',
        'date',
        'nb_company',
        'nb_merchant',
        'nb_street',
        'tp_operation',
        'voucher',
        'voucher_comercio',
        'voucher_cliente',
        'friendly_response',
        
        'aut',
        'error',
        'ccName',
        'amount',
        'ccNum',
        'type',
        
        'isBeca',
        'isVentanilla',
        'coloquios',
        'descuentos',
        'precioInicial',
        'precioDescuento',
        'precioFinal', 'monedaInicial', 'monedaFinal', 'exchangeRate',
        'xmla',
        'xmlResponse',
        'webpay',
        'estatus',
        'version',
		'donativo'
    ];


//    protected $appends = [  'status' , 'key'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function evento() {
        return $this->belongsTo('App\Evento');
    }

    public function transaccion() {
        return $this->hasOne('App\Transaccion');
    }

    public function donativos() {
        return $this->hasOne('App\Donativo');
    }

}
