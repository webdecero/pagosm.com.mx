
<!-- BASIC PAGE NEEDS -->
<meta charset="utf-8">
<base href="{{ url('/') }}/" >
<meta name="csrf-token" content="{!! csrf_token() !!}">

<title> Fundación SM | pagosm.com.mx</title>        
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Harry Boo">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- Favicons -->
<link rel="shortcut icon" href="sm/assets/img/favicon.png">
<link rel="apple-touch-icon" href="sm/assets/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="sm/assets/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="sm/assets/img/apple-touch-icon-114x114.png">

<!-- Load Core CSS 
=====================================-->
<link rel="stylesheet" href="sm/assets/css/core/bootstrap.min.css">
<link rel="stylesheet" href="sm/assets/css/core/animate.min.css">

<!-- Load Main CSS 
=====================================-->
<link rel="stylesheet" href="sm/assets/css/main/main.css">
<link rel="stylesheet" href="sm/assets/css/main/setting.css">
<link rel="stylesheet" href="sm/assets/css/main/hover.css">

<!-- Load Magnific Popup CSS 
=====================================-->
<link rel="stylesheet" href="sm/assets/css/magnific/magic.min.css">        
<link rel="stylesheet" href="sm/assets/css/magnific/magnific-popup.css">              
<link rel="stylesheet" href="sm/assets/css/magnific/magnific-popup-zoom-gallery.css">

<!-- Load OWL Carousel CSS 
=====================================-->
<link rel="stylesheet" href="sm/assets/css/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="sm/assets/css/owl-carousel/owl.theme.css">
<link rel="stylesheet" href="sm/assets/css/owl-carousel/owl.transitions.css">

<!-- Load Color CSS - Please uncomment to apply the color.
=====================================      
<link rel="stylesheet" href="sm/assets/css/color/blue.css">
<link rel="stylesheet" href="sm/assets/css/color/brown.css">
<link rel="stylesheet" href="sm/assets/css/color/cyan.css">
<link rel="stylesheet" href="sm/assets/css/color/dark.css">
<link rel="stylesheet" href="sm/assets/css/color/green.css">
<link rel="stylesheet" href="sm/assets/css/color/orange.css">
<link rel="stylesheet" href="sm/assets/css/color/purple.css">
<link rel="stylesheet" href="sm/assets/css/color/pink.css">
<link rel="stylesheet" href="sm/assets/css/color/red.css">
<link rel="stylesheet" href="sm/assets/css/color/yellow.css">-->
<link rel="stylesheet" href="sm/assets/css/color/sm.css">

<!-- Load Fontbase Icons - Please Uncomment to use linea icons
=====================================       
<link rel="stylesheet" href="sm/assets/css/icon/linea-arrows-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-basic-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-basic-elaboration-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-ecommerce-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-music-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-software-10.css">
<link rel="stylesheet" href="sm/assets/css/icon/linea-weather-10.css">--> 
<link rel="stylesheet" href="sm/assets/css/icon/font-awesome.css">
<link rel="stylesheet" href="sm/assets/css/icon/et-line-font.css">

<!-- Load JS
HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
WARNING: Respond.js doesn't work if you view the page via file://
=====================================-->

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37595349-4', 'auto');
  ga('send', 'pageview');

</script>



<!-- LIB CSS Opcionales que se remplazan en vistas-->
@yield('css-lib')

