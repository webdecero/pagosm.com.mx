@if(session()->has('error'))
<!-- ======================================= Mensajes: Error Personalizados ======================================= -->
<div class="modal fade modalMensaje" tabindex="-1" role="dialog" aria-labelledby="tituo error" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content  text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans( 'mensajes.alerta.titulo' )}}</h4>
            </div>
            <div class="modal-body ">
                <h5>{{trans( 'mensajes.alerta.detecto' )}}</h5>
                <p class="text-danger c-font-red" >{!! session('error') !!}</p>
                @if($errors->has())
                <ul>
                    @foreach ($errors->all() as $error)
                    <li class="text-danger c-font-red" >
                        {{ $error }} 
                    </li>
                    @endforeach
                </ul>
                @endif

                <a class="btn btn-outline btn-success" data-dismiss="modal" href="#">{{trans( 'etiquetas.modal.cerrar' )}}</a>
            </div>
        </div>
    </div>
</div>


@endif
@if(session()->has('mensaje'))
<!-- ======================================= Mensajes: Mensaje ======================================= -->
<div class="modal fade modalMensaje"  tabindex="-1" role="dialog" aria-labelledby="titulo mensaje" aria-hidden="true" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >{{trans( 'mensajes.mensaje.titulo' )}}</h4>
            </div>
            <div class="modal-body ">
                <p>{!! session('mensaje') !!}</p>
                <a class="btn btn-outline btn-success" data-dismiss="modal" href="#">{{trans( 'etiquetas.modal.cerrar' )}}</a>
            </div>
        </div>
    </div>
</div>
@endif




<!-- Login Modal Dialog Box
       ===================================== -->
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title text-center"><i class="fa fa-lock fa-fw"></i> Acceder</h5>
            </div>
            <div class="modal-body pt25">
                <!--                <div class="text-center">
                                    <span class="color-dark">Sign in with your social account</span><br>
                                    <a href="#">
                                        <img src="sm/assets/img/other/fbconnect.png" alt="" class="mt10 mb10">
                                    </a>
                                    <a href="#">
                                        <img src="sm/assets/img/other/twconnect.png" alt="" class="mt10 mb10"><br><br>
                                    </a>
                                    <span class="color-dark">- Or sign in with your email address -</span>
                                </div>-->

                @if(session()->has('errorLogin'))
                <div class="text-center">


                    <!--<span class="color-dark errorLogin">{{trans( 'mensajes.alerta.detecto' )}}</span><br>-->
                    <p class="color-danger errorLogin" >{!! session('errorLogin') !!}</p>
                    @if($errors->has())
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li class="text-danger c-font-red" >
                            {{ $error }} 
                        </li>
                        @endforeach
                    </ul>
                    @endif



                </div>

                @endif

                <form id="form-login"class="form-horizontal mt25 ml50" method="Post" action="{{route('page.usuario.login')}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

<!--                    <input type="hidden" name="route" value="{{Request::url()}}">-->

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="button button-pasific button-block">Entrar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-gray">
                <span class="pull-left">¿No estas registrado? <a href="{{route('page.registro')}}" class="color-dark"> <b>Registrate aquí </b></a></span>
                <span class="pull-right">¿Recuperar contraseña? <a href="#" data-toggle="modal" data-target="#recuperarModal" class="color-dark"> <b>Recuperar</b></a></span>
                
                
            </div>
        </div>

    </div>
</div>





<!-- Recuperar Modal Dialog Box
       ===================================== -->
<div id="recuperarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title text-center"><i class="fa fa-unlock fa-fw"></i> Recuperar Password</h5>
            </div>
            <div class="modal-body pt25">
                

                @if(session()->has('errorRecovery'))
                <div class="text-center">

                    <p class="color-danger errorRecovery" >{!! session('errorRecovery') !!}</p>
                    @if($errors->has())
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li class="text-danger c-font-red" >
                            {{ $error }} 
                        </li>
                        @endforeach
                    </ul>
                    @endif


                </div>

                @endif

                <form id="form-login"class="form-horizontal mt25 ml50" method="Post" action="{{route('page.usuario.recuperar')}}">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

<!--                    <input type="hidden" name="route" value="{{Request::url()}}">-->

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="button button-pasific button-block">Recuperar</button>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-gray">
                <span class="text-center">¿No estas registrado? <a href="{{route('page.registro')}}" class="color-dark"> <b>Registrate aquí </b></a></span>
            </div>
        </div>

    </div>
</div>


<!-- Search Modal Dialog Box
===================================== -->
<div id="searchModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-gray">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title text-center"><i class="fa fa-search fa-fw"></i> Search here</h5>
            </div>
            <div class="modal-body">                        
                <form action="#" class="inline-form">
                    <input type="text" class="modal-search-input" autofocus>
                </form>
            </div>
            <div class="modal-footer bg-gray">
                <span class="text-center"><a href="#" class="color-dark">Advanced Search</a></span>
            </div>
        </div>

    </div>
</div>    