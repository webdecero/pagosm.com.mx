<!-- Page Loader
===================================== -->
<div id="pageloader">
    <div class="loader-item">
        <img src="sm/assets/img/other/puff.svg" alt="page loader">
    </div>
</div>

<!-- Navigation Area
      ===================================== -->
<nav class="navbar navbar-pasific navbar-op navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="#page-top">

            <!--            logo-negro.png
            logo-normal.png-->
            <img src="sm/img/logo-negro.png" alt="logo">

        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>



    <div class="navbar-collapse collapse navbar-right">
        <ul class="nav navbar-nav mr20">
            <!--<li class="hidden"><a href="#page-top"></a></li>-->




            <li><a href="{{ route('page.inicio') }}#eventos" @if('page.inicio' == Route::currentRouteName()) data-target="#eventos" @endif  >Eventos</a></li>                      
            <li><a href="{{ route('page.inicio') }}#contacto" @if('page.inicio' == Route::currentRouteName()) data-target="#contacto" @endif  >Contacto</a></li>                    


            @if (!Auth::check()) 
            <li><a href="{{ route('page.registro') }}">Registro</a></li> 
            @endif

            <li><a href="{{ route('page.recibo.info') }}">Recibo de donativo</a></li>  

            @if (Auth::check()) 
            <li><a href="{{ route('page.historial') }}">Historial de Compras</a></li>    

            <li><a href="{{ route('page.usuario.logout') }}" >Salir <i class="fa fa-sign-out"></i></a></li>

            @else
            <li><a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-lock fa-fw"></i>Iniciar Sesión</a></li>
            @endif







            <!--<li><a href="#" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search fa-fw"></i></a></li>-->
        </ul>
    </div>



</nav>


