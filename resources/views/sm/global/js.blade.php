@section('js-default')

<!-- JQuery Core
        =====================================-->
        <script src="sm/assets/js/core/jquery.min.js"></script>
        <script src="sm/assets/js/core/bootstrap.min.js"></script>
      
<script src="sm/assets/js/jquery-validation/dist/jquery.validate.js"></script>
<script src="sm/assets/js/jquery-validation/src/localization/messages_es.js"></script>

        
       <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57461c186e488a45"></script>
 
        
        

@show

<!-- LIB JS Opcionales que se remplazan en vistas-->
@yield('js-lib')


<!-- script JS -->
@section('js-script')
<!-- MAIN JAVASCRIPT -->
        <script src="sm/assets/js/main/main.js"></script>   
@show








