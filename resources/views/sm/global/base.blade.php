<!DOCTYPE html>
<html  lang="{{App::getLocale()}}" >
    <head>
        @include('sm.global.head')

</head>

<body  id="page-top" data-spy="scroll" data-target=".navbar" data-offset="100" class="{{ $classBody or '' }}" >

   <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
   
   
        @section('nav')

        @include('sm.global.nav') 
        
        @show

        
        @yield('contenido')


        @section('footer')
        <!-- Footer -->
        @include('sm.global.footer')
        <!-- Fin de footer -->
        @show


        @include('sm.global.mensajes') 
        
        
        @include('sm.global.js')

		
    </body>
</html>




