<!-- footer Area
     ===================================== -->
<div id="footer" class="footer-two pt50">
    <div class="container-fluid bb-solid-1">
        <div class="container pb35">
            <div class="row">

                <!-- footer about start -->
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <h6 class="font-montserrat text-uppercase color-dark">Acerca del SIEI Continuo.</h6>
                    <p>En el marco del 10º SIEI, Fundación SM lanza un espacio de reflexión permanente titulado SIEI Continuo, el cual ofrece formación a lo largo de todo el año –presencial y vía streaming- con la finalidad de crear una comunidad internacional conformada por profesionales que alienten la discusión, el análisis y la reflexión en torno a los temas más relevantes de la educación.
                    <br>
                    Bienvenida a esta primera sesión del SIEI Continuo titulada Gestión del cambio. Hacia la autonomía escolar.
                    <br>
                    ¡Que la disfrutes!</p>
                </div>
                <!-- footer about end -->




                <!-- footer menu one start -->
<!--                <div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-4">
                    <h6 class="font-montserrat text-uppercase color-dark">Menu</h6>
                    <ul class="no-icon-list">
                        <li><a href="{{ route('page.inicio') }}" >Home</a></li>
                        <li><a href="{{ route('page.registro') }}" >Registro</a></li>
                        <li><a href="{{ route('page.inicio') }}#eventos" >Eventos</a></li>
                        <li><a href="{{ route('page.inicio') }}#contacto" >Contacto</a></li>
                    </ul>
                </div>-->
                <!-- footer menu one end -->
                <!--                        
                                         footer menu two start 
                                        <div class="col-md-2 col-sm-3 col-xs-4">
                                            <h6 class="font-montserrat text-uppercase color-dark">Learn more</h6>
                                            <ul class="no-icon-list">
                                                <li><a href="#">Tour</a></li>
                                                <li><a href="#">Pricing</a></li>
                                                <li><a href="#">New Features</a></li>
                                                <li><a href="#">Payment</a></li>
                                            </ul>
                                        </div>
                                         footer menu two end 
                                        
                                         footer menu three start 
                                        <div class="col-md-2 col-sm-3 col-xs-4">
                                            <h6 class="font-montserrat text-uppercase color-dark">Support</h6>
                                            <ul class="no-icon-list">
                                                <li><a href="#">FAQs</a></li>
                                                <li><a href="#">Knowledgebase</a></li>
                                                <li><a href="#">Forum</a></li>
                                                <li><a href="#">Contact Us</a></li>
                                            </ul>
                                        </div>
                                         footer menu three end 
                                        
                -->

                <!-- footer social icons start -->
                <div class=" col-md-offset-2 col-md-3 col-sm-3 col-xs-12">
                    <h6 class="font-montserrat text-uppercase color-dark">Redes Sociales</h6>
                    <div class="social social-two">
                        <a href="https://twitter.com/edicionesSMmx" target="_blank" ><i class="fa fa-twitter color-blue"></i></a>
                        <a href="https://www.facebook.com/literaturaSMmexico" target="_blank" ><i class="fa fa-facebook color-primary"></i></a>
<!--                                <a href="#"><i class="fa fa-linkedin color-blue"></i></a><br><br>
                        <a href="#"><i class="fa fa-github color-dark"></i></a>
                        <a href="#"><i class="fa fa-pinterest color-red"></i></a>-->
                    </div>
                </div>
                <!-- footer social icons end -->

            </div><!-- row end -->
        </div><!-- container end -->
    </div><!-- container-fluid end -->

    <div class="container-fluid pt20">
        <div class="container">
            <div class="row">

                <!-- copyright start -->
                <div class="col-md-6 col-sm-6 col-xs-6 pull-left">
                    <p><?php echo date("Y"); ?> <a href="http://www.fundacion-sm.org.mx/">Fundación SM. </a> Derechos Reservados.</p>
                </div>
                <!-- copyright end -->

                <!-- footer bottom start -->
                <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                    <p class="text-right">
                        <a href="{{ route('page.aviso') }}" target="_blank" class="mr20">Aviso de privacidad</a>
                        <a href="{{ route('page.terminos') }}" target="_blank" class="mr20">Términos y condiciones </a>
                        <!--<a href="#" class="mr50">Site Map</a>-->
                    </p>
                </div>
                <!-- footer bottom end -->

            </div><!-- row end -->
        </div><!-- container end -->
    </div><!-- container-fluid end -->
</div><!-- footer end -->