@extends('sm.global.base' )
@section('nav')

@include('sm.global.nav-interno') 

@stop

@section('contenido')


<!-- Subheader Area
     ===================================== -->
<header class="bg-grad-stellar mt70">

    <div class="container">
        <div class="row mt20 mb30">
            <div class="col-md-6 text-left">
                <h3 class="color-light text-uppercase animated" data-animation="fadeInUp" data-animation-delay="100">{{ $evento->titulo}}<small class="color-light alpha7">{{ $evento->codigo}}</small></h3>
            </div>
            <div class="col-md-6 text-right pt35">
                <ul class="breadcrumb">
                    <li><a href="{{ route('page.inicio') }}">Home</a></li>
                    <li><a href="{{ route('page.inicio') }}#eventos">Eventos</a></li>
                    <li>{{ $evento->titulo}}</li>
                </ul>
            </div>
        </div>
    </div>

</header>


<!-- Blog Area
===================================== -->
<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2 col-xs-12">

                <div class="panel panel-success">
                    <div class="panel-body">
                        <h3>Operación finalizada con exito</h3>

                       

                        <a  href="{{route('page.comprobante', ['idPago'=> $pago->id])}}" class="btn btn-default">Descargar Comprobante<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a>

                        <a href="{{route('page.inicio',["#eventos"])}}" target="_top"  class="btn btn-default" > <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Finalizar</a>
                       

                    </div>
                </div>




            </div>        


        </div>



    </div>
</section>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



