@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')




<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center mb30">Historial de compras realizadas
                
                    <small>{{$user->email}}</small>
                </h4>
                
                
                <table class="table table-striped">

                    <thead>
                        <tr>
                            <th>Referencia</th>
                            <th>Evento</th>
                            <th>Tarjeta</th>
                            <th>Precio</th>
                            <th>Moneda</th>
                            <th>Fecha</th>
                            <th class="text-center">Transmisión de Video </th>
                            
                        </tr>
                    </thead>

                    <tbody>


                            @foreach($pagos as $pago)

                        <tr>



                            <td>{{$pago->referencia}}</td>

                            <td>{{$pago->evento()->first()->titulo}}</td>
                            <td>{{$pago->ccNum}}</td>
                            <td>{{number_format($pago->precioFinal , 2, '.', '')}}</td>



                            <td>{{$pago->monedaFinal}}</td>


                            <td>{{$pago->created_at->format('d-m-Y')}}</td>

                            
                            
                            
                            
                            
                            
                            <td>
                                
                                
                                @if($pago->streamingActiva == false )
                                
                                <span  class='center-block text-center' data-toggle='tooltip' title='No Disponible' > <i class='fa fa-ban'></i> </span>
                                
                                @elseif ($pago->streamingMostrarFecha == true)

                                <span  class='center-block text-center' data-toggle='tooltip' title='Fuera de tiempo' > <i class='fa fa-eye-slash'></i>  {{$pago->streamingEvento->streamingFechaInicio->format('d-m-Y H:i')}} a {{$pago->streamingEvento->streamingFechaFinal->format('d-m-Y H:i')}} </span>
                                
                               @elseif ($pago->streamingMostrarFecha == false )

                                
                                <a href="{{route('page.streaming', [ 'idPago'=>  $pago->id  ])}}"  class='center-block text-center' data-toggle='tooltip' title='Acceder a video' > <i class='fa fa-eye'></i> Acceder </a>
                                
                                @endif
                            </td>
                            
                            
                            
                            
                            



                        </tr>


                            @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<div class="mastfoot">
    <div class="inner">
        <p class="color-dark text-center">&copy;2016 Fundación SM Derechos Reservados.</p>
    </div>
</div>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



