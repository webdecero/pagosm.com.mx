@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')




<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <div class="col-md-10 col-md-offset-1 ">


                <div class="text-justify" >



                    <div >
                        <div>
                            <div>
                                <h3>TÉRMINOS Y CONDICIONES DE USO Y PRIVACIDAD.</h3>
                                <br>
<p>A los Usuarios les informamos de los siguientes Términos y Condiciones de Uso y Privacidad, los cuales son aplicables por el simple uso o acceso a cualquiera de las Páginas que integran el Grupo SM, por lo que entenderemos que los acepta, y acuerda en obligarse en su cumplimiento.</p>
<p>En el caso de que no esté de acuerdo con los Términos y Condiciones de Uso y Privacidad deberá abstenerse de acceder o utilizar el Portal.</p>
Fundación SM de Ediciones México, A.C; se reserva el derecho de modificar discrecionalmente el contenido del Portal en cualquier momento, sin necesidad de previo aviso.</p>
<p>El Usuario entendido como aquella persona que realiza el acceso mediante equipo de cómputo y/o de comunicación, conviene en no utilizar dispositivos, software, o cualquier otro medio tendiente a interferir tanto en las actividades y/u operaciones del Portal o en las bases de datos y/o información que se contenga en el mismo.</p>
<p>Fundación SM de Ediciones México, A.C; tendrá el derecho a negar, restringir o condicionar al usuario el acceso a las Páginas, total o parcialmente, a su entera discreción, así como a modificar los Servicios y Contenidos de las Páginas en cualquier momento y sin necesidad de previo aviso.</p>

<p>-    PROPIEDAD INTELECTUAL.</p>
<p>Los derechos de propiedad intelectual respecto de los Servicios y Contenidos y los signos distintivos y dominios de las Páginas o el Portal, así como los derechos de uso y explotación de los mismos, incluyendo su divulgación, publicación, reproducción, distribución y transformación, son propiedad exclusiva de Fundación SM de Ediciones México, A.C; y/o empresas que integran el Grupo SM. El Usuario no adquiere ningún derecho de propiedad intelectual por el simple uso de los Servicios y Contenidos de la Página y en ningún momento dicho uso será considerado como una autorización o licencia para utilizar los Servicios y Contenidos con fines distintos a los que se contemplan en los presentes Términos y Condiciones de Uso y Privacidad.</p>
<p>Fundación SM de Ediciones México, A.C; así como empresas que integran el Grupo SM, no se convertirán en titulares de derechos sobre aquellas obras que se envíen por medio de sus portales con relación a concursos o convocatorias que Fundación SM de Ediciones México, A.C o cualquier empresa hermana de Grupo SM lancen al público, únicamente se convertirán en titulares de los derechos de explotación cuando se firme algún contrato de cesión de derechos, licencia u obra por encargo, así como cualquier documento legal en el que el usuario y empresas de Grupo SM lo establezcan.</p>
 
<p>-    CONFIDENCIALIDAD.</p>
<p>Fundación SM de Ediciones México, A.C; se obliga a mantener confidencialidad sobre la información que reciba del Usuario, la cual se tratará con el carácter de “Datos Personales”, siendo aplicable la Ley Federal de Protección de Datos Personales en Posesión de los Particulares. Fundación SM de Ediciones México, A.C; no asume ninguna obligación de mantener confidencial cualquier otra información que el Usuario no le haya proporcionado para los fines de la convocatoria del “Seminario Internacional de Educación Integral”. </p>

<p>-    COOKIES.</p>
<p>El dominio http://www.SIEI.com utiliza cookies y web beacons, los cuales son pequeños ficheros de datos que se generan en el ordenador del usuario y que nos permiten conocer su frecuencia de visitas, los contenidos más seleccionados y los elementos de seguridad que pueden intervenir en el control de acceso a áreas restringidas, así como la visualización de publicidad en función de criterios predefinidos por Fundación SM de Ediciones México, A.C.,   y que se activan por cookies servidas por dicha entidad o por terceros que presten estos servicios por cuenta de Fundación SM de Ediciones México, A.C</p>
<p>El usuario tiene la opción de impedir la generación de cookies, mediante la selección de la correspondiente opción en su programa navegador.</p>

<p>-    AVISO DE PRIVACIDAD DE DATOS PERSONALES.</p>
<p>Toda la información que Fundación SM de Ediciones México, A.C; así como empresas hermanas de Grupo SM recabe del Usuario es tratada con absoluta confidencialidad conforme las disposiciones legales aplicables.</p>
<p>Para mayor información, puede recurrir a nuestro aviso de privacidad, el cual se encuentra en la siguiente liga: http://www.SIEI.com </p>


<p>-    MODIFICACIONES</p>
<p>Fundación SM de Ediciones México, A.C; tendrá el derecho de modificar en cualquier momento los Términos y Condiciones de Uso y Privacidad. En consecuencia, el Usuario debe leer atentamente los Términos y Condiciones de Uso y Privacidad cada vez que pretenda utilizar el Portal.</p>
<p>En caso de que los presentes Términos y Condiciones de Uso y Privacidad sean modificados, la Fundación avisará al usuario de sus cambios mediante un aviso en su página web, trípticos o folletos disponibles en nuestras oficinas o hacerlas llegar al correo personal del usuario.</p>

<p>-    POLÍTICAS DE PAGO Y DEVOLUCIONES. </p>
<p>El usuario está de acuerdo que cualquier pago que realice a nombre de Fundación SM de Ediciones México, A.C., por motivo de su participación o asistencia en el Seminario Internacional de Educación Integral (en adelante SIEI) le dará el derecho de participar o asistir en el SIEI, así como en los casos en que el participante o asistente cancele su participación o asistencia Fundación SM de Ediciones México, A.C., se reserva el derecho de la devolución total o parcial de su pago. </p>
<p>De igual forma, el participante o asistente al SIEI acuerda que los pagos realizados por motivo de su inscripción serán personales e intransferibles.</p>
<p>En los casos en los que los pagos realizados por el participante o asistente al SIEI se dupliquen, Fundación SM de Ediciones México, A.C. le devolverá su dinero sólo por aquellas cantidades duplicadas.</p>
<p>Para seguridad del usuario, Fundación SM de Ediciones México, A.C no almacenará ni en medios físicos, electrónicos o análogos los números de Tarjeta, Códigos de seguridad o cualquier otra información relativa a los pagos realizados para la participación o asistencia en el SIEI.</p>
<p>En casos de que el Usuario se percate de fallas en el sistema de pagos para la participación o asistencia en el SIEI deberá reportarlo en un plazo no mayor a 48 horas a Fundación SM de Ediciones México, A.C..</p>
<p>La devolución de los pagos realizados a Fundación SM de Ediciones México, A.C., únicamente podrán realizarse en caso de que el Usuario por error del sistema haya hecho un depósito duplicado, sin embargo, en caso de que el Usuario cancele su participación o asistencia en el SIEI, Fundación SM de Ediciones México, A.C., no podrá realizar devolución alguna. </p>

<p>-    DAÑOS O RIESGOS OCURRIDOS DURANTE EL EVENTO. </p>
<p>Fundación SM de Ediciones México, A.C., se hará responsable sólo de aquellos auxilios médicos preventivos que cualquier participante o asistente al SIEI necesite, sin embargo, los gastos por conceptos de intervención médica o traslado a hospitales correrán a cargo del participante o asistente del SIEI. </p>
<p>Fundación SM de Ediciones México, A.C., no se hará responsable por aquellos daños y/o riesgos sufridos a personas y/o bienes durante el evento. Por lo anterior, el participante o asistente al SIEI se hará responsable de sus pertenencias en todo momento. </p>

<p>-    LEYES APLICABLES Y JURISDICCIÓN.</p>
<p>Para la interpretación, cumplimiento y ejecución de los presentes Términos y Condiciones de Uso y Privacidad, el Usuario está de acuerdo en que serán aplicables las leyes Federales de los Estados Unidos Mexicanos y competentes los tribunales de la Ciudad de México, renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera corresponderles en razón de sus domicilios presentes o futuros o por cualquier otra causa.</p>

<p>Fecha de actualización: Miércoles 4 de Enero de 2017.</p>
                            </div>
                        </div>
                    </div>


                </div>




            </div>
        </div>
    </div>
</section>

<div class="mastfoot">
    <div class="inner">
        <p class="color-dark text-center">&copy;<?php echo date("Y"); ?> Fundación SM Derechos Reservados.</p>
    </div>
</div>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



