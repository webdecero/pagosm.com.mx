@extends('sm.global.base' )
@section('nav')

@include('sm.global.nav-interno') 

@stop

@section('contenido')


<!-- Subheader Area
     ===================================== -->
<header class="bg-grad-stellar mt70">

    <div class="container">
        <div class="row mt20 mb30">
            <div class="col-md-6 text-left">
                <h3 class="color-light text-uppercase animated" data-animation="fadeInUp" data-animation-delay="100">{{ $evento->titulo}}<small class="color-light alpha7">{{ $evento->codigo}}</small></h3>
            </div>
            <div class="col-md-6 text-right pt35">
                <ul class="breadcrumb">
                    <li><a href="{{ route('page.inicio') }}">Home</a></li>
                    <li><a href="{{ route('page.inicio') }}#eventos">Eventos</a></li>
                    <li>{{ $evento->titulo}}</li>
                </ul>
            </div>
        </div>
    </div>

</header>


<!-- Blog Area
===================================== -->
<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2 col-xs-12 ">


                <h4 class="mt25 text-center">
                    Puedes realizar el pago en ventanilla
                    <span class="heading-divider mt10"></span>
                </h4>
                <a  href="{{route('page.instrucciones', ['referencia'=> $transaccion->referencia])}}" class="button button-block button-pasific hover-ripple-out mt30">Solicitar instrucciones<i class="fa fa-download fa-fw fa-1x"></i></a>



            </div>


            <div class="col-md-10 col-md-offset-1 col-xs-12 "> 
                <hr>
                <h4 class="mt25 text-center">
                    O puedes realizar tu pago en línea
                    <span class="heading-divider mt10"></span>




                </h4>
                <p class="help-block text-center">
                    <img class="img-responsive center-block" src="sm/img/visa.png" alt="visa">
                    Sólo aceptamos pagos con tarjetas de crédito / debito: Visa o MasterCard
                </p>




                <div class="row">


                    <!--Columna formulario-->

                    <div
                        class="col-md-6 col-xs-12 bg-gray pt30 pb20">
                        <h4 class="text-center mb20">Información  Tarjeta</h4>


                        <div class="row">

                            <form name="form" id="form-tarjeta" action="{{ route('page.recibo', ['idTransaccion' => $transaccion->_id]) }}" method="post" autocomplete="off" >

                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                <input type="hidden" name="idEvento" value="{{$evento->_id}}">





                                <div class="col-md-12">               
                                    <div class="form-group">
                                        <label>Nombre Tarjetahabiente</label>
                                        <input class="input-md input-rounded form-control" id="nombre-tarjeta" name="name" placeholder="" type="text" required="" maxlength="40" >
                                    </div>                                               
                                </div>


                                <div class="col-md-12">               
                                    <div class="form-group">
                                        <label>Numero de Tarjeta</label>
                                        <input class="input-md input-rounded form-control" id="numero-tarjeta" name="number" placeholder="xxxx-xxxx-xxxx-xxxx" type="text" required="" >
                                    </div>                                               
                                </div>

                                <div class="col-md-3">               
                                    <div class="form-group">
                                        <label>Mes Expiración</label>

                                        <select class="form-control input-md input-rounded" name="expmonth" placeholder="Mes" required="" >
                                            <option>01</option>
                                            <option>02</option>
                                            <option>03</option>
                                            <option>04</option>
                                            <option>05</option>
                                            <option>06</option>
                                            <option>07</option>
                                            <option>08</option>
                                            <option>09</option>
                                            <option>10</option>
                                            <option>11</option>
                                            <option>12</option>
                                        </select>





                                    </div>                                               
                                </div>
                                <div class="col-md-3">               
                                    <div class="form-group">
                                        <label>Año Expiración</label>

                                        <select class="form-control input-md input-rounded" name="expyear" placeholder="Año" required="" >
                                            
                                            <option>17</option>
                                            <option>18</option>
                                            <option>19</option>
                                            <option>20</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                        </select>


                                    </div>                                               
                                </div>
                                <div class="col-md-5 col-md-offset-1">               
                                    <div class="form-group">
                                        <label>Codigo Seguridad</label>
                                        <input class="input-md input-rounded form-control" id="cvv-tarjeta" name="cvv" placeholder="cvv" type="password" required="" maxlength="3" >
                                    </div>                                               
                                </div>



                                <div class="col-md-12">               
                                    <button type="submit" class="btn btn-primary pull-right">Pagar</button>                                        
                                </div>





                            </form>


                        </div>

                    </div>

                    <div class="col-md-5 col-md-offset-1  col-xs-12 pt30 pb20  text-center  panel-gray">


                        <h4 class=" mb20">Detalle de compra</h4>


                        <h5 class="mt25">
                            No. Referencia <small>{{$transaccion->referencia}}</small>
                            <span class="heading-divider mt10"></span>
                        </h5>



                        <div class="shop-sidebar-cart pr25 pl25 clearfix">


                            @foreach($transaccion->descuentos as $descuento)
                            <div class="sidebar-cart-container">

                                <h6>{!! $descuento['descripcion'] !!} </h6>
                                <span class="sidebar-cart-price text-gray">${{ number_format($descuento['cantidadDescontada'] , 2, '.', '')}} {{$evento->moneda}}  </span>
                                <span class="sidebar-cart-remove"><i class="fa fa-check-circle"></i>Aplicado</span>
                            </div>
                            @endforeach

                        </div>

                        <hr>


                        <h5>
                            <span class="text-gray">Total a pagar:</span> <span class="text-pasific">${{$transaccion->precioDescuento}} {{$transaccion->monedaFinal}}</span></h5>


                        @if($transaccion->monedaInicial != 'MXN')

                        <div class="text-center">
                            Tipo de Cambio ${{ number_format($transaccion->exchangeRate , 2, '.', '')}} {{$transaccion->monedaFinal}}
                        </div>

                        @endif







                    </div>

                </div>






            </div>        


        </div>



    </div>
</section>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



<script src="sm/assets/js/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>

@stop



