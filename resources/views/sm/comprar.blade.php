@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')


<!-- Subheader Area
     ===================================== -->
<header class="bg-grad-stellar mt70">

    <div class="container">
        <div class="row mt20 mb30">
            <div class="col-md-6 text-left">
                <h3 class="color-light text-uppercase animated" data-animation="fadeInUp" data-animation-delay="100">{{ $evento->titulo}}<small class="color-light alpha7">{{ $evento->codigo}}</small></h3>
            </div>
            <div class="col-md-6 text-right pt35">
                <ul class="breadcrumb">
                    <li><a href="{{ route('page.inicio') }}">Home</a></li>
                    <li><a href="{{ route('page.inicio') }}#eventos">Eventos</a></li>
                    <li>{{ $evento->titulo}}</li>
                </ul>
            </div>
        </div>
    </div>

</header>


<!-- Blog Area
===================================== -->
<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <!--Informacion Evento-->

            <div class="col-md-6">

                <div class="blog-three-mini">
                    <h3 class="color-dark">

                        <small class="color-success">{{ $evento->ubicacion }}</small>
                        {{ $evento->tituloLargo}}</h3>
                    <!--                    <div class="blog-three-attrib">
                                            <div><i class="fa fa-calendar"></i>Dec 15 2015</div> |
                                            <div><i class="fa fa-pencil"></i><a href="#">Harry Boo</a></div> |
                                            <div><i class="fa fa-comment-o"></i><a href="#">90 Comments</a></div> |
                                            <div><a href="#"><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div> |
                                            <div>
                                                Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                            </div>
                                        </div>-->

                    <img src="{{ $evento->imagen }}" alt="{{ $evento->tituloLargo}}" class="img-responsive center-block">

					<!-- Go to www.addthis.com/dashboard to customize your tools -->






                    {!! $evento->descripcion !!}
                    <div class="addthis_native_toolbox mt30"></div>

<!--                    <p class="lead mt25">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
    excepteur sint occaecat cupidatat non proident sunt in culpa.
</p>
<h3 class="mt25 mb25">Heading Title Two</h3>
<p>
    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
</p>
<blockquote>
    Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations.
    <footer><i class="fa fa-quote-right mt25"></i> Steve Job</footer>
</blockquote>
<p>
    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
</p>
<h4 class="mt25 mb25">Heading Title Three</h4>
<p>
    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
</p>-->

                    <!--                    <div class="blog-post-read-tag mt50">
                                            <i class="fa fa-tags"></i> Tags:
                                            <a href="#"> Javascript</a>,
                                            <a href="#"> HTML</a>,
                                            <a href="#"> Wordpress</a>,
                                            <a href="#"> Tutorial </a>
                                        </div>-->

                </div>






            </div>

            <!--Barra compra Evento-->
            <div id="sidebar"  class="col-md-6 mt50 animated" data-animation="fadeInRight" data-animation-delay="250">

                <div class="row">


                    <form id="form-comprar" action="{{route('page.dispatcher.pagar')}}" method="post" class="validate">

                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                        <input type="hidden" name="idEvento" value="{{$evento->_id}}">



                        <div  class="col-md-12 mt25 " >




                            @if(!empty($fechasColoquio))
                            <div class="pr25 pl25 mt25 clearfix">


                                <h5 class="mt25">
                                    Coloquios <small>Selecciona los coloquios de tu interes</small>
                                    <span class="heading-divider mt10"></span>
                                </h5>


                                @foreach($fechasColoquio as $key => $fecha)
                                <h6 >Horario: {{ date('d F Y  H:i', $fecha['_id']->sec)}}</h6>

                                <select name="coloquios[]" class="form-control " id="coloquios{{$key}}" >

                                    <option value="" > Selecciona tu Coloquio </option>

                                    @foreach($fecha['coloquios'] as $coloquio)

                                    <option value="{{$coloquio['_id']}}" >{{$coloquio['titulo'] .' - '. $coloquio['lugar'] }}</option>

                                    @endforeach

                                </select>
                                @endforeach


                            </div>

                            @endif


                            <!-- Codigo Descuento
                            ===================================== -->
                            <div class="mt25 pr25 pl25 clearfix">

                                <h5 class="mt25">
                                    Codigo de Descuento
                                    <small>Introducir codigo descuento</small>
                                    <span class="heading-divider mt10"></span>
                                </h5>


                                <div class="form-group">

                                    <input type="text" class="form-control" id="codigoDescuento" value="{{ old('codigoDescuento')}}"  name="codigoDescuento" placeholder="Codigo Descuento" maxlength="6" minlength="6" >
                                </div>
                                <!--<button type="submit" class="button button-sm button-block button-dark mt-10">Aplicar</button>-->

                            </div>

                            <!-- Descuentos Aplicados
                            ===================================== -->
                            <div class="pr25 pl25 mt25 clearfix">


                                <h5 class="mt25">
                                    Descuentos <small>Descuentos aplicados</small>
                                    <span class="heading-divider mt10"></span>
                                </h5>



                                <div class="shop-sidebar-cart pr25 pl25 clearfix">


                                    @foreach($descuentosAplicados as $descuento)
                                    <div class="sidebar-cart-container">

                                        <h6>{!! $descuento['descripcion'] !!} </h6>
                                        <span class="sidebar-cart-price text-gray">${{ number_format($descuento['cantidadDescontada'] , 2, '.', '')}} {{$evento->moneda}}  </span>
                                        <span class="sidebar-cart-remove"><i class="fa fa-check-circle"></i>Aplicado</span>
                                    </div>
                                    @endforeach



                                </div>






                            </div>




                            <!-- form Donativo
                            ===================================== -->

                            <div class="pr25 pl25 mt25 clearfix">



								<h5 class="mt25">


                                    ¿Requiere comprobante fiscal? <div class="checkbox">
										<label class="text-uppercase " >
											<input  type="checkbox" name="donativoCheck" id="donativoCheck" value="1">
											Sí. (Recuerda que posteriormente no podrás solicitar el comprobante fiscal.)
										</label>
									</div>
                                    
                                </h5>


                                <div id="donativoBox">


									<h5 class="mt25">


										<small>Completa los campos que se solicitan.</small>
										<span class="heading-divider mt10"></span>
									</h5>

									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Nombre completo de la institución/empresa/persona" name="nombreDonativo"  value="{{ old('nombreDonativo')}}" required=""  >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Razón social" name="razonSocialDonativo"  value="{{ old('razonSocialDonativo')}}" required="" >
									</div>





									<div class="form-group">
										<input type="email" class="input-md input-rounded form-control" placeholder="Correo electrónico"  name="emailDonativo" value="{{ old('emailDonativo')}}" required=""  >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Cédula de Identidad Fiscal (CIF) o RFC" name="rfcDonativo" value="{{ old('rfcDonativo')}}" required=""  >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Código telefónico del país" name="codigoPaisDonativo" value="{{ old('codigoPaisDonativo')}}"  >

									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Larga Distancia telefónica" name="ladaDonativo" value="{{ old('ladaDonativo')}}" >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Teléfono" name="telefonoDonativo" value="{{ old('telefonoDonativo')}}" required="" >
									</div>








									<div class="form-group">
										<label for="paisDonativo">Selecciona País</label>

										<select id="paisDonativo" name="paisDonativo" class="input-md input-rounded form-control" required=""  >
											@foreach (trans('listado.paises') as $pais)

											@if($pais == 'México')
											<option value="{{$pais }}" selected="selected" >{{$pais }}</option>
											@else

											<option value="{{$pais }}"  >{{$pais }}</option>

											@endif
											@endforeach

										</select>



									</div>



									<div class="form-group">
										<label for="estadoDonativo">Selecciona Estado</label>

										<select name="estadoDonativo" class="input-md input-rounded form-control" required=""  >


											@foreach (trans('listado.estados') as $estado)
											<option value="{{$estado }}">{{$estado }}</option>
											@endforeach


										</select>


									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Calle" name="direccionDonativo" value="{{ old('direccionDonativo')}}" required="" >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Número exterior" name="numExtDonativo" value="{{ old('numExtDonativo')}}" required="" >
									</div>



									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Número interior" name="numIntDonativo" value="{{ old('numIntDonativo')}}" >
									</div>





									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Colonia" name="coloniaDonativo" value="{{ old('coloniaDonativo')}}" required="" >
									</div>




									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control" placeholder="Delegación ó Municipio" name="delegacionDonativo" value="{{ old('delegacionDonativo')}}" required="" >
									</div>




									<div class="form-group">
										<input  type="text" class="input-md input-rounded form-control" placeholder="Ciudad" id="ciudadDonativo" name="ciudadDonativo" value="{{ old('ciudadDonativo')}}"  >
									</div>




									<div class="form-group">
										<input type="text" class="input-md input-rounded form-control codigoPostal" placeholder="Codigo Postal" id="codigoPostalDonativo" name="codigoPostalDonativo" value="{{ old('codigoPostalDonativo')}}"  >
										<p class="help-block"> Si en su país no existe el código postal, deberá colocar en este campo 00000 </p>
									</div>








                                </div>






                            </div>



                            <!-- Boton pagar
                            ===================================== -->

                            <div class="pr25 pl25 mt25 clearfix">





                                <button  type="submit" class="button button-block button-pasific hover-ripple-out mt30">Pagar  ${{$evento->precioDescuento}} {{$evento->moneda}}<i class="fa fa-shopping-basket fa-fw fa-1x"></i></button>


                                @if($evento->moneda != 'MXN')

                                <div class="text-center">
                                    Tipo de Cambio ${{ number_format($USD , 2, '.', '')}} {{$evento->moneda}}
                                </div>

                                @endif





                            </div>


                        </div>



                    </form>

                </div>
            </div>




        </div>



    </div>
</section>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



