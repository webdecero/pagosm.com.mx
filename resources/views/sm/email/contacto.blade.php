<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <!--<link rel="STYLESHEET" href="css/print_static.css" type="text/css" />-->
    </head>

    <body>

        <div id="body">

            <div id="section_header">
            </div>

            <div id="content">

                <div class="page" style="font-size: 7pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td><h1 style="text-align: left; color: #ff0000;">Contacto pagosm.com.mx</h1></td>

                        </tr>
                    </table>

                    <table style="width: 100%; font-size: 10pt;">
                        <tr>
                            <td colspan="2"><strong>Información:</strong></td>
                        </tr>
                        <tr>
                            <td>Nombre: <strong> {{ $input['nombre'] or ''}} </strong></td>
                        </tr> 
                        <tr>
                            <td>Email: <strong> {{ $input['email'] or '' }} </strong></td>
                        </tr>
                        <tr>
                            <td>Telefono: <strong> {{ $input['telefono'] or '' }} </strong></td>
                        </tr>
                        <tr>
                            <td>Mensaje: <strong> {{ $input['mensaje'] or '' }} </strong></td>
                        </tr>

                    </table>

                </div>

            </div>
        </div>


    </body>
</html>