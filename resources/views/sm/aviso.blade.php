@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')




<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-1 ">


                <div class="text-justify" >


                    <h3 class="font-montserrat cover-heading mb20 mt20">AVISO DE PRIVACIDAD</h3>

                    <p>
                        Fundación SM de Ediciones México, A.C.; es una asociación civil constituida de acuerdo con las leyes de la República Mexicana, con domicilio en Magdalena N&deg; 211, Colonia Del Valle, Delegación Benito Juárez, México, D.F., C.P. 03100, y con Registro Federal de Contribuyentes número FSE100215SQ2, inscrita en el Registro Público de la Propiedad y el Comercio con el Folio Mercantil número 93 en fecha 23 de julio de 2010; licenciatario en México de la marca &ldquo;FUNDACIÓN SM&rdquo; y titular del dominio http://www.cilelij.com (en adelante el dominio), siendo responsable de las bases de datos generadas con los datos de carácter personal suministrados por los usuarios (en adelante datos personales) le brinda el siguiente aviso de privacidad.</p>
                    <p>
                        1. &iquest;Para qué fines utilizaremos sus datos personales?</p>
                    <p>
                        Los datos personales que recabamos de Usted, los utilizaremos para las siguientes finalidades:</p>

                    <ul>
                        <li>
                            <p>
                                Envío de información y promociones de futuros eventos.</p>
                        </li>
                        <li>
                            <p>
                                Mantener contacto con aquellos participantes directos o indirectos y asistentes del</p>
                            <p>
                                Congreso Iberoamericano de Lengua y Literatura Infantil y Juvenil (en adelante CILELIJ)</p>
                            <p>
                                para futuras colaboraciones con empresas del Grupo SM.</p>
                        </li>
                        <li>
                            <p>
                                Contar con una base estadística sobre la cantidad de participantes y asistentes en el</p>
                            <p>
                                CILELIJ.</p>
                        </li>
                    </ul>

                    <p>
                        En caso de que no desee que sus datos personales sean tratados para alguno de estos fines, desde este momento usted nos puede comunicar lo anterior, a través de un escrito dirigido a la Gerencia de Proyectos de Fundación SM México al domicilio &ldquo;Magdalena N&deg; 211, Colonia Del Valle, Delegación Benito Juárez, México, Distrito Federal, Código Postal 03100&rdquo; o bien al correo electrónico contacto@fundacion-sm.com.</p>


                    <p>
                        2.&iquest;Qué datos personales utilizaremos para estos fines?</p>
                    <p>
                        Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales:&nbsp;</p>

                    <ul>
                        <li>
                            <p>
                                Nombre completo</p>
                        </li>
                        <li>
                            <p>
                                Correo electrónico</p>
                        </li>
                        <li>
                            <p>
                                Sexo</p>
                        </li>
                        <li>
                            <p>
                                Fecha de nacimiento</p>
                        </li>
                        <li>
                            <p>
                                Domicilio</p>
                        </li>
                        <li>
                            <p>
                                Institución donde labora</p>
                        </li>
                        <li>
                            <p>
                                Sector de su empresa (Público o privada)</p>
                        </li>
                        <li>
                            <p>
                                Cargo</p>
                        </li>
                        <li>
                            <p>
                                Área de desempeño</p>
                        </li>
                        <li>
                            <p>
                                Datos Fiscales (sólo en aquellos casos en los que se requiera la emisión de recibos por concepto de donación).</p>
                        </li>
                    </ul>

                    <p>
                        3. &iquest;Con quién compartimos su información personal y para qué fines?</p>

                    <p>
                        Le informamos que sus datos personales podrán ser compartidos únicamente con las sociedades presentes o futuras que conforman el Grupo SM con presencia en Iberoamérica con el único propósito de poder cumplir con las finalidades enumeradas anteriormente en el punto 1. Para conocer la presencia de las empresas de Grupo SM con las que se podrá realizar la transferencia de sus datos personales puede acceder a la siguiente página web: http://www.grupo- sm.com/presencia</p>

                    <p>
                        4. &iquest;Cómo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso?</p>
                    <p>
                        Usted tiene derecho de acceder, rectificar y cancelar sus datos personales, así como de oponerse al tratamiento de los mismos o revocar el consentimiento que para tal fin nos haya otorgado, a través de los procedimientos implementados. Para conocer los procedimientos, requisitos y plazos para acceder, rectificar y cancelas sus datos, puede contactar a la Gerencia de Proyectos de Fundación SM México al número: 1087-8400 y/o en el correo electrónico: contacto@fundacion-sm.com</p>

                    <p>
                        5. El uso de tecnologías de rastreo en nuestro portal de Internet.</p>

                    <p>
                        Fundación SM de Ediciones México, A.C, obtiene información anónima acerca de sus visitantes, lo que significa que dicha información no puede ser asociada a un usuario concreto e identificado. Los datos que se conservan son:</p>

                    <ol>
                        <li>
                            <p>
                                El nombre de dominio del proveedor (ISP) que les da acceso a la red. El nombre del proveedor de servicios que le da acceso a Internet (ISP).</p>
                        </li>
                        <li>
                            <p>
                                La fecha y hora de acceso a nuestra Web.</p>
                        </li>
                        <li>
                            <p>
                                La dirección de Internet desde la que partió el link que dirige a nuestra Web.</p>
                        </li>
                        <li>
                            <p>
                                El número de visitantes diarios de cada sección.</p>
                        </li>
                        <li>
                            <p>
                                El navegador que se utilizó para entrar a nuestro sitio web.</p>
                        </li>
                    </ol>

                    <p>
                        El fin único del registro de estos datos es la mejora de la experiencia de navegación del usuario.</p>

                    <p>
                        6. Navegación con cookies</p>

                    <p>
                        El dominio http://www.cilelij.com utiliza cookies y web beacons, los cuales son pequeños ficheros de datos que se generan en el ordenador del usuario y que nos permiten conocer su frecuencia de visitas, los contenidos más seleccionados y los elementos de seguridad que pueden intervenir en el control de acceso a áreas restringidas, así como la visualización de publicidad en función de criterios predefinidos por Fundación SM de Ediciones México, A.C., y que se activan por cookies servidas por dicha entidad o por terceros que presten estos servicios por cuenta de Fundación SM de Ediciones México, A.C&nbsp;</p>

                    <p>
                        El usuario tiene la opción de impedir la generación de cookies, mediante la selección de la correspondiente opción en su programa navegador.</p>

                    <p>
                        7. &iquest;Cómo puede conocer los cambios a este aviso de privacidad?</p>

                    <p>
                        El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.</p>
                    <p>
                        Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, a través de nuestro domino http://www.cilelij.com o a través del correo electrónico personal o institucional el cual será el que se recabe del titular.</p>




                </div>




            </div>
        </div>
    </div>
</section>

<div class="mastfoot">
    <div class="inner">
        <p class="color-dark text-center">&copy;2016 Fundación SM Derechos Reservados.</p>
    </div>
</div>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



