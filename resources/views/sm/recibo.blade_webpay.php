<!DOCTYPE html>
<html  lang="es" >
    <head>
        <!-- BASIC PAGE NEEDS -->
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- Load Core CSS 
        =====================================-->
        <link rel="stylesheet" href="https://pagosm.com.mx/sm/assets/css/core/bootstrap.min.css">

    </head>

    <body  >

        <!-- Recibo
      ===================================== -->

        <div class="container">

            <div class="row">
                <div class="col-md-12 ">
                    <div class="panel panel-success">
                        <div class="panel-body">
                            <h3> {!!$messages!!}</h3>

                            @if($success == false && isset($error) && $error != false)
                            <span class="label label-warning">{{$error}}</span>
                            @endif


                            @if($success == true)
                            <dl>
                                <dt>Número Autorizacion:</dt>
                                <dd>{{$pago->aut}}</dd>
                                <dt>Nombre:</dt>
                                <dd>{{$pago->ccName}}</dd>
                                <dt>Tarjeta:</dt>
                                <dd>{{$pago->ccNum}}</dd>
                                <dt>Referencia:</dt>
                                <dd>{{$pago->referencia}}</dd>
                            </dl>
                            
                            <a  href="{{route('page.comprobante', ['idPago'=> $pago->id])}}" class="btn btn-default">Descargar Comprobante<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a>

                            <a href="{{route('page.inicio',["#eventos"])}}" target="_top"  class="btn btn-default" > <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Finalizar</a>
                            @else
                            <a href="{{route('page.comprar',["idEvento"=>$evento->id])}}" target="_top"  class="btn btn-success" > <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Comenzar nuevamente</a>
                            <a href="mailto:rvazquez@ediciones-sm.com.mx" target="_top"  class="btn btn-default" > <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Contactar administrador</a>
                            @endif
                           
                        </div>
                    </div>







                </div>        



            </div>



        </div>








        <!-- JQuery Core
        =====================================-->
<!--        <script src="sm/assets/js/core/jquery.min.js"></script>
        <script src="sm/assets/js/core/bootstrap.min.js"></script>-->



    </body>
</html>




