<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        <!--<link rel="STYLESHEET" href="css/print_static.css" type="text/css" />-->
    </head>

    <body>

        <div id="body">

            <div id="section_header">
                <img src="{{ $evento->imagenComprobante }}" alt="{{ $evento->titulo }}" />
            </div>

            <div id="content">

                <div class="page" style="font-size: 7pt">
                    <table style="width: 100%;" class="header">
                        <tr>
                            <td><h1 style="text-align: left;">Gracias por inscribirte a <br> 
                                    
                                    <span style="color: #ff0000;">{{ $evento->titulo }} </span></h1></td>
                            
                        </tr>
                    </table>
                    
                    <table style="width: 100%; font-size: 10pt; background-color:#fdf9eb; ">
                        <tr>
                            <td colspan="2"><strong>A continuación mostramos tus datos tal y como fueron capturados en nuestro sistema de registro:</strong></td>

                        </tr>



                        <tr>
                            <td>Nombre: <strong> {{ $user->nombreCompleto}} </strong></td>

                        </tr>
                        <tr>
                            <td>Email: <strong> {{ $user->email }} </strong></td>

                        </tr>
                        <tr>
                            <td>Institución: <strong> {{ $user->empresa }} </strong></td>

                        </tr>
                        <tr>
                            <td>Cargo: <strong> {{ $user->cargo }} </strong></td>

                        </tr>
                        <tr>
                            <td>Código de referencia: <strong> {{ $transaccion->referencia }} </strong></td>

                        </tr>
                        <tr>
                            <td>Subtotal: <strong> {{ $transaccion->precioInicial }} {{ $transaccion->monedaInicial }} </strong></td>

                        </tr>

                        @foreach($transaccion->descuentos as $descuento)
                        <tr>
                            <td>
                                
                                Descuento: <strong> {{ $descuento['porcentaje']}}% @if( $descuento['tipo'] == "descuentoCodigo" ) Código descuento {{ $descuento['codigo']}} @endif </strong>
                            
                               
                            </td>
                            
                            
                        </tr>
                        @endforeach

                        <tr>
                            <td>Total: <strong> {{ $transaccion->precioDescuento }} {{ $transaccion->monedaInicial }}</strong></td>

                        </tr>



                    </table>

                    <table class="sa_signature_box" style="border-top: 1px solid #333333;  margin-top: 1em; font-size: 10pt;">

                       

                        <tr><td colspan="6" style="white-space: normal">
                                {!! $evento->instrucciones !!}
                            </td>
                        </tr>



                    </table>

                </div>

            </div>
        </div>


    </body>
</html>