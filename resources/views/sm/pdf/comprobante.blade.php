<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
    <head>
        
        <style>
            #descuentos, #descuentos th,#descuentos  td {
                border: 1px solid black;
                border-collapse: collapse;
                text-align: center;
            }
            table{
                width: 100%;
                font-size: 10pt;
            }
        </style>
        
        
    </head>

    <body>

        <div id="body">

            <div id="section_header">
                <img src="{{ $evento->imagenComprobante }}" alt="{{ $evento->titulo }}" />
            </div>

            <div id="content">

                <div class="page" >
                    <table  class="header">
                        <tr>
                            <td><h1 style="text-align: left;">Gracias por inscribirte a <br> 

                                    <span style="color: #ff0000;">{{ $evento->titulo }} </span></h1></td>

                        </tr>
                    </table>

                    <table >
                        <tr>
                            <td colspan="2"><strong>A continuación mostramos tus datos tal y como fueron capturados en nuestro sistema de registro:</strong></td>

                        </tr>

                    </table>

                    <table style="  background-color:#fdf9eb; ">


                        <tr>
                            <td>Nombre: <strong> {{ $user->nombreCompleto}} </strong></td>

                        </tr>
                        <tr>
                            <td>Email: <strong> {{ $user->email }} </strong></td>

                        </tr>
                        <tr>
                            <td>Institución: <strong> {{ $user->empresa }} </strong></td>

                        </tr>
                        <tr>
                            <td>Cargo: <strong> {{ $user->cargo }} </strong></td>

                        </tr>

                        <tr>
                            <td>Referencia: <strong> {{ $pago->referencia }} </strong></td>

                        </tr>
                        
                        
                        <tr>
                            <td>Precio inicial: <strong> $ {{ number_format($pago->precioInicial , 2, '.', '') }} {{ $pago->monedaInicial }} </strong></td>

                        </tr>
                        
                        <tr>
                            <td>Pago total: <strong> $ {{ number_format($pago->precioFinal , 2, '.', '') }} {{ $pago->monedaFinal }} </strong></td>

                        </tr>




                    </table>
                    
                    @if(isset($coloquios) && $coloquios != NULL )

                    <table id="coloquios" style="margin-top: 15px;  " >

                        
                         <tr>
                            
                            <td>  Estás inscrito en los siguientes coloquios:</td>

                        </tr>


                        @foreach($coloquios as $key => $coloquio)
                        <tr>
                            
                            <td>  {{ $coloquio->fechaFinalFormato('l d F') }}: <b> {{ $coloquio->titulo }} </b> </td>

                        </tr>

                        @endforeach      



                    </table>

                    @endif 


                    @if(isset($pago->descuentos) && !empty($pago->descuentos))

                    
                    <table id="descuentos" style="  border: 1px solid black;   ">

                        <caption><h3>Desglose descuentos</h3></caption>
                        <tr>
                            
                            <th>Tipo Descuento</th>
                            <th>Porcentaje %</th>
                            <th>Monto descontado</th>

                        </tr>


                        @foreach($pago->descuentos as $key => $descuento)
                        <tr>
                            
                            <td>
                                <?php
                                switch ($descuento['tipo']) {
                                    case 'descuentoCodigo':

                                        echo "Descuento por Código <b>". $descuento['codigo'] ."</b>";

                                        break;
                                    case 'descuentoFecha':

                                        echo "Descuento por Fecha";

                                        break;
                                    case 'descuentoPais':

                                        echo "Descuento por País";

                                        break;
                                }
                                ?>
                            </td>
                            
                            
                            
                            
                            
                            <td>{{ $descuento['porcentaje']}}% </td>

                            <td> $ {{ $descuento['cantidadDescontada']}} {{ $pago->monedaInicial }} </td>
                            
                        </tr>

                        @endforeach      



                    </table>

                    @endif  


                    <table class="sa_signature_box" style="border-top: 1px solid #333333; padding-top: 2em; margin-top: 2em; ">




                        <tr><td style="white-space: normal">
                                {!! $evento->comprobante !!}
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: normal; width: 100%; text-align: center;">
                                <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($pago->referencia, "C39") }}" alt="{{ $pago->referencia }}" />
                                <br>
                                {{ $pago->referencia }}
                            </td>
                        </tr>



                    </table>

                </div>

            </div>
        </div>


    </body>
</html>