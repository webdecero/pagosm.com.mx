@extends('sm.global.base' )



@section('nav')

@include('sm.global.nav-interno') 

@stop


@section('contenido')



<div class="site-wrapper bg-grad-stellar">

    <div class="site-wrapper-inner">

        <div class="container">



            <div id="formDonativo" class="inner cover animated" data-animation="fadeIn" data-animation-delay="100">
                <br>
                <div class="row">
                    
                    
                    <div class="col-md-12">
                        <h2 class="color-light text-center mt50">Formulario de Donativo</h2><br><br>
                    </div>









                    <div class=" col-md-offset-2 col-md-8 col-xs-12 text-center">

                        <div class="contact contact-us-one">


                            <div class="col-sm-12 col-xs-12 text-center mb20">
                                <h4 class="pb25 bb-solid-1 text-uppercase">
                                    ¿Requiere recibo de Donativo?
                                    <small class="text-lowercase">Completa los campos que se solicitan.</small>
                                </h4>
                            </div>



                            <form id="form-donativo" action="{{ route('page.donativo.guarda') }}" method="Post" >

                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="idPago" value="{{ $pago->id }}" >
                                
                                <input type="hidden" name="referencia" value="{{ $pago->referencia }}" >




                                <!--Donativo-->


                                




                                <div id="donativoBox">



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Nombre completo de la institución/empresa/persona" name="nombreDonativo"  value="{{ old('nombreDonativo')}}" required=""  >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">


                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Razón social" name="razonSocialDonativo"  value="{{ old('razonSocialDonativo')}}" required="" >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="email" class="input-md input-rounded form-control" placeholder="Correo electrónico"  name="emailDonativo" value="{{ old('emailDonativo')}}" required=""  >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Cédula de Identidad Fiscal (CIF) o RFC" name="rfcDonativo" value="{{ old('rfcDonativo')}}" required=""  >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Código telefónico del país" name="codigoPaisDonativo" value="{{ old('codigoPaisDonativo')}}"  >

                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Larga Distancia telefónica" name="ladaDonativo" value="{{ old('ladaDonativo')}}" >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Teléfono" name="telefonoDonativo" value="{{ old('telefonoDonativo')}}" required="" >
                                        </div>
                                    </div>



                                    
                                    
                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="paisDonativo">Selecciona País</label>

                                            <select id="paisDonativo" name="paisDonativo" class="input-md input-rounded form-control"  >
                                                @foreach (trans('listado.paises') as $pais)

                                                @if($pais == 'México')
                                                <option value="{{$pais }}" selected="selected" >{{$pais }}</option>
                                                @else

                                                <option value="{{$pais }}"  >{{$pais }}</option>

                                                @endif
                                                @endforeach

                                            </select>



                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="estadoDonativo">Selecciona Estado</label>

                                            <select name="estadoDonativo" class="input-md input-rounded form-control"  >


                                                @foreach (trans('listado.estados') as $estado)
                                                <option value="{{$estado }}">{{$estado }}</option>
                                                @endforeach


                                            </select>


                                        </div>
                                    </div>


                                    
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Calle" name="direccionDonativo" value="{{ old('direccionDonativo')}}" required="" >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Número exterior" name="numExtDonativo" value="{{ old('numExtDonativo')}}" required="" >
                                        </div>
                                    </div>               

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Número interior" name="numIntDonativo" value="{{ old('numIntDonativo')}}" >
                                        </div>
                                    </div>



                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Colonia" name="coloniaDonativo" value="{{ old('coloniaDonativo')}}" required="" >
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control" placeholder="Delegación ó Municipio" name="delegacionDonativo" value="{{ old('delegacionDonativo')}}" required="" >
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input  type="text" class="input-md input-rounded form-control" placeholder="Ciudad" name="ciudadDonativo" value="{{ old('ciudadDonativo')}}"  >
                                        </div>
                                    </div>


                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="input-md input-rounded form-control codigoPostal" placeholder="Codigo Postal" name="codigoPostalDonativo" value="{{ old('codigoPostalDonativo')}}"  >
                                            <p class="help-block"> Si en su país no existe el código postal, deberá colocar en este campo 00000 </p>
                                        </div>
                                    </div>

                                    
                                    
                                    
                                    


                                </div>


                                <div class="col-sm-12 col-xs-12 mt10 text-center mb20">




                                    
                                    <button type="submit" class="button-3d button-md button-block button-blue hover-ripple-out" role="button" >Solicitar Recibo</button>
                                </div>











                            </form>







                        </div>
                    </div>

                </div>
            </div>


            <div class="mastfoot">
                <div class="inner">

                    <p class="color-light text-center">&copy;2016 Fundación SM</p>
                </div>
            </div>

        </div>
    </div>
</div>




@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<!--<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>-->

<!-- Progress Bars
=====================================-->
<!--<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>-->

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



<script src="sm/assets/js/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>





@stop



