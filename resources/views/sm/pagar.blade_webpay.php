@extends('sm.global.base' )
@section('nav')

@include('sm.global.nav-interno') 

@stop

@section('contenido')


<!-- Subheader Area
     ===================================== -->
<header class="bg-grad-stellar mt70">

    <div class="container">
        <div class="row mt20 mb30">
            <div class="col-md-6 text-left">
                <h3 class="color-light text-uppercase animated" data-animation="fadeInUp" data-animation-delay="100">{{ $evento->titulo}}<small class="color-light alpha7">{{ $evento->codigo}}</small></h3>
            </div>
            <div class="col-md-6 text-right pt35">
                <ul class="breadcrumb">
                    <li><a href="{{ route('page.inicio') }}">Home</a></li>
                    <li><a href="{{ route('page.inicio') }}#eventos">Eventos</a></li>
                    <li>{{ $evento->titulo}}</li>
                </ul>
            </div>
        </div>
    </div>

</header>


<!-- Blog Area
===================================== -->
<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-md-offset-2 col-xs-12 ">

                
                <h4 class="mt25 text-center">
                    Puedes realizar el pago en ventanilla
                    <span class="heading-divider mt10"></span>
                </h4>
                <a  href="{{route('page.instrucciones', ['referencia'=> $transaccion->referencia])}}" class="button button-block button-pasific hover-ripple-out mt30">Solicitar instrucciones<i class="fa fa-download fa-fw fa-1x"></i></a>
               
                
                
            </div>
            
            
            <div class="col-md-10 col-md-offset-1 col-xs-12 "> 
                <hr>
            </div>

            <div class="col-md-8 col-md-offset-2 col-xs-12">

                <h4 class="mt25 text-center">
                    O puedes realizar tu pago en línea
                    <span class="heading-divider mt10"></span>
                    
                    
                                
                    
                </h4>
                       <p class="help-block text-center">
                                    <img class="img-responsive center-block" src="sm/img/visa.png" alt="visa">
                                    Sólo aceptamos pagos con tarjetas de crédito / debito: Visa o MasterCard
                                </p>

<!--                <div class="site-wrapper" style="background:#f2f2f2;">

                    <div class="site-wrapper-inner">
                        <div class="cover-container">                    
                            <div id="formLogin" class="inner cover text-center animated" data-animation="fadeIn" data-animation-delay="100">
                                <br>
                                <i class="under-construction color-pasific"></i><i class="under-construction color-dark"></i><br>
                                <h3 class="font-montserrat cover-heading mb20 mt20">Por el momento el sistema de pago bancario se encuentra en mantenimiento.</h3>

                                <label>Cualquier duda puede comunicarse a: <a href="mailto:contacto@fundacion-sm.com">contacto@fundacion-sm.com</a></label>


                                <br>
                            </div>

                            <div class="mastfoot">
                                <div class="inner">
                                    <p class="color-dark text-center">&copy;2016 Fundación SM Derechos Reservados.</p>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>-->



                 <iframe src="{{ $webpay }}" frameborder="0" style="width:100%; height:110vh;">
</iframe>

            </div>        


        </div>



    </div>
</section>










@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



