@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')

<!-- Disable Clic derecho --> 
<script language="Javascript">
document.oncontextmenu = function(){return false}
</script>

<!-- Responsive Video Flash -->
<style type="text/css">
	.video-container {
	position: relative;
	padding-bottom: 56.25%;
	padding-top: 30px; height: 0; overflow: hidden;
	}
	 
	.video-container iframe,
	.video-container object,
	.video-container embed {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	}
</style>

<!-- CHAT AMB - SIEI Continuo -->

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="44a88900-947e-4b84-970d-472f20f22054";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

<!-- CHAT AMB - SIEI Continuo -->


<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center mb30">{{ $evento->titulo }}
                
                    
                </h4>
                
                
                
                {!! $evento->streaming !!}
               

                
            </div>
        </div>
    </div>
</section>



@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



