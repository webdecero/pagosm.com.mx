@extends('sm.global.base' )



@section('nav')

@include('sm.global.nav-interno') 

@stop


@section('contenido')



<div class="site-wrapper bg-grad-stellar">

    <div class="site-wrapper-inner">

        <div class="container">



            <div id="formLogin" class="inner cover animated" data-animation="fadeIn" data-animation-delay="100">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="color-light text-center mt50">Formulario de Registro</h2><br><br>
                    </div>









                    <div class=" col-md-offset-3 col-md-6 col-xs-12 text-center">

                        <div class="contact contact-us-one">


                            <div class="col-sm-12 col-xs-12 text-center mb20">
                                <h4 class="pb25 bb-solid-1 text-uppercase">
                                    Registro
                                    <small class="text-lowercase">Completa los campos que se solicitan.</small>
                                </h4>
                            </div>



                            <form id="form-registro" action="{{ route('page.usuario.guarda') }}" method="Post" >

                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="idEvento" value="{{ $idEvento }}" >




                                <div id="carousel-registro" class="carousel slide" data-ride="carousel" >

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">


                                        <!--Datos Personales-->

                                        <div class="item active">

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <h3 class="form-control-static">Datos Personales</h3>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Nombre" name="nombre"  value="{{ old('nombre')}}" maxlength="100" required>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Apellido Paterno"  name="apellidoPaterno" value="{{ old('apellidoPaterno')}}"  maxlength="100" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Apellido Materno" name="apellidoMaterno"  value="{{ old('apellidoMaterno')}}" maxlength="100" >
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="email" class="input-md input-rounded form-control" placeholder="Email" name="email" maxlength="100"  value="{{ old('email')}}" required>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input id="password" type="password" class="input-md input-rounded form-control" placeholder="Contraseña" name="password" maxlength="100" required>
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="password" class="input-md input-rounded form-control" placeholder="Confirmar Contraseña" name="passwordConfirmar" maxlength="100" required>
                                                </div>
                                            </div>



                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for=""> Género</label>

                                                    <div class="radio">
                                                        <label> 
                                                            <input type="radio" name="genero" id="" value="hombre" checked>
                                                            Hombre
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="genero" id="" value="mujer" >
                                                            Mujer
                                                        </label>
                                                    </div>



                                                </div>
                                            </div>


                                            <div class="col-md-6 col-xs-12">
                                                <div class="form-group">
                                                    <label for=""> Fecha Nacimiento</label>

                                                    <div class="form-group">
                                                        <input id="fechaNacimiento" type="text" class="input-md input-rounded form-control"  value="{{ old('fechaNacimiento')}}"  name="fechaNacimiento" >
                                                    </div>



                                                </div>
                                            </div>





                                            <div class="col-sm-12 col-xs-12 mt10 text-center mb20">

                                                <button type="button" class="button-3d button-md button-block button-blue hover-ripple-out siguiente" role="button" >Siguiente</button>
                                            </div>
                                            <!--                                                <div class="col-sm-12 mt10 text-center mb20">
                                                                                                <button type="submit" class="button-3d button-md button-block button-blue hover-ripple-out">Register</button>
                                                                                            </div>-->
                                            <div class="col-sm-6 pull-left"><a href="#" class="color-dark">&nbsp;</a></div>


                                        </div>


                                        <!--Direccion-->

                                        <div class="item">


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <h3 class="form-control-static">Dirección</h3>
                                                </div>
                                            </div>



                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="pais">Selecciona País</label>

                                                    <select id="pais" name="pais" class="input-md input-rounded form-control"  >
                                                        @foreach (trans('listado.paises') as $pais)

                                                        @if($pais == 'México')
                                                        <option value="{{$pais }}" selected="selected" >{{$pais }}</option>
                                                        @else

                                                        <option value="{{$pais }}"  >{{$pais }}</option>

                                                        @endif
                                                        @endforeach

                                                    </select>



                                                </div>
                                            </div>



                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="estado">Selecciona Estado</label>

                                                    <select name="estado" class="input-md input-rounded form-control"  >


                                                        @foreach (trans('listado.estados') as $estado)
                                                        <option value="{{$estado }}">{{$estado }}</option>
                                                        @endforeach


                                                    </select>


                                                </div>
                                            </div>



                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input id="codigoPostal" type="text" class="input-md input-rounded form-control codigoPostal" placeholder="Codigo Postal" name="codigoPostal" >
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input id="ciudad" type="text" class="input-md input-rounded form-control" placeholder="Ciudad" name="ciudad" >
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Delegación ó Municipio" name="delegacion" >
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Colonia" name="colonia" >
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Calle y número" name="direccion" >
                                                </div>
                                            </div>


                                            <div class="col-sm-12 col-xs-12 mt10 text-center mb20">

                                                <button type="button" class="button-3d button-md button-block button-pasific hover-ripple-out anterior" role="button" >Anterior</button>
                                                <button type="button" class="button-3d button-md button-block button-blue hover-ripple-out siguiente" role="button" >Siguiente</button>
                                            </div>



                                        </div>


                                        <!--Laborales-->

                                        <div class="item">


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <h3 class="form-control-static">Datos Laborales</h3>
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Institución, organización o empresa en la que labora *" name="empresa" value="{{ old('empresa')}}" required>
                                                </div>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">


                                                    <div class=" radio-inline">
                                                        <label  > 
                                                            <input type="radio" name="sector" id="" value="publico" checked>
                                                            Público
                                                        </label>
                                                    </div>
                                                    <div class=" radio-inline">
                                                        <label >
                                                            <input type="radio" name="sector" id="" value="privado" >
                                                            Privado
                                                        </label>
                                                    </div>



                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="input-md input-rounded form-control" placeholder="Cargo" name="cargo" value="{{ old('cargo')}}"  required>
                                                </div>
                                            </div>





                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="areaDesempeno">Área de desempeño</label>

                                                    <select id="areaDesempeno"  name="areaDesempeno" class="input-md input-rounded form-control"  >


                                                        @foreach (trans('listado.areaDesempeno') as $area)
                                                        <option value="{{$area }}">{{$area }}</option>
                                                        @endforeach


                                                    </select>


                                                </div>
                                            </div>


                                            <div id="claveTrabajoBox" class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input id="claveTrabajo" type="text" class="input-md input-rounded form-control" placeholder="Clave Trabajo" name="claveTrabajo"  value="{{ old('claveTrabajo')}}"  >
												</div>
												<div class="form-group">
													<label for="cenep">La escuela donde laboras pertenece a la Confederación Nacional de Escuelas Particulares (CNEP)</label>
													<label class="radio-inline">
														<input type="radio" name="cenep"  value="Sí"> Sí
													</label>
													<label class="radio-inline">
														<input type="radio" name="cenep" id="cenepNo" value="No" checked > No
													</label>
                                                </div>
                                            </div>

                                            <div id="nivelEscolarBox" class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="nivelEscolar">Nivel Escolar</label>

                                                    <select id="nivelEscolar" name="nivelEscolar" class="input-md input-rounded form-control"  >

                                                        <option value="">Selecciona uno</option>
                                                        @foreach (trans('listado.nivelEscolar') as $nivel)
                                                        <option value="{{$nivel }}">{{$nivel }}</option>
                                                        @endforeach


                                                    </select>
                                                </div>
                                            </div>

                                            <div id="especificaBox" class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input id="especifica" type="text" class="input-md input-rounded form-control" placeholder="Especifica" name="especifica" value="{{ old('especifica')}}" >
                                                </div>
                                            </div>




                                            <div class="col-sm-12 col-xs-12 mt10 text-center mb20">

                                                <button type="button" class="button-3d button-md button-block button-pasific hover-ripple-out anterior" role="button" >Anterior</button>

                                                <button type="submit" class="button-3d button-md button-block button-blue hover-ripple-out" role="button" >Guardar Registro</button>
                                            </div>



                                        </div>





                                    </div>





                                </div>





                            </form>







                        </div>
                    </div>

                </div>
            </div>


            <div class="mastfoot">
                <div class="inner">

                    <p class="color-light text-center">&copy;2016 Fundación SM</p>
                </div>
            </div>

        </div>
    </div>
</div>




@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<!--<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>-->

<!-- Progress Bars
=====================================-->
<!--<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>-->

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



<script src="sm/assets/js/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>





@stop



