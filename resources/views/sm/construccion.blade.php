@extends('sm.global.base' )


@section('nav')

@include('sm.global.nav-interno')

@stop


@section('contenido')


<!-- Subheader Area
     ===================================== -->
<header class="bg-grad-stellar mt70">

    <div class="container">
        <div class="row mt20 mb30">
            <div class="col-md-6 text-left">
                <h3 class="color-light text-uppercase animated" data-animation="fadeInUp" data-animation-delay="100">{{ $evento->titulo}}<small class="color-light alpha7">{{ $evento->codigo}}</small></h3>
            </div>
            <div class="col-md-6 text-right pt35">
                <ul class="breadcrumb">
                    <li><a href="{{ route('page.inicio') }}">Home</a></li>
                    <li><a href="{{ route('page.inicio') }}#eventos">Eventos</a></li>
                    <li>{{ $evento->titulo}}</li>
                </ul>
            </div>
        </div>
    </div>

</header>




<div class="site-wrapper" style="background:url(sm/assets/img/bg/bg-pattern-2.png) 50% 50% repeat;">

            <div class="site-wrapper-inner">
                <div class="cover-container">                    
                    <div id="formLogin" class="inner cover text-center animated" data-animation="fadeIn" data-animation-delay="100">
                        <br>
                        <i class="under-construction color-pasific"></i><i class="under-construction color-dark"></i><br>
                        <h3 class="font-montserrat cover-heading mb20 mt20">Pagina en Construcción</h3>
                        
                         <label>Leave your email to get notify when the problem was gone.</label>
                        
                        
                        <br>
                    </div>

                    <div class="mastfoot">
                        <div class="inner">
                            <p class="color-dark text-center">&copy;2016 Fundación SM Derechos Reservados.</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>







@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



