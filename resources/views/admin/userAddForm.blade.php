@extends('admin.global.base')


@section('css-lib')

@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav') 
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> Usuario</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Alta Usuario
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                
                                
                                <div class="col-lg-12">
                                    <form role="form" action="{{ route('usuario.guarda') }}" method="POST" enctype="multipart/form-data" >
                                         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                         <div class="form-group" id="wrap-archivo">
                                        <label>Fotografía Usuario</label>
                                        <input type="file"  name="foto" >
                                    </div>
                                         
                                         
                                         <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">
                                        </div>
                                       
                                        
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea class="form-control" rows="3" name="descripcion">{{ old('descripcion') }}</textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Genero</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="genero" id="optionsRadios1" value="hombre" checked>Masculino
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="genero" id="optionsRadios2" value="mujer">Femenino
                                                </label>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Rol</label>
                                            <select name="rol" class="form-control">
                                                <option value="admin">Administrador</option>
                                                <option value="editor">Editor</option>
                                            </select>
                                        </div>
                                       
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                        <button type="reset" class="btn btn-default">Borrar</button>
                                    </form>
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

@stop





