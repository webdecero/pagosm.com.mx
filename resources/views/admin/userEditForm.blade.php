@extends('admin.global.base')


@section('css-lib')

@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav') 
    
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-user fa-fw"></i> Usuario</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Editar Usuario
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                
                                
                                <div class="col-lg-12">
                                    <form role="form" action="{{ route('usuario.actualiza') }}" method="POST"  enctype="multipart/form-data" >
                                         <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                          <input type="hidden" name="_id" value="{{$_id}}">
                                          
                                          <div class="form-group" id="wrap-archivo">
                                        <label>Fotografía Usuario</label>
                                        <input type="file"  name="foto" >
                                    </div>
                                         <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" value="{{ $record->email }}" disabled>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" name="password" >
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input type="text" class="form-control" name="nombre" value="{{ $record->nombre }}">
                                        </div>
                                       
                                        
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea class="form-control" rows="3" name="descripcion">{{ $record->descripcion }}</textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Genero</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="genero" id="optionsRadios1" value="hombre" 
                                                           
                                                           @if($record->genero=="hombre")
                                                           checked
                                                           @endif
                                                           
                                                           >Masculino
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="genero" id="optionsRadios2" value="mujer" 
                                                           
                                                           @if($record->genero=="mujer")
                                                           checked
                                                           @endif
                                                           
                                                           >Femenino
                                                </label>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Rol</label>
                                            <select name="rol" class="form-control">
                                                <option value="admin" 
                                                        
                                                           @if($record->rol=="admin")
                                                           selected
                                                           @endif
                                                           
                                                           >Administrador</option>
                                                <option value="editor"
                                                        
                                                           @if($record->rol=="editor")
                                                           selected
                                                           @endif
                                                           
                                                           >Editor</option>
                                                
                                                <option value="lector"
                                                        
                                                           @if($record->rol=="lector")
                                                           selected
                                                           @endif
                                                           
                                                           >Lector</option>
                                                
                                                
                                                
                                            </select>
                                        </div>
                                          
                                          
                                        <div class="form-group">
                                            <label>Estatus {{$record->estatus}}</label>
                                            
                                            <select name="estatus" class="form-control">
                                                <option value="1" 
                                                        
                                                           @if($record->estatus==1)
                                                           selected
                                                           @endif
                                                           
                                                           >Activo</option>
                                                <option value="0"
                                                        
                                                           @if($record->estatus==0)
                                                           selected
                                                           @endif
                                                           
                                                           >Inactivo</option>
                                            </select>
                                        </div>
                                       
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                        <button type="reset" class="btn btn-default">Borrar</button>
                                    </form>
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

@stop





