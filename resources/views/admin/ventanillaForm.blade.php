@extends('admin.global.base')


@section('css-lib')

<link href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-pencil"></i> Pago Ventanilla</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Panel
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">



                                <form role="form" action="{{ route('transaccion.ventanilla.actualiza') }}" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                      <input type="hidden" name="_id" value="{{$transaccion->_id}}">


                                 


                                    <div class="form-group">
                                        <label>Referencia</label>
                                        <input type="text" class="form-control" name="referencia" value="{{ $transaccion->referencia or '' }}"  disabled="" >
                                        
                                    </div>
                

                                    <div class="form-group">
                                        <label>Exchange Rate</label>
                                        <input type="text" class="form-control" name="exchangeRate" value="{{ $transaccion->exchangeRate or '' }}"  disabled="" >
                                        
                                    </div>

                                    <div class="form-group">
                                        <label>Precio Final</label>
                                        <input type="text" class="form-control" name="precioFinal" value="{{ $transaccion->precioFinal or '' }}"  disabled="" >
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Moneda Final</label>
                                        <input type="text" class="form-control" name="monedaFinal" value="{{ $transaccion->monedaFinal or '' }}"  disabled="" >
                                        
                                    </div>
    
    
                                    <div class="form-group">
                                        <label>Fecha creación</label>
                                        <input type="text" class="form-control " name="created_at" value="{{  $transaccion->created_at }}" disabled="" >
                                        
                                    </div>

                                      
                                      
                                      
                                    <div class="form-group">
                                        <label>Comentario Ventanilla</label>
                                        <input type="text" class="form-control" name="comentarioVentanilla"  required >
                                        
                                    </div>


                                    <div class="form-group">
                                        <label>Archivo Referencia</label>
                                        <input type="file" class="form-control" name="archivoVentanilla" required="" >
                                          <p class="help-block">Maximo 5MB</p>
                                        
                                    </div>






                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    




                                </form>








                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

<script src="admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>



<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@stop





