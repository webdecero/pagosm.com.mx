<!DOCTYPE html>
<html lang="{{App::getLocale()}}" >
    <head>
        @include('admin.global.head')

</head>

<body>




        @yield('contenido')


        @section('footer')
        <!-- Footer -->
        @include('admin.global.footer')
        <!-- Fin de footer -->
        @show

        @include('admin.global.mensajes') 


        @include('admin.global.js')

		
    </body>
</html>