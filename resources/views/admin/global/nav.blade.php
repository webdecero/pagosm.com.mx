<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">


    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}" ><img src="logo/logo.png" class="img-responsive" alt="Logo" style="width: 65%"></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <!-- /.dropdown -->
        <li class="dropdown">

            <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                {{$user->email}}
                @if(isset($user->thumbFoto))
                <img src="{{$user->thumbFoto}}" style="height: 45px;" class="img-responsive img-thumbnail" >
                @else 
                <i class="fa fa-user fa-fw"></i>
                @endif 
                <i class="fa fa-caret-down"></i>

            </a>
            <ul class="dropdown-menu dropdown-user">


                @if($user->rol=="admin")
                <li><a href="{{ route('usuario.formEdit', [$user->_id]) }}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                </li>

                <li class="divider"></li>
                @endif

                <li><a href="{{ route('logout') }}" ><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <!--                <li>
                                    <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                                </li>-->

                <!--                <li>
                                    <a href="#">
                                        <i class="fa fa-bar-chart-o fa-fw"></i> CRM<span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level">
                                        
                
                                        
                                        <li>
                                            <a href="{{route("newsletter.panel")}}"><i class="fa fa-newspaper-o"></i> Newsletter</a>
                                        </li>
                                    </ul>
                                     /.nav-second-level 
                                </li>-->


                @if($user->rol=="admin")
                <li>
                    <a href="{{route("usuario.panel")}}"><i class="fa fa-users fa-fw"></i> Usuarios</a>
                </li>
                @endif

                <li>
                    <a href="{{route("pago.panel")}}"><i class="fa fa-credit-card"></i> Pagos</a>
                </li>
                
                <li>
                    <a href="{{route("evento.panel")}}"><i class="fa fa-calendar"></i> Eventos</a>
                </li>



                <li>
                    <a href="{{route("coloquio.panel")}}"><i class="fa fa-comment"></i> Coloquios</a>
                </li>

                <li>
                    <a href="{{route("descuento.pais.panel")}}"><i class="fa fa-flag"></i> Descuentos País</a>
                </li>


                <li>
                    <a href="{{route("descuento.fecha.panel")}}"><i class="fa fa-clock-o"></i> Descuentos Fecha</a>
                </li>
                
                
                
                <li>
                    <a href="{{route("descuento.codigo.panel")}}"><i class="fa fa-barcode"></i> Descuentos Codigo</a>
                </li>

                
                
                <li>
                    <a href="{{route("transaccion.panel")}}"><i class="fa fa-file-o"></i> Transacciones</a>
                </li>

                
                
                
                <li>
                    <a href="{{route("donativo.panel")}}"><i class="fa fa-list-alt"></i> Comprobantes fiscales</a>
                </li>
                
                
                
                <li>
                    <a href="{{route("paginas.formEdit", ['clave'=> 'Recibo'])}}"><i class="fa fa-info"></i> Info Recibo donativo</a>
                </li>
                
                
                <li>
                    <a href="{{route("paginas.formEdit", ['clave'=> 'Banner'])}}"><i class="fa fa-inbox"></i> Banner</a>
                </li>


                <li>
                    <a target="_blank" href="{{ url('/') }}"><i class="fa fa-institution"></i> Vistar web</a>
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

