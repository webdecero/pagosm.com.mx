@if(session()->has('error'))
<!-- ======================================= Mensajes: Error Personalizados ======================================= -->
<div class="modal fade modalMensaje" tabindex="-1" role="dialog" aria-labelledby="tituo error" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content  text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans( 'mensajes.alerta.titulo' )}}</h4>
            </div>
            <div class="modal-body ">
                <h5>{{trans( 'mensajes.alerta.detecto' )}}</h5>
                <p class="text-danger c-font-red" >{!! session('error') !!}</p>
                @if($errors->has())
                <ul>
                    @foreach ($errors->all() as $error)
                    <li class="text-danger c-font-red" >
                        {{ $error }} 
                    </li>
                    @endforeach
                </ul>
                @endif

                <a class="btn btn-outline btn-success" data-dismiss="modal" href="#">{{trans( 'etiquetas.modal.cerrar' )}}</a>
            </div>
        </div>
    </div>
</div>


@endif
@if(session()->has('mensaje'))
<!-- ======================================= Mensajes: Mensaje ======================================= -->
<div class="modal fade modalMensaje"  tabindex="-1" role="dialog" aria-labelledby="titulo mensaje" aria-hidden="true" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >{{trans( 'mensajes.mensaje.titulo' )}}</h4>
            </div>
            <div class="modal-body ">
                <p>{!! session('mensaje') !!}</p>
                <a class="btn btn-outline btn-success" data-dismiss="modal" href="#">{{trans( 'etiquetas.modal.cerrar' )}}</a>
            </div>
        </div>
    </div>
</div>
@endif

<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="modal mensaje" aria-hidden="true" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                
                <h4 class="modal-title" >{{trans( 'mensajes.mensaje.titulo' )}}</h4>
            </div>
            <div class="modal-body">
                <p class="mensaje"></p>
                <ul class="error">
                    
                </ul>
                
                <a class="btn btn-outline btn-success" data-dismiss="modal" href="#">{{trans( 'etiquetas.modal.cerrar' )}}</a>
            </div>
        </div>
    </div>
</div>


<!-- ======================================= Mensajes: Borro ======================================= -->
<div class="modal fade" id="modalBorro" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content text-uppercase text-center">
            <div class="modal-body ">
                <p>¿Deseas borrar elemento? Este cambio es irreversible.</p>
                <a class="btn btn-success btn-lg nav-left acept" data-dismiss="modal" href="#">Aceptar</a>
                <a class="btn btn-success btn-lg nav-right cancel" data-dismiss="modal" href="#">Cancelar</a>
            </div>
        </div>
    </div>
</div>




<!-- Modal Crop -->
<div class="modal fade" id="imageModalCrop" aria-labelledby="modalLabel" role="dialog" tabindex="-1" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog " role="document">
        <div class="modal-content">

            <div class="modal-body">

                <div>
                    <img id="imageModalCropPreview" class="img-responsive" src="sm/img/placehold.png" >
                </div>

                <input type="file" id="imageModalCropFile" name="imageModalCropFile" accept="image/*" class=""   >

            </div>
            <div class="modal-footer">


                <div class="blockError">
                    <ul ></ul>
                </div>


                <button type="button" class="btn btn-primary pull-left" data-loading-text="Cargando..." autocomplete="off" id="updloadModalImage" >Upload Imagen</button>

                X: <input type="text" name=""  class="imageCropW" value="" readonly="">

                H: <input type="text" name=""  class="imageCropH" value="" readonly="">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



