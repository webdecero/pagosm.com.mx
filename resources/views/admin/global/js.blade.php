<!-- Default JS -->
@section('js-default')
<!-- jQuery -->

  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  
    <script src="admin/js/tinymce_es.js"></script>
  
    <script src="admin/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="admin/js/jquery-ui.min.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>
    
    <script src="admin/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="admin/bower_components/jquery-validation/src/localization/messages_es.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="admin/dist/js/sb-admin-2.js"></script>



@show

<!-- LIB JS Opcionales que se remplazan en vistas-->
@yield('js-lib')


<!-- script JS -->
@section('js-script')
<script src="admin/js/scripts.js"></script>
@show



