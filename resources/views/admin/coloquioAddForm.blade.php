@extends('admin.global.base')


@section('css-lib')

<link href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-pencil"></i> Coloquio</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Alta Coloquio {{$evento->titulo}}
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">



                                <form role="form" action="{{ route('coloquio.guarda') }}" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <input type="hidden" name="estatus" value="true">
                                    <input type="hidden" name="eventoId" value="{{$evento->id}}">




                                    <div class="form-group">
                                        <label>Día Fecha {{$evento->titulo}}</label>
                                        
                                        <select name="fechaDia" class="form-control" >

                                            @foreach($evento->rangoDateTime as $fecha)
                                                <option value="{{ $fecha->format("d-m-Y")}}" >{{ $fecha->format("d-m-Y")}}</option>
                                            @endforeach
                                        </select>

                                    </div>


                                    <div class="form-group">
                                        <label>Título</label>
                                        <input type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label >Descripcion</label>
                                        <textarea class="form-control" rows="3" name="descripcion" required >{{ old('descripcion') }}</textarea>
                                    </div>


                                    <div class="form-group">
                                        <label>Lugar</label>
                                        <input type="text" class="form-control" name="lugar" value="{{ old('lugar') }}" required >
                                    </div>


                                    <div class="form-group">
                                        <label>Hora Inicio</label>
                                        <input type="text" class="form-control horasMinutoMask" name="fechaInicio" value="{{ old('fechaInicio') }}" required >
                                         <p class="help-block">Utilizar 24horas</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Hora Final</label>
                                        <input type="text" class="form-control horasMinutoMask" name="fechaFinal" value="{{ old('fechaFinal') }}" required >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>


                                    <div class="form-group">
                                        <label>Capacidad</label>
                                        <input type="number" class="form-control" name="capacidad" value="{{ old('capacidad') }}" required min="0" max="1000" >
                                        <p class="help-block">Si el es igual a 0 singnifica que es ilimitado</p>
                                    </div>















                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    <button type="reset" formnovalidate name="cancel" class="btn btn-default">Borrar</button>




                                </form>








                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')


<script src="admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>


<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@stop





