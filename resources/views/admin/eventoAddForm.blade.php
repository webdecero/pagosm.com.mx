@extends('admin.global.base')


@section('css-lib')

<link href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-pencil"></i> Evento</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Alta Evento
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">



                                <form role="form" action="{{ route('evento.guarda') }}" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    <input type="hidden" name="estatus" value="true">


                                    <div class="form-group">
                                        <label>Título</label>
                                        <input type="text" class="form-control" name="titulo" value="{{ old('titulo') }}" required maxlength="60" >
                                        <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"></span>   </p>
                                    </div>

                                    <div class="form-group">
                                        <label>Descripción Corta</label>
                                        <input type="text" class="form-control" name="descripcionCorta" value="{{ old('descripcionCorta') }}" required maxlength="155" >
                                        <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"></span>   </p>
                                    </div>


                                    <div class="form-group">
                                        <label>Título Largo</label>
                                        <input type="text" class="form-control" name="tituloLargo" value="{{ old('tituloLargo') }}" required >
                                    </div>



                                    <div class="form-group">
                                        <label >Descripción</label>
                                        <textarea class="form-control" rows="3" name="descripcion" required >{{ old('descripcion') }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label >Ubicación</label>
                                        <textarea class="form-control" rows="3" name="ubicacion" required >{{ old('ubicacion') }}</textarea>
                                    </div>

									<div class="form-group">
                                        <label>Categoría</label>
                                        <input type="text" class="form-control" name="categoria" value="{{ old('categoria') }}" required >
                                    </div>

                                    <div class="form-group">
                                        <label>Código</label>
                                        <input type="text" class="form-control" name="codigo" value="{{ old('codigo') }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label>URL</label>
                                        <input type="url" class="form-control" name="url" value="{{ old('url') }}" required >
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" required >
                                    </div>



                                    <div class="form-group">


                                        <!--                                        <button class="btn btn-success btn-sm addInput  form-control-static" type="button">
                                            <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span> Agregar otra imagen
                                        </button>-->


                                        <div class="row campoInput">




                                            <div class="col-md-8 text-center fileImageUploadPreview ">

                                                <img  class="img-responsive  form-control-static"    src="http://placehold.it/520x520" >


                                                <input type="text" name="imagen"  class="hiddenImagePath form-control" value="{{ old('imagen') }}" readonly="" required="" >

                                                <p class="help-block">http://placehold.it/520x520</p>
                                            </div>


                                            <div class="col-md-4 text-center ">

                                                <button type="button" class="btn btn-primary btn-md lunchImageModal "

                                                        data-url="{{ route('image.load') }}"
                                                        data-target="#imageModalCrop"
                                                        data-toggle="modal"
                                                        data-min-width="520"
                                                        data-min-height="520"
                                                        data-aspect-ratio="1 / 1"
                                                        data-path="sm/img"
                                                        data-format="jpg"  data-quality="80"
                                                        data-prefix=""

                                                        >
                                                    Cargar imagen
                                                </button>
                                            </div>
                                        </div>



                                    </div>






                                    <div class="form-group">
                                        <label>Precio</label>
                                        <input type="number" class="form-control" name="precio" value="{{ old('precio') }}" required min="0"  >

                                    </div>



                                    <div class="form-group">
                                        <label>Moneda</label>
                                        <select name="moneda" class="form-control" >
                                            <option value="MXN" selected="">MXN</option>
                                            <option value="USD">USD</option>

                                        </select>

                                        <p class="help-block">Si el estatus es no publicado el artículo no estara visible dentro del sitio</p>
                                    </div>






                                    <div class="form-group">
                                        <label>Fecha Inicio</label>
                                        <input type="text" class="form-control fechaHoraMask" name="fechaInicio" value="{{ old('fechaInicio') }}" required >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Fecha Final</label>
                                        <input type="text" class="form-control fechaHoraMask" name="fechaFinal" value="{{ old('fechaFinal') }}" required >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>




                                    <div class="form-group">
                                        <label >Instrucciones</label>
                                        <textarea class="form-control" rows="3" name="instrucciones" required >{{ old('instrucciones') }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label >Comprobante</label>
                                        <textarea class="form-control" rows="3" name="comprobante" required >{{ old('comprobante') }}</textarea>
                                    </div>





                                    <div class="form-group">


                                        <!--                                        <button class="btn btn-success btn-sm addInput  form-control-static" type="button">
                                            <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span> Agregar otra imagen
                                        </button>-->


                                        <div class="row campoInput">




                                            <div class="col-md-8 text-center fileImageUploadPreview ">

                                                <img  class="img-responsive  form-control-static"    src="http://placehold.it/500x214" >


                                                <input type="text" name="imagenComprobante"  class="hiddenImagePath form-control" value="{{ old('imagenComprobante') }}" readonly="" required="" >

                                                <p class="help-block">http://placehold.it/233x100</p>
                                            </div>


                                            <div class="col-md-4 text-center ">

                                                <button type="button" class="btn btn-primary btn-md lunchImageModal "

                                                        data-url="{{ route('image.load') }}"
                                                        data-target="#imageModalCrop"
                                                        data-toggle="modal"
                                                        data-min-width="233"
                                                        data-min-height="100"
                                                        data-aspect-ratio="21 / 9"
                                                        data-path="sm/img"
                                                        data-format="jpg"  data-quality="80"
                                                        data-prefix=""

                                                        >
                                                    Cargar imagen
                                                </button>
                                            </div>
                                        </div>



                                    </div>





                                    <div class="form-group">
                                        <label>Limite</label>
                                        <input type="number" class="form-control" name="limite" value="{{ old('limite') }}" required min="0"  >
                                        <p class="help-block">Si el es igual a 0 singnifica que es ilimitado</p>
                                    </div>



									<div class="form-group">
                                        <label>Categoría</label>
                                        <input type="text" class="form-control" name="categoria" value="{{ old('categoria') }}" required >
                                    </div>

									<div class="form-group">
                                        <label>Orden</label>
                                        <input type="text" min="0" max="100" minlength="1" maxlength="3" data-number="true" class="form-control" name="orden" value="{{ old('orden') }}" required >
                                    </div>



                                    <div class="form-group">
                                        <h3 class="form-control-static">Panel Streaming</h3>
                                    </div>

                                    <div class="form-group">
                                        <label>Activar Streaming</label>
                                        <select name="streamingActiva" class="form-control" >
                                            <option value="true"  >Activo</option>
                                            <option value="false" selected="" >Inactivo</option>

                                        </select>

                                        <p class="help-block">Si se encuentra inactiva no estara disponible dentro del panel Historial de compras</p>
                                    </div>




                                    <div class="form-group">
                                        <label>Iframe Streaming</label>
                                        <textarea class="form-control" rows="3" name="streaming"  >{{ old('streaming') }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label> Streaming Fecha Inicio</label>
                                        <input type="text" class="form-control fechaHoraMask" name="streamingFechaInicio" value="{{ old('streamingFechaInicio') }}"  >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Streaming Fecha Final</label>
                                        <input type="text" class="form-control fechaHoraMask" name="streamingFechaFinal" value="{{ old('streamingFechaFinal') }}"  >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>


                                    <div class="form-group">
                                        <h3 class="form-control-static">Semilla MIT</h3>
                                    </div>

                                    <div class="form-group">
                                        <label>Configuración Cuenta Banco</label>
                                        <select name="semilla" class="form-control" >

                                            @foreach ( Config::get("webServiceRetail") as $key => $item )
                                            <option value="{{$key}}" >{{$key.' - id_company('.$item['id_company'].')'}}</option>
                                            @endforeach

                                        </select>

                                        <p class="help-block">Si se encuentra inactiva no estara disponible dentro del panel Historial de compras</p>
                                    </div>






                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    <button type="reset" formnovalidate name="cancel" class="btn btn-default">Borrar</button>




                                </form>








                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

<script src="admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>



<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@stop





