@extends('admin.global.base')


@section('css-lib')
<!-- DataTables CSS -->
<link href="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">


@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav') 

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-graduation-cap"></i> Transacción </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tabla Transacción
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">



                            <form method="POST" action="{{ route('dataTable.transaccion')}}/0/1"  class="form-inline" >
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                <h5>Filtros:</h5>


                                

                                <div class="form-group">
                                    <label for="estatus">Respuesta</label> <br>

                                    <select class="reloadTable" name="response">
                                        <option value="">Ninguno</option>
                                        <option value="incompleta">Incompleta</option>
                                        <option value="approved">Aprobado</option>
                                        <option value="error">Error</option>
                                        <option value="denied">Denegado</option>
                                        
                                        


                                    </select>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label for="evento_id">Evento</label> <br>

                                    <select class="reloadTable" name="evento_id">
                                        <option value="">Ninguno</option>
                                       
                                         @foreach ($eventos as $evento)
                                            <option value="{{$evento->_id }}">{{$evento->titulo }}</option>
                                            @endforeach


                                    </select>
                                </div>








                                <div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

                                   
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="submit"class="btn btn-success " >

                                            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL

                                        </button>
                                    </div>
                                </div>



                                <hr>
                                <div style="clear: both;" ></div>


                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover datatable" id="dataTables-example" data-url="{{ route('dataTable.transaccion')}}">

                                        <thead class="no-border">

                                            <tr>

                                                <th data-details="true" ></th>

                                              
                                                <th data-name="referencia" data-orderable="true"  data-order="asc">
                                                    <strong>Referencia</strong>
                                                </th>
                                                <th data-name="response" data-orderable="true"  data-order="asc">
                                                    <strong>Respuesta</strong>
                                                </th>


                                                
                                               
                                                <th data-name="evento-titulo" data-orderable="true">
                                                    <strong>Evento</strong>
                                                </th>
                                                
                                                
                                                
                                                
                                                <th data-name="precioFinal" data-orderable="true">
                                                    <strong>Precio</strong>
                                                </th>
                                                
                                                
                                                <th data-name="monedaFinal" data-orderable="true">
                                                    <strong> Moneda </strong>
                                                </th>

                                               
                                                
                                                
                                                <th data-name="created_at" data-orderable="true" data-order="asc" >
                                                    <strong>Fecha Alta</strong>
                                                </th>
                                                
                                                

                                             
                                                <th data-name="opciones">
                                                    <strong>Opciones</strong>
                                                </th>
                                            </tr>

                                        </thead>

                                    </table>
                                </div>
                            </form>






                        </div>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->




@stop

@section('js-lib')

<!-- DataTables JavaScript -->
<script src="admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

@stop





