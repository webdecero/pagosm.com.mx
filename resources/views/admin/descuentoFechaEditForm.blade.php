@extends('admin.global.base')


@section('css-lib')

<link href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-pencil"></i> Descuento Fecha</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edita Desceunto
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">



                                <form role="form" action="{{ route('descuento.fecha.actualiza') }}" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                      <input type="hidden" name="_id" value="{{$descuentoFecha->_id}}">


                                    <div class="form-group">
                                        <label>Evento</label>
                                        <select name="" class="form-control" disabled="">

                                            @foreach ($eventos as $evento)
                                            <option value="{{$evento->_id }}" 
                                                    @if($descuentoFecha->evento_id == $evento->_id)
                                                    selected="" 
                                                    @endif
                                                    
                                                    >{{$evento->titulo }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                 


                                    <div class="form-group">
                                        <label>Título</label>
                                        <input type="text" class="form-control" name="titulo" value="{{ $descuentoFecha->titulo or '' }}" required maxlength="60" >
                                        <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"></span>   </p>
                                    </div>


                                    <div class="form-group">
                                        <label >Descripción</label>
                                        <textarea class="form-control" rows="3" name="descripcion" required >{{ $descuentoFecha->descripcion or '' }}</textarea>
                                    </div>


                                    <div class="form-group">
                                        <label>Porcentaje %</label>
                                        <input type="number" class="form-control" name="porcentaje" value="{{ $descuentoFecha->porcentaje or '' }}" required min="0" max="100"  >

                                    </div>



                                    <div class="form-group">
                                        <label>Fecha Inicio</label>
                                        <input type="text" class="form-control fechaHoraMask" name="fechaInicio" value="{{ $descuentoFecha->fechaInicio->format('d-m-Y H:i') }}" required >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Fecha Final</label>
                                        <input type="text" class="form-control fechaHoraMask" name="fechaFinal" value="{{ $descuentoFecha->fechaFinal->format('d-m-Y H:i')}}" required >
                                        <p class="help-block">Utilizar 24horas</p>
                                    </div>



                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    <button type="reset" formnovalidate name="cancel" class="btn btn-default">Borrar</button>




                                </form>








                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

<script src="admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>



<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@stop





