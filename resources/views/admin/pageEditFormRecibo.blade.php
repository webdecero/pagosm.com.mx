@extends('admin.global.base')


@section('css-lib')

<link href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-pencil"></i> {{$pagina->clave}}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edita 
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">



                                <form role="form" action="{{ route('paginas.actualiza') }}" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="clave" value="{{$pagina->clave}}">

                                    <div class="form-group">
                                        <label>Título</label>
                                        <input type="text" class="form-control" name="titulo" value="{{ $pagina->titulo or '' }}"  required="" >

                                    </div>

                                    <div class="form-group">
                                        <label>Título Largo</label>
                                        <input type="text" class="form-control" name="tituloLargo" value="{{ $pagina->tituloLargo or '' }}"   >

                                    </div>



                                    <div class="form-group">
                                        <label >Contenido</label>
                                        <textarea class="form-control" rows="3" name="contenido" required >{{ $pagina->contenido or '' }}</textarea>
                                    </div>

                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    <button type="reset" formnovalidate name="cancel" class="btn btn-default">Borrar</button>

                                </form>

                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')


<script src="admin/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>


<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@stop





