@extends('admin.global.base')


@section('css-lib')
<!-- DataTables CSS -->
<link href="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">


@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav') 

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-graduation-cap"></i> Evento</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tabla Evento
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">



                            <form method="POST" action="{{ route('dataTableEvento')}}/0/1"  class="form-inline" >
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                <h5>Filtros:</h5>


                                

                                <div class="form-group">
                                    <label for="estatus">Estatus</label> <br>

                                    <select class="reloadTable" name="estatus">
                                        <option value="">Ninguno</option>
                                        <option value="true">Activo</option>
                                        <option value="false">Inactivo</option>


                                    </select>
                                </div>



                                <div class="form-group">
                                    <label for="moneda">Moneda</label> <br>

                                    <select class="reloadTable" name="moneda">
                                        <option value="">Ninguno</option>
                                        <option value="MXN">MXN</option>
                                        <option value="USD">USD</option>


                                    </select>
                                </div>


								
								<div class="form-group">
                                    <label for="categoria">Categoría</label> <br>

                                    <select class="reloadTable" name="categoria">
                                        <option value="">Ninguno</option>
										
										
                                            @foreach ( $categorias as $key => $item )
											@if($item['categoria'])
											<option value="{{$item['categoria']}}" >{{$item['categoria']}}</option>
											@endif
                                            
                                            @endforeach


                                    </select>
                                </div>


                                <div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

                                    <div class="btn-group" role="group" aria-label="...">

                                        <a class="btn btn-success" href="{{ route('evento.form')}}" >
                                            <i class="fa fa-plus-circle"></i> NUEVO
                                        </a>

                                    </div>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="submit"class="btn btn-success " >

                                            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL

                                        </button>
                                    </div>
                                </div>



                                <hr>
                                <div style="clear: both;" ></div>


                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover datatable" id="dataTables-example" data-url="{{ route('dataTableEvento')}}">

                                        <thead class="no-border">

                                            <tr>

                                                <th data-details="true" ></th>

                                              
                                                <th data-name="titulo" data-orderable="true"  data-order="asc">
                                                    <strong>Título</strong>
                                                </th>


                                                <th data-name="descripcionCorta" data-orderable="true">
                                                    <strong>Descripción</strong>
                                                </th>
                                                
                                                
                                                <th data-name="moneda" data-orderable="true">
                                                    <strong>Moneda</strong>
                                                </th>
                                                <th data-name="categoria" data-orderable="true">
                                                    <strong>Categoría</strong>
                                                </th>
                                                
                                                <th data-name="codigo" data-orderable="true">
                                                    <strong>codigo</strong>
                                                </th>
                                                <th data-name="nuevoColoquio" data-orderable="false">
                                                    <strong>Coloquio</strong>
                                                </th>

                                                <th data-name="created_at" data-orderable="true"><strong>Fecha Alta</strong></th>

                                                <th data-name="opciones">
                                                    <strong>Opciones</strong>
                                                </th>
                                            </tr>

                                        </thead>

                                    </table>
                                </div>
                            </form>






                        </div>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->




@stop

@section('js-lib')

<!-- DataTables JavaScript -->
<script src="admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

@stop





