@extends('admin.global.base')


@section('css-lib')
<!-- DataTables CSS -->
<link href="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">


@stop


@section('contenido')


<div id="wrapper">

    @include('admin.global.nav') 

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-users fa-fw"></i> Usuarios</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tabla Usuarios
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">



                            <form method="POST" action="{{ route('dataTableUsuarios')}}/0/1"  class="form-inline" >
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


                                <h5>Filtros:</h5>


                                <div class="form-group">
                                    <label for="rol">Rol</label> <br>

                                    <select class="reloadTable" name="rol">
                                        <option value="">Ninguno</option>
                                        <option value="admin">Administrador</option>
                                        <option value="editor">Editor</option>
                                        <option value="user">Usuario</option>


                                    </select>
                                </div>


<!--                                <div class="form-group">
                                    <label for="genero">Género</label> <br>

                                    <select class="reloadTable" name="genero">
                                        <option value="">Ninguno</option>
                                        <option value="hombre">Hombre</option>
                                        <option value="mujer">Mujer</option>


                                    </select>
                                </div>-->

                                <div class="form-group">
                                    <label for="estatus">Estatus</label> <br>

                                    <select class="reloadTable" name="estatus">
                                        <option value="">Ninguno</option>
                                        <option value="true">Activo</option>
                                        <option value="false">Desactivado</option>


                                    </select>
                                </div>


                                <div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

                                    <div class="btn-group" role="group" aria-label="...">
                                        <a class="btn btn-success " href="{{ route('usuario.form')}}" >

                                            <i class="fa fa-plus-circle"></i> NUEVO

                                        </a>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <button type="submit"class="btn btn-success " >

                                            <i class="fa fa-file-excel-o"></i> EXPORT EXCEL

                                        </button>
                                    </div>
                                </div>











                                <hr>
                                <div style="clear: both;" ></div>


                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover datatable" id="dataTables-example" data-url="{{ route('dataTableUsuarios')}}">

                                        <thead class="no-border">

                                            <tr>

<!--                                            <th data-details="true" ></th>-->

                                                <th data-name="id">
                                                    <strong>Id</strong>
                                                </th>
                                                
                                                <th data-name="nombre" data-orderable="true" >
                                                    <strong>Nombre</strong>
                                                </th>
                                                
                                                <th data-name="apellidoPaterno" data-orderable="true"  >
                                                    <strong>Paterno</strong>
                                                </th>
                                                
                                                <th data-name="apellidoMaterno" data-orderable="true" >
                                                    <strong>Materno</strong>
                                                </th>
                                                
                                                <th data-name="email" data-orderable="true">
                                                    <strong>Email</strong>
                                                </th>
                                                <th data-name="pais" data-orderable="true">
                                                    <strong>País</strong>
                                                </th>
                                                <th data-name="rol" data-orderable="true">
                                                    <strong>Rol</strong>
                                                </th>

                                                <th data-name="estatus" data-orderable="true">
                                                    <strong>Estatus</strong>
                                                </th>

                                                <th data-name="created_at" data-orderable="true" data-order="asc" >
                                                    <strong>Fecha Alta</strong>
                                                </th>
                                                
                                                <th data-name="opciones">
                                                    <strong>Opciones</strong>
                                                </th>
                                            </tr>

                                        </thead>

                                    </table>
                                </div>
                            </form>






                        </div>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@stop

@section('js-lib')

<!-- DataTables JavaScript -->
<script src="admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

@stop





