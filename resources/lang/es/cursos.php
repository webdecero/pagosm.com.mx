<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return[
    'MicroCurso' => [
        'label' => 'Micro Curso',
        'campoTutor' => false,
        'campoPonente' => false,
        'url' => false,
        
    ],
    'Mooc' => [
        'label' => 'Mooc´s',
        'campoTutor' => false,
        'campoPonente' => false,
        'url' => false,
        
    ],
    'Curso' => [
        'label' => 'Curso',
        'campoTutor' => true,
        'campoPonente' => false,
        'url' => true,
    ],
    'Conferencia' => [
        'label' => 'Conferencia',
        'campoTutor' => false,
        'campoPonente' => true,
        'url' => true,
    ],
    'Seminario' => [
        'label' => 'Seminario',
        'campoTutor' => true,
        'campoPonente' => false,
        'url' => true,
    ],
    'Diplomado' => [
        'label' => 'Diplomado',
        'campoTutor' => true,
        'campoPonente' => false,
        'url' => true,
    ]
];
