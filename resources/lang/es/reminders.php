<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "La contraseña debe contener al menos 6 caracteres.",

    "user"     => "No podemos encontrar a un usuario con ese correo electrónico.",

    "token"    => "Este token de recuperación de contraseña es inválido.",

    "sent"     => "Te hemos enviado un recordatorio de la contraseña a tu correo",
    
    "title-form"     => "Restablecer Contraseña",
        
    "complete-form"     => "Para restablecer contraseña, completar el siguiente formulario:",

);
