<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    
    'modal' => array(
        'cerrar'=>'Cerrar',
        
    ),
    'label' =>  array(
        'ingresar'=>'Ingresar',
        'miParticipacion'=>'MI PARTICIPACIÓN',
        'nombre'=>'Nombre',
        'usuario'=>'Usuario',
        'ayuda'=>'¿NECESITAS AYUDA?',
        'reproducir'=>'REPRODUCIR VIDEO',
        'adulto'=>'ADULTO',
        'menor'=>'MENOR DE 18 AÑOS',
        'registro'=> 'Registro',
        'registro-pago'=> 'Realizar pago',
        'mayorRegistro'=> 'Registro adulto',
        'menorRegistro'=> 'Registro menor de 18 años',
        'ingresar'=> 'Ingresar',
        'hola'=> 'HOLA',
        'salir'=> 'SALIR',
        'categoria'=> 'Modalidad',
        'pais'=> 'Pa&iacute;s',
        'participacion'=> 'PARTICIPACI&Oacute;N',
        'subir'=> 'SUBIR FOTOS O VIDEOS',
        'contacto'=> 'CONT&Aacute;CTANOS',
        'accesarRegistrar'=> 'Accesar/Registrar',
        
        
        'hubo'=>'HUBO UN PROBLEMA',
        'cerrar'=>'CERRAR',
        'progreso'=>'PROGRESO',
        'sentimos'=>'¡Lo sentimos!',
        
        'cancelar'=>'Cancelar',
        'enviar'=>'Enviar',
        'aceptar'=>'Aceptar',
        
        
        
        
        
    ),
    'botones' => array(
        'enviar'=>'Enviar',
        'guardar'=>'Guardar',
        'seleccionar'=>'SELECCIONAR'
    ),
    
    
);
