<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return[
    'ordeBy' => [
        'created_at-desc' => 'Más nuevo',
        'created_at-asc' => 'Más antiguo',
        'precio-desc' => 'Precio más alto',
        'precio-asc' => 'Precio más bajo',
        'ranking-desc' => 'Ranking más alto',
        'ranking-asc' => 'Ranking más bajo',
        
    ],
    'paginate' => [
        '6' => '6 elementos',
        '12' => '12 elementos',
        '24' => '24 elementos',
        '48' => '48 elementos',
        
    ],
    'filtro' => [
        'tipo' => 'Categoría: :param',
        'tags' => 'Tag: :param',
        'rangePrecio' => 'Precio: :param',
        'ordeBy' => 'Ordenado por: :param',
        'query' => 'Buscar: ":param"',
        
        
    ]
];
