<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return array(
    'tituloPrincipal' => 'formatodoc.com',
    'alerta' => array(
        'titulo' => 'Lo sentimos, hubo un problema',
        'detecto' => 'Se detecto lo siguiente:',
    ),
    'mensaje' => array(
        'titulo' => 'Mensaje',
    ),
    'operacion' => array(
        'correcta' => 'Registro ha sido exitoso.',
        'incorrecta' => 'Operación Incorrecta, intente nuevamente',
        'error' => 'Operación Errónea, intente mas tarde',
        'noEncotrado' => 'Operación Incorrecta, elemento no existente'
    ),
    'usuario' => array(
        'login' => 'Acceso Correcto',
        'noEncontrado' => 'El usuario no ha sido encontrado',
        'yaRegistrado' => 'El usuario ya se encuentra registrado',
        'invalidEmail' => 'Email invalido',
        'YaValidado' => 'El usuario ya se encuentra validado, es necesario autenticar',
        'noLogin' => 'Es necesario autenticar',
        'recuperar' => 'Se envío una nueva contraseña a su correo electrónico'
    ),
    'registro' => array(
        'exito' => 'Guardado Correcto',
        'incompleto' => 'Registro incompleto',
        'continuar' => 'Es necesario finalizar Registro',
        'validacionEmail' => 'Un mensaje de validación ha sido enviado a tu correo electrónico. Revisa tu correo para continuar con el proceso de activación de tu cuenta.',
    ),
    'logout' => array(
        'finalizar' => 'La sesión caducó, pero puedes volver a ingresar ',
        'correcto' => 'Registro incompleto'
    ),
    'ie' => array(
        'titulo' => 'TU NAVEGADOR NO ES COMPATIBLE',
        'mensaje' => 'TE RECOMENDAMOS UTILIZAR LA ÚLTIMA VERSIÓN DE:'
    ),
    'session' => array(
        'TokenMismatchException' => 'Ha pasado mucho tiempo, es necesario refrescar la pagina, intentalo nuevamente.',
        'privilegios' => 'No tienes suficientes privilegios para acceder',
        'politica' => 'Es necesario aceptar política de privacidad',
    ),
    'webpay' => array(
        'error' => 'Ocurrió un error al realizar el pago<br> No se realizó ningún cargo a su tarjeta.<br> Favor de intentar con otra tarjeta.<br>',
        'denied' => 'Cobro declinado<br>No se realizó ningún cargo a su tarjeta.<br>La operación fue declinada por su banco emisor.<br>Favor de intentar con otra tarjeta.',
        'approved' => 'Cobro exitoso',
        'noRespuesta' => 'Ocurrió un error al realizar el pago.',
        'refresh' => 'Ha caducado la página, es necesario pulsar el botón comprar nuevamente.',
         'duplicada' => 'Ocurrió un error al procesar la transacción<br> No se realizó ningún cargo a su tarjeta.<br> Favor de intentar nuevamente.<br>',
        'utilizado' => 'La referencia ha sido ocupada,  es necesario pulsar el botón comprar nuevamente.',
        'comprado' => 'El usuario ya cuenta con una compra realizada.',
         'codigoError' => 'El código  de descuento es Incorrecto.',
        'coloquio' => 'El coloquio ya no se encuentra disponible.',
    ),
    'password_reset' => 'Recuperar Contraseña',
    'donativo' => array(
        'correcta' => 'Guardado Correcto, proximamente enviaremos un recibo a su dirección'
        
    )
);

