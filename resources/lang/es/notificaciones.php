<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return[
    'villanueva' => [
        'prospecto' => [
            'subject' => 'Registro completado Universidad Villanueva Montaño Online',
            'view' => 'emails.villanueva.bienvenidaEmail',
            'from' => [
                'email' => 'info@villanuevamontano-online.com',
                'name' => 'Villanueva Montaño Online',
            ],
        ],
        'contacto' => [
            'subject' => 'Registro Villanueva Montaño Online',
            'view' => 'emails.villanueva.contatoRegistroEmail',
            'from' => [
                'email' => 'send@innovateca.com.mx',
                'name' => 'CRM Innovateca',
            ],
            'to' => [
                [
                    'email' => 'send@innovateca.com.mx',
                    'name' => 'CRM Innovateca'
                ]
            ]
            
        ],
    ],
];
