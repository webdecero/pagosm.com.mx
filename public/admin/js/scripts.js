jQuery(document).ready(function ($) {

    var $modalAlert = $("#modalAlert"),
            $modalAlertMsg = $modalAlert.find('.modal-body > p.mensaje'),
            $modalAlertError = $modalAlert.find('.modal-body > ul.error');






    $.ajaxSetup({
        headers: {
            '_token': $('input[meta="_token"]').val()
        }
    });

    var oLanguage = {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };


    /* Formatting function for row details - modify as you need */
    function format(d) {


        var table = '<table class="table .table-bordered" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

        $.each(d, function (key, field) {

            if (field !== null && typeof field === 'object')
            {
//                console.log(field);

                table += '<tr>' +
                        '<td colspan="2" style="text-align: center; font-weight: bold; ">' + key + '</td>' +
                        '</tr>';

            } else
            {
                table += '<tr>' +
                        '<td>' + key + '</td>' +
                        '<td>' + field + '</td>' +
                        '</tr>';

            }

        });
        if (d.status === 'descartado') {
            table += '<tr>' +
                    '<td colspan="2"><a href="#" class="btn btn-success btnRecuperarFoto" data-foto-id="' + d._id + '" data-foto-edad="' + d.categoriaEdad + '">Recuperar</a>"</td>' +
                    '</tr>';
        }
        table += '</table>';
        return table;
    }


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    if ($(".modalMensaje").length > 0) {
        $('.modalMensaje').modal('show');
    }
    if ($('[data-toggle="tooltip"]').length > 0) {
        $('[data-toggle="tooltip"]').tooltip();
    }


    var $modalBorro = $('#modalBorro');

    var modalBorrar = function (e) {

        var href = $(this).attr("href");

        e.preventDefault();

        $modalBorro.modal('show');

        var $btnAcept = $modalBorro.find('a.acept');

        $btnAcept.on('click', function (e) {

            window.location = href;

        });



    };




    if ($('.datatable').length > 0) {

//Datatable ignition

        var $table = $(".datatable");


        var url = $table.attr('data-url'),
                indexColums = [],
                fieldsColums = [],
                dataTh = $table.find('thead th');

        var orders = [];
        $.each(dataTh, function (key, item) {

            var name = $(item).attr('data-name');

            var details = $(item).attr('data-details');

            var order = $(item).attr('data-order');
            var orderable = ($(item).attr('data-orderable') === 'true') ? true : false;

            if (details)
            {

                fieldsColums.push({"data": "url_details"});

                indexColums.push({
                    "className": 'details-control text-center',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                }
                );

            } else if (name)
            {
                indexColums.push(
                        {
                            "data": name,
                            "orderable": orderable
                        }
                );

                fieldsColums.push({"data": name});

            }


            if (typeof order !== typeof undefined && order !== false)
            {

                orders.push([key, order]);
            }

        });

        var oTable = $table.DataTable({
            responsive: true,
            oLanguage: oLanguage,
            processing: true,
            serverSide: true,
            order: orders,
            ajax: {
                url: url,
                type: "POST",
                data: function (d) {

                    $("[type=search]").attr('name', 'query');

                    d.fields = fieldsColums;

                    $.each($(".reloadTable"), function (k, input) {

                        var name = $(this).attr('name');

                        if ($(this).attr('type') == 'checkbox')
                        {

                            if ($(this).is(':checked'))
                            {
                                d[name] = $(this).val();
                            }

                        } else
                        {
                            if ($(this).val())
                                d[name] = $(this).val();

                        }


                    });

                }
            },
            columns: indexColums,
            "rowCallback": function (row, data) {

                if (data.hasOwnProperty('url_details'))
                {

                    var i = jQuery('<i/>', {
                        class: 'fa fa-info-circle '
                    });
                    $(row).find('td.details-control').html(i);

                    $(row).attr('data-url-details', data.url_details);
                }

            },
            "drawCallback": function (settings) {
//        var api = this.api();
//        console.log( api.rows( {page:'current'} ).data() );


            }
        });

        $(".reloadTable").on('change', function () {


            oTable.ajax.reload();

        });

        // Add event listener for opening and closing details
        $table.on('click', 'td.details-control', function () {



            var tr = $(this).closest('tr');


            var row = oTable.row(tr);

            var url = tr.attr('data-url-details');

            if (url)
            {

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row

                    $.post(url, function (details) {

                        row.child(format(details)).show();

                        tr.addClass('shown');

                    });

                }

            }


        });

        $table.on('draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            if ($('a.borrar').length > 0) {

                $('a.borrar').on('click', modalBorrar);

            }


        });


        if ($('#getUserId').length > 0) {


            var $inputSearch = $('[name="query"]');
            $inputSearch.val($('#getUserId').val());
            $inputSearch.trigger($.Event("keyup", {keyCode: 13}));
        }

    }





//    if ($('#fechaInicio').length > 0) {
//
//
//        $("#fechaInicio").mask("99-99-9999", {placeholder: "dd-mm-aaaa"});
//        $("#fechaFinal").mask("99-99-9999", {placeholder: "dd-mm-aaaa"});
//
//
//    }


    if ($('.fechaHoraMask').length > 0) {

        $('.fechaHoraMask').mask('00-00-0000 00:00', {
            clearIfNotMatch: true,
            placeholder: "DD-MM-AAAA HH:MM"
        });



    }



    if ($('.horasMinutoMask').length > 0) {

        $('.horasMinutoMask').mask('00:00', {
            clearIfNotMatch: true,
            placeholder: "HH:MM"
        });



    }




    if ($('textarea').length > 0) {

        tinymce.init({
            selector: 'textarea',
            extended_valid_elements: '#i[class],span[class|style]',
            forced_root_block: false,
            height: 250,
            plugins: [
                'textcolor',
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
            visualblocks_default_state: true,
            content_css: [
//                '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                '//www.tinymce.com/css/codepen.min.css',
//                '//innovateca.com.mx/admin/dist/css/entypoFont.css'

            ],
            init_instance_callback: function (editor) {
                editor.on('Change', function (e) {

                    editor.save();
                    
                    if ($('form.validate').length > 0) {
                        
                        $("form.validate").valid();
                    }

                });
            }
        });





    }



    if ($('#tags').length > 0) {



        function split(val) {
            return val.split(/,\s*/);
        }
        function extractLast(term) {
            return split(term).pop();
        }

        var tags = $("#tags"),
                service = tags.attr("data-service-tags");


        var cacheTags = {};


        tags.bind("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        }).autocomplete({
            minLength: 0,
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
//                        response($.ui.autocomplete.filter(
//                                nivelesEscolares, extractLast(request.term)));


                var term = request.term;
                if (term in cacheTags) {
                    response(cacheTags[ term ]);
                    return false;
                }

                $.getJSON(service, request, function (data, status, xhr) {
                    cacheTags[ term ] = data;
                    response(data);
                });



            },
            change: function (ev, ui) {
//                        if (!ui.item)
//                            $(this).val("").addClass("error");
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join(", ");
                return false;
            }
        });



    }


    if ($('#categoria').length > 0) {


        var categoria = $("#categoria"),
                serviceCategoria = categoria.attr("data-service-categorias");


        var cacheCategorias = {};


        categoria.bind("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        }).autocomplete({
            minLength: 0,
            source: function (request, response) {


                var term = request.term;
                if (term in cacheCategorias) {
                    response(cacheCategorias[ term ]);
                    return false;
                }

                $.getJSON(serviceCategoria, request, function (data, status, xhr) {
                    cacheCategorias[ term ] = data;
                    response(data);
                });



            }
        });



    }



    if ($('form.validate').length > 0) {



        $("form.validate").each(function () {
            $(this).validate({
                ignore: ".ignore",
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
//                    $(element.form).find("label[for=" + element.id + "]")
//                            .addClass(errorClass);


                    var elParent = $(element).parent('.form-group');

                    elParent.addClass('has-error has-feedback');

                    var glypicon = elParent.find('span.glyphicon.glyphicon-remove');

                    if (glypicon.length === 0) {

                        elParent.append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');

                    }


                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
//                    $(element.form).find("label[for=" + element.id + "]")
//                            .removeClass(errorClass);

                    var elParent = $(element).parent('.form-group');
                    elParent.removeClass('has-error has-feedback');

                    var glypicon = elParent.find('span.glyphicon.glyphicon-remove');
                    if (glypicon.length > 0) {

                        glypicon.remove();
                    }



                },
                submitHandler: function (form) {


//                    console.debug(tinymce.activeEditor);

                    form.submit();
                }

            });
        });

    }




    if ($('input[maxlength]').length > 0) {

        $("input[maxlength]").each(function () {
            var el = $(this);
            var maxlength = el.attr('maxlength');

            el.next('.contador').find('.total').html(el.val().length);
            el.next('.contador').find('.hasta').html(maxlength);



            $(el).keyup(function () {

                el.next('.contador').find('.total').html(el.val().length);




            });
        });




    }




    if ($("#imagen").length > 0) {

        var image = document.getElementById('imageCrop');
        var cropper = null;


        var minWidth = $("#cropWidth").attr('min');
        var minHeight = $("#cropHeight").attr('min');

        var aspectRatio = 1 / 1;

        if ($("#aspectRatio").length > 0) {
            aspectRatio = eval($("#aspectRatio").val());

        }


        $("#imagen").on("change", function () {


            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader)
                return;
            if (/^image/.test(files[0].type)) {


                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function () {


                    if (cropper === null) {

                        cropper = new Cropper(image, {
                            aspectRatio: aspectRatio,
                            viewMode: 1,
//                            movable: false,
//                            zoomable: false,
//                            cropBoxResizable: false,
                            minCropBoxWidth: minWidth,
                            minCropBoxHeight: minHeight,
                            crop: function (data) {

                                $('#cropX').val(data.detail.x);
                                $('#cropY').val(data.detail.y);
                                $('#cropWidth').val(data.detail.width);
                                $('#cropHeight').val(data.detail.height);

                            }
                        });


                    }

                    cropper.replace(this.result);



                };

            }
        });

    }





    if ($(".lunchImageModal").length > 0) {

        var btnData = {};

        $(document.body).on("click", '.lunchImageModal', function (event) {

            btnData = $(this).data();

            if (!btnData.aspectRatio) {
                btnData.aspectRatio = 1 / 1;
            } else {
                btnData.aspectRatio = eval(btnData.aspectRatio);
            }

            if (!btnData.path) {
                btnData.path = 'innovateca/img';
            }
            if (!btnData.format) {
                btnData.format = 'jpg';
            }
            if (!btnData.quality) {
                btnData.quality = 80;
            } else {
                btnData.quality = eval(btnData.quality);
            }
            if (!btnData.prefix) {
                btnData.prefix = '';
            }


            btnData.hiddenImagePath = $(this).parents('.campoInput').find('.hiddenImagePath');
            btnData.fileImageUploadPreview = $(this).parents('.campoInput').find('.fileImageUploadPreview > img');

        });


//        Modal image crop

        var imageModalCrop = $("#imageModalCrop");

        var imageModalCropPreview = imageModalCrop.find('#imageModalCropPreview')[0];

        var imageModalCropFile = imageModalCrop.find("#imageModalCropFile");

        var imageCropW = imageModalCrop.find(".imageCropW");
        var imageCropH = imageModalCrop.find(".imageCropH");
        var blockError = imageModalCrop.find(".blockError");

        var updloadModalImage = imageModalCrop.find("#updloadModalImage");


        var btndismiss = imageModalCrop.find("button[data-dismiss='modal']");






        var cropperObj = null;


        imageModalCrop.on('shown.bs.modal', function () {

            cropperObj = new Cropper(imageModalCropPreview, {
                aspectRatio: btnData.aspectRatio,
                viewMode: 1,
                minCropBoxWidth: btnData.minWidth,
                minCropBoxHeight: btnData.minHeight,
                crop: function (data) {

                    imageCropW.val(data.detail.width);
                    imageCropH.val(data.detail.height);
                }
            });

        }).on('hidden.bs.modal', function () {

            cropperObj.destroy();
        });


        imageModalCropFile.on("change", function () {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader)
                return;
            if (/^image/.test(files[0].type)) {


                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function () {

                    cropperObj.reset().replace(this.result);

                };

            }
        });


        updloadModalImage.on("click", function () {

            blockError.find('ul').html("");

            if (parseInt(btnData.minWidth) > parseInt(imageCropW.val())) {
                alert('El ancho es menor al minimo');
                return false;
            }

            if (parseInt(btnData.minHeight) > parseInt(imageCropH.val())) {
                alert('El alto es menor al minimo');
                return false;
            }


            var $btn = $(this).button('loading');
            cropperObj.disable();

            btndismiss.hide();




            cropperObj.getCroppedCanvas().toBlob(function (blob) {
                var formData = new FormData();

                formData.append('image', blob);
                formData.append('path', btnData.path);
                formData.append('format', btnData.format);
                formData.append('quality', btnData.quality);


                formData.append('w', btnData.minWidth);
                formData.append('h', btnData.minHeight);


                $.ajax(btnData.url, {
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false

                }).done(function (data) {

                    if (data.success === true) {

                        $(btnData.hiddenImagePath).val(data.data);
                        $(btnData.fileImageUploadPreview).attr('src', data.data);

                        imageModalCrop.modal('hide');

                    } else {

                        $.each(data.messages, function (index, value) {
                            blockError.find('ul').append("<li>" + value + "</li>");
                        });
                    }

                }).always(function () {

                    $btn.button('reset');
                    btndismiss.show();
                    cropperObj.enable();
                });
            });


        });


    }




    if ($("#sortable").length > 0) {

        var $sortableFiles = $("#sortable").sortable({
            placeholder: "ui-state-highlight"
        });
        $sortableFiles.disableSelection();



        $("#guardar-orden").on("click", function (event) {


            var $btn = $(this).button('loading');

            var sortedIds = $sortableFiles.sortable("toArray");

            var url = $(this).data('url');


//            console.debug(sortedIds);

            var param = {
                clave: $("#clave").val(),
                orden: sortedIds
            };



            $.post(url, param, function (data) {


//                console.debug(data);

                $modalAlertMsg.html('');
                $modalAlertError.html('');
                $modalAlertMsg.html(data.message);


                if (data.success === true) {


                    var $sortableItems = $("#sortable > li");

                    $.each($sortableItems, function (index, item) {

                        $(item).attr('id', index);
                    });


                } else {


                    $.each(data.errors, function (index, value) {


                        $modalAlertError.append("<li>" + value + "</li>");
                    });

                }



                $modalAlert.modal('show');


                $btn.button('reset');

            }, "json");


        });
    }

    if ($(".addInput").length > 0) {

        $(".addInput").on("click", function (event) {

            var campoInput = $(this).next('.campoInput').clone();



            var $btnBorrar = $("<div class='col-md-1'>\n\
                                <button class='btn btn-warning btn-sm removeCampoInput' type='button'>\n\
                                    <span aria-hidden='true' class='glyphicon glyphicon-remove-circle'></span>\n\
                                </button>\n\
                            </div>");

            campoInput.append($btnBorrar);

            campoInput.find(':input').each(function () {
                $(this).val('');
            });

            $(this).parent().append(campoInput);


        });


        $(document.body).on("click", ".removeCampoInput", function (event) {

            var campoInput = $(event.currentTarget).parents('.campoInput');
            campoInput.remove();
        });


    }

    $(document.body).on("change", ".previewIcono", function (event) {
        var el = $(event.currentTarget);

        var icon = el.parent('div').prev('.icon-preview').find('i');

        icon.removeClass();

        icon.addClass(el.val());


    });

    $(document.body).on("change", ".fileImageUpload", function (event) {
        var el = $(event.currentTarget);

        var imagenPath = el.parent('div').prev('.fileImageUploadPreview').find(":input[type='hidden']");
        var imagenPreview = el.parent('div').prev('.fileImageUploadPreview').find("img");


        var f = event.currentTarget;

        var files = !!f.files ? f.files : [];

        if (!files.length || !window.FileReader)
            return;
        if (/^image/.test(files[0].type)) {


            var reader = new FileReader();
            reader.readAsDataURL(files[0]);

            reader.onloadend = function (e) {

                imagenPreview.attr('src', e.target.result);
                imagenPath.val('');



            };

        }


    });



    if ($("#select-evento-id").length > 0) {


        var borrarSelect = function (el) {

            el.find('option')
                    .remove()
                    .end()
                    .append(
                            $('<option>', {
                                value: '',
                                text: 'Ninguno'
                            })
                            );

        };



        $("#select-evento-id").on("change", function (event) {
            var el = $(this);

            var tituloSelect = $('#select-titulo-id');
            var action = $('#descuentoFiltroAction').val();
            var data = {
                idEvento: el.val()
            };



            $.ajax({
                method: "POST",
                url: action,
                data: data,
                beforeSend: function (xhr) {




                }

            }).done(function (data) {

                borrarSelect(tituloSelect);



                if (data.success) {

                    $.each(data.data, function (i, item) {
                        tituloSelect.append($('<option>', {
                            value: item.titulo,
                            text: item.titulo
                        }));
                    });


                } else {



                }


            }).always(function (data) {

                $('#select-titulo-id').val("").change();
            });



        });


    }











}); /* end of as page load scripts */