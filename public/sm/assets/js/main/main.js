/* --------------------------------------------
 MAIN FUNCTION
 -------------------------------------------- */
$(document).ready(function () {

	/* --------------------------------------------------------
	 ANIMATED PAGE ON REVEALED
	 ----------------------------------------------------------- */
	$(function ($) {
		"use strict";
		$('.animated').appear(function () {
			var elem = $(this);
			var animation = elem.data('animation');
			if (!elem.hasClass('visible')) {
				var animationDelay = elem.data('animation-delay');
				if (animationDelay) {

					setTimeout(function () {
						elem.addClass(animation + " visible");
					}, animationDelay);
				} else {
					elem.addClass(animation + " visible");
				}
			}
		});
	});
	/* --------------------------------------------
	 CLOSE COLLAPSE MENU ON MOBILE VIEW EXCEPT DROPDOWN
	 -------------------------------------------- */
	$(function () {
		"use strict";
		$('.navbar-collapse ul li a:not(.dropdown-toggle)').on('click', function (event) {
			$('.navbar-toggle:visible').click();
		});
	});
	/* --------------------------------------------
	 STICKY SETTING
	 -------------------------------------------- */
	$(function () {
		"use strict";
		if ($(".navbar-sticky").length > 0) {
			$(".navbar-sticky").sticky({topSpacing: 0});
			$(".navbar-sticky").css('z-index', '100');
			$(".navbar-sticky").addClass('bg-light');
			$(".navbar-sticky").addClass("top-nav-collapse");
		}
		;
	});
	/* --------------------------------------------------------
	 ANIMATED SCROLL PAGE WITH ACTIVE MENU - BOOTSTRAP SROLLSPY
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		$(".navbar-op ul li a[data-target], .navbar-op a.navbar-brand, .intro-direction a").on('click', function (event) {
			event.preventDefault();
			var hash = this.hash;

			if (hash) {


				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 900, function () {
					window.location.hash = hash;
				});
			}


		});
	});
	/* --------------------------------------------------------
	 NAVBAR FIXED TOP ON SCROLL
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($(".navbar-standart").length > 0) {
			$(".navbar-pasific").addClass("top-nav-collapse");
		} else {
			$(window).scroll(function () {
				if ($(".navbar").offset().top > 10) {
					$(".navbar-pasific").addClass("top-nav-collapse");
				} else {
					$(".navbar-pasific").removeClass("top-nav-collapse");
				}
			});
		}
		;
	});
	/* --------------------------------------------------------
	 BOOTSTRAP TOGGLE TOOLTIP
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		$('[data-toggle="tooltip"]').tooltip();
	});
	/* --------------------------------------------------------
	 TEAM HOVER
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		$('.team-seven').hover(
				function () {
					var overlay = $(this).find('.team-seven-overlay');
					overlay.removeClass(overlay.data('return')).addClass(overlay.data('hover'));
				},
				function () {
					var overlay = $(this).find('.team-seven-overlay');
					overlay.removeClass(overlay.data('hover')).addClass(overlay.data('return'));
				}
		);
	});
	/* --------------------------------------------------------
	 ISOTOPE MASONRY GRID
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		var $portfolioMasonryOne = $('.portfolio-masonry-one').isotope({
			itemSelector: '.portfolio-masonry-one-item',
			masonry: {
				columnWidth: 180,
				gutter: 10
			}
		});
		var $portfolioMasonryOneFullwidth = $('.portfolio-masonry-one-fullwidth').isotope({
			itemSelector: '.portfolio-masonry-one-item',
			masonry: {
				columnWidth: 180,
				gutter: 10
			}
		});
		var $portfolioMasonryTwo = $('.portfolio-masonry-two').isotope({
			itemSelector: '.portfolio-masonry-two-item',
			masonry: {
				columnWidth: 250,
				gutter: 10
			}
		});
		var $portfolioMasonryTwoFullwidth = $('.portfolio-masonry-two-fullwidth').isotope({
			itemSelector: '.portfolio-masonry-two-item',
			masonry: {
				columnWidth: 250,
				gutter: 10
			}
		});
		var $portfolio = $('.portfolio').isotope({
			itemSelector: '.portfolio-item',
			masonry: {
				rowHeight: 280
			}
		});
		var $blogMasonry6col = $('.blog-masonry-6col').isotope({
			itemSelector: '.blog-masonry-item',
			masonry: {
				columnWidth: '.col-lg-2',
				gutter: 0
			}
		});
		var $blogMasonry4Col = $('.blog-masonry-4col').isotope({
			itemSelector: '.blog-masonry-item',
			masonry: {
				columnWidth: '.col-md-3',
				gutter: 0
			}
		});
		var $blogMasonry3Col = $('.blog-masonry-3col').isotope({
			itemSelector: '.blog-masonry-item',
			masonry: {
				columnWidth: '.col-md-4',
				gutter: 0
			}
		});
		var $blogMasonry2Col = $('.blog-masonry-2col').isotope({
			itemSelector: '.blog-masonry-item',
			masonry: {
				columnWidth: '.col-md-6',
				gutter: 0
			}
		});
		$('ul.filters li a').on('click', function () {
			var filterValue = $(this).attr('data-filter');
			$portfolioMasonryOne.isotope({filter: filterValue});
			$portfolioMasonryOneFullwidth.isotope({filter: filterValue});
			$portfolioMasonryTwo.isotope({filter: filterValue});
			$portfolioMasonryTwoFullwidth.isotope({filter: filterValue});
			$portfolio.isotope({filter: filterValue});
		});
		$('ul.filters li a').on('click', function () {
			$('ul.filters li a').removeClass('active');
			$(this).addClass('active');
		});
	});
	/* --------------------------------------------------------
	 COUNT TO
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		$(".fact-number").appear(function () {
			var dataperc = $(this).attr('data-perc');
			$(this).each(function () {
				$(this).find('.factor').delay(6000).countTo({
					from: 10,
					to: dataperc,
					speed: 3000,
					refreshInterval: 50,
				});
			});
		});
	});
	/* --------------------------------------------------------
	 OWL CAROUSEL FOR TESTIMONIAL
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		var owlSectionOneItem = $("#owlSectionOneItem");
		owlSectionOneItem.owlCarousel({
			autoPlay: 5000,
			items: 1,
			itemsDesktop: [1000, 1],
			itemsDesktopSmall: [900, 1],
			itemsTablet: [600, 1],
			itemsMobile: false
		});
		var owlSectionTwoItem = $("#owlSectionTwoItem");
		owlSectionTwoItem.owlCarousel({
			autoPlay: 5000,
			items: 2,
			itemsDesktop: [1000, 2],
			itemsDesktopSmall: [900, 2],
			itemsTablet: [600, 2],
			itemsMobile: false
		});
		var owlSectionThreeItem = $("#owlSectionThreeItem");
		owlSectionThreeItem.owlCarousel({
			autoPlay: 5000,
			items: 3,
			itemsDesktop: [1000, 3],
			itemsDesktopSmall: [900, 3],
			itemsTablet: [600, 1],
			itemsMobile: false
		});
		var owlSectionFourItem = $("#owlSectionFourItem");
		owlSectionFourItem.owlCarousel({
			autoPlay: 5000,
			items: 4,
			itemsDesktop: [1000, 4],
			itemsDesktopSmall: [900, 2],
			itemsTablet: [600, 2],
			itemsMobile: false
		});
		var owlSectionFiveItem = $("#owlSectionFiveItem");
		owlSectionFiveItem.owlCarousel({
			autoPlay: 5000,
			items: 5,
			itemsDesktop: [1000, 5],
			itemsDesktopSmall: [900, 3],
			itemsTablet: [600, 3],
			itemsMobile: false
		});
		var owlSectionSixItem = $("#owlSectionSixItem");
		owlSectionSixItem.owlCarousel({
			autoPlay: 5000,
			items: 6,
			itemsDesktop: [1000, 6],
			itemsDesktopSmall: [900, 3],
			itemsTablet: [600, 3],
			itemsMobile: false
		});
	});
	/* --------------------------------------------------------
	 OWL CAROUSEL FOR SHOP
	 -----------------------------------------------------------  */
	$(function () {
		"use strict";
		var owlShop = $("#owlShop");
		owlShop.owlCarousel({
			slideSpeed: 1000,
			autoPlay: true,
			pagination: false,
			items: 4,
		});
		$(".shop-control-next").on('click', function () {
			owlShop.trigger('owl.next');
		})
		$(".shop-control-prev").on('click', function () {
			owlShop.trigger('owl.prev');
		});
	});
	/* --------------------------------------------------------
	 PAGE LOADER
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		$(".loader-item").delay(700).fadeOut();
		$("#pageloader").delay(800).fadeOut("slow");
	});
	/* --------------------------------------------------------
	 JQUERY TYPED
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($("#typed").length > 0) {
			$("#typed").typed({
				stringsElement: $('#typed-strings'),
				startDelay: 2000,
				typeSpeed: 50,
				backDelay: 500,
				loop: true,
				contentType: 'html',
				loopCount: false,
			});
		}
		;
	});
	/* --------------------------------------------------------
	 JQUERY TYPED#2
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($("#typed-2").length > 0) {
			$("#typed-2").typed({
				stringsElement: $('#typed-strings-2'),
				startDelay: 100,
				typeSpeed: 50,
				backDelay: 500,
				loop: false,
				contentType: 'html',
			});
		}
		;
	});
	/* --------------------------------------------------------
	 JQUERY TEXTILLATE
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($(".tlt").length > 0) {
			$('.tlt').textillate({
				loop: true,
				minDisplayTime: 5000,
				out: {
					effect: 'hinge',
					delayScale: 1.5,
					delay: 50,
					sync: false,
					shuffle: false,
					reverse: false,
					callback: function () {}
				},
			});
		}
		;
	});
	/* --------------------------------------------------------
	 JQUERY ROTATE
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($(".rotate").length > 0) {
			$(".rotate").textrotator({
				animation: "fade",
				separator: ",",
				speed: 2000
			});
		}
		;
	});
	/* --------------------------------------------------------
	 SHOP RANGE SLIDER
	 ----------------------------------------------------------- */
	$(function () {
		"use strict";
		if ($("#shop-range-price").length > 0) {
			var $rangePrice = $("#shop-range-price");
			$rangePrice.ionRangeSlider({
				type: "double",
				grid: true,
				min: 0,
				max: 250,
				from: 45,
				to: 200,
				prefix: "$"
			});
		}
		;
	});





	/* --------------------------------------------------------
	 CUSTOM JS
	 -----------------------------------------------------------  */
	$(function () {
		"use strict";


		if ($("#form-registro").length > 0) {

			$("#form-registro").validate({
				rules: {
					nombre: {
						required: true,
						minlength: 2
					},
					apellidoPaterno: {
						required: true,
						minlength: 2
					},
					email: {
						required: true,
						email: true
					},
					password: {
						required: true,
						minlength: 6
					},
					passwordConfirmar: {
						required: true,
						equalTo: "#password"
					}


				}
//                errorPlacement: function (error, element) {
//                    error.insertBefore(element);
//                }
			});
			var carouselRegistro = $("#carousel-registro");
			carouselRegistro.carousel({
				interval: false,
				wrap: false,
				pause: false,
				keyboard: false
			});



			var validarSlide = function (slideSelector) {


				var slideAvanzar = true;
				var slide = $(slideSelector);
				var inputs = slide.find("input, select, textarea");
				$.each(inputs, function (key, input) {

					if (!$(input).valid()) {
						slideAvanzar = false;
					}
				});
				return slideAvanzar;
			};
			carouselRegistro.find(".siguiente").on('click', function () {

//                carouselRegistro.carousel('next');
				var slide = $(this).parents(".item");

				if (validarSlide(slide) === true) {
					carouselRegistro.carousel('next');
				}


			});
			carouselRegistro.find(".anterior").on('click', function () {

				carouselRegistro.carousel('prev');
//                var slide = $(this).parents(".item");
//
//                if (validarSlide(slide) === true) {
//                    carouselRegistro.carousel('prev');
//                }


			});
			$('#fechaNacimiento').mask('D0-M0-0000', {'translation': {
					D: {pattern: /[0-3]/},
					M: {pattern: /[0-1]/}


				},
//                reverse: true
				clearIfNotMatch: true,
				placeholder: "DD-MM-AAAA"
			});
			$('.codigoPostal').mask('00009', {
//                clearIfNotMatch: true,
				placeholder: "Código Postal"
			});


			$("select#pais")
					.change(function () {

						var inputPais = $(this);




						var inputs = $("select[name='estado']");




						var opt = inputPais.find("option:selected");
						var inputPostal = $("#codigoPostal");
						var inputCiudad = $("#ciudad");



						if (opt.text() === "México") {



							inputCiudad.rules("add", {
								required: true,
								minlength: 2
							});

							inputPostal.rules("add", {
								required: true,
								minlength: 2
							});


							inputs.prop("disabled", false);

						} else {


							inputCiudad.rules("remove");
							inputPostal.rules("remove");
							inputs.prop("disabled", true);

						}

					})
					.trigger("change");



			$("select#areaDesempeno")
					.change(function () {

						var areaDesempeno = $(this);


						var arr = ['Docente', 'Director escolar', 'Coordinador académico', 'Institución Religiosa'];


						var opt = areaDesempeno.find("option:selected");


						var claveTrabajoBox = $("#claveTrabajoBox");
						var nivelEscolarBox = $("#nivelEscolarBox");
						var especificaBox = $("#especificaBox");
						var cenepNo = $("#cenepNo");



						if (jQuery.inArray(opt.text(), arr) >= 0) {


							claveTrabajoBox.show();
							nivelEscolarBox.show();

							claveTrabajoBox.find('input').rules("add", {
								required: true,
								minlength: 2
							});

							especificaBox.hide().find('input').val('');



							nivelEscolarBox.find('select')
									.change(function () {

										var optSub = $(this).find("option:selected");


										if (optSub.text() === 'Otro') {




											especificaBox.show();


										} else {
											especificaBox.hide().find('input').val('');
										}



									}).trigger("change");







						} else if (opt.text() === 'Otro') {



							claveTrabajoBox.hide().find('input').val('');
							nivelEscolarBox.hide().find('select').val('');

							claveTrabajoBox.find('input').rules("remove");


							especificaBox.show();


						} else {


							claveTrabajoBox.hide().find('input').val('');
							nivelEscolarBox.hide().find('select').val('');

							claveTrabajoBox.find('input').rules("remove");

							especificaBox.hide().find('input').val('');
							$("#cenepNo").prop("checked", true);
						}





					})
					.trigger("change");






		}//If form registro



		if ($("#form-login").length > 0) {


			$("#form-login").validate({
				rules: {
					email: {
						required: true,
						email: true
					},
					password: {
						required: true,
						minlength: 6
					}


				}
			});

		}//If form login



		/*
		 
		 if ($("#form-donativo").length > 0) {
		 
		 
		 $("#form-donativo").validate();
		 
		 
		 
		 
		 $("select#paisDonativo")
		 .change(function () {
		 
		 var inputPais = $(this);
		 
		 
		 var inputs = $("select[name='estadoDonativo']");
		 
		 
		 var opt = inputPais.find("option:selected");
		 
		 var inputPostal = $("#codigoPostalDonativo");
		 var inputCiudad = $("#ciudadDonativo");
		 
		 
		 if (opt.text() === "México") {
		 
		 
		 inputCiudad.rules("add", {
		 required: true,
		 minlength: 2
		 });
		 
		 inputPostal.rules("add", {
		 required: true,
		 minlength: 2
		 });
		 
		 
		 inputs.prop("disabled", false);
		 
		 } else {
		 
		 
		 inputCiudad.rules("remove");
		 inputPostal.rules("remove");
		 
		 
		 inputs.prop("disabled", true);
		 
		 }
		 
		 })
		 .trigger("change");
		 
		 
		 }
		 
		 */
		//If form Donativo



		/*
		 
		 $("[name='donativo']").change(function () {
		 var donativo = $("[name='donativo']:checked");
		 
		 
		 var obligatorios = [
		 "input[name='nombreDonativo']",
		 "input[name='razonSocialDonativo']",
		 "input[name='emailDonativo']",
		 "input[name='rfcDonativo']",
		 "input[name='telefonoDonativo']",
		 "input[name='direccionDonativo']",
		 "input[name='numExtDonativo']",
		 "input[name='coloniaDonativo']",
		 "input[name='delegacionDonativo']",
		 "input[name='ciudadDonativo']",
		 "input[name='codigoPostalDonativo']"
		 ].join(", ");
		 
		 
		 var grupoDonativo = $(obligatorios);
		 
		 
		 
		 if (donativo.val() === "true") {
		 
		 $("#donativoBox").show();
		 $(".si").show();
		 $(".no").hide();
		 
		 
		 grupoDonativo.each(function () {
		 $(this).rules("add", {
		 required: true,
		 minlength: 2
		 });
		 });
		 
		 
		 
		 } else {
		 
		 $("#donativoBox").hide();
		 $(".si").hide();
		 $(".no").show();
		 
		 grupoDonativo.each(function () {
		 $(this).rules("remove");
		 });
		 
		 }
		 
		 
		 }).trigger("change");
		 
		 */





		if ($("#form-comprar").length > 0) {

			jQuery.validator.addMethod("alphanumeric", function (value, element) {
				return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
			}, "Solo valores alphanumericos");



			$("#form-comprar").validate({
				rules: {
					codigoDescuento: {
						minlength: 6,
						maxlength: 6,
						alphanumeric: true
					},
					'coloquios[]': {
						required: true
					}


				}
			});



					var donativoCheck =  $("#donativoCheck");
					var paisDonativo =  $("select#paisDonativo");
					
					
			


			donativoCheck
					.change(function () {


						var donativoBox = $("#donativoBox");


						if (donativoCheck.is(':checked')) {

							donativoBox.show();
						} else {


							donativoBox.hide();
							donativoBox.find('input, select').val('');
							
							
						}



					})
					.trigger("change");

			paisDonativo
					.change(function () {

						var inputPais = $(this);


						var inputs = $("select[name='estadoDonativo']");


						var opt = inputPais.find("option:selected");

						var inputPostal = $("#codigoPostalDonativo");
						var inputCiudad = $("#ciudadDonativo");


						if (opt.text() === "México") {


							inputCiudad.rules("add", {
								required: true,
								minlength: 2
							});

							inputPostal.rules("add", {
								required: true,
								minlength: 2
							});


							inputs.prop("disabled", false);

						} else {


							inputCiudad.rules("remove");
							inputPostal.rules("remove");


							inputs.prop("disabled", true);

						}

					})
					.trigger("change");





		}//If form comprar



		if ($("#form-tarjeta").length > 0) {

			$('#nombre-tarjeta').mask('ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ', {
				translation: {
					'Z': {
						pattern: /[ñÑa-zA-Z\s]/, optional: true
					}
				}

			});

			$('#cvv-tarjeta').mask('000', {
				clearIfNotMatch: true,
				onComplete: function (cep) {


					if (cep === '000') {

						$('#cvv-tarjeta').val('');

					}

				}

			});

			$('#numero-tarjeta').mask('0000-0000-0000-0000', {
				placeholder: "xxxx-xxxx-xxxx-xxxx"

			});

		}






		if ($(".modalMensaje").length > 0) {

			$('.modalMensaje').modal('show');
		}


		if ($(".errorLogin").length > 0) {

			$('#loginModal').modal('show');
		}

		if ($(".errorRecovery").length > 0) {

			$('#recuperarModal').modal('show');
		}






	});




}(jQuery));