Cambios 09 Octubre 2017

    [OK] Centrar los títulos y poner en botón el menú de selección de eventos. (menu masonry)
    [OK] Dar más espacio entre cada una de las imágenes de eventos, a un 20%

    [OK] Cambiar el tipo de letra, resaltando más el nombre del evento y con menor tamaño la fecha del mismo.
    [OK] Revisar el interlineado de texto
    
    El orden de la información es:

    [OK] Nombre del evento (Probar con h4 y negrita)
    [OK] Fecha (Probar con h3)
    [OK] Ubicación (Probar con h3)

    [OK] El botón ubicarlo al centro y conservar su tamaño.

    [¿?] Sección de banner no aparece, se agregara en el dashboard como se acordó.
    [¿?] Al dar clic en comprar un evento, me solicitó que me logueara. Inicié sesión y me regresó a la oferta formativa, pero la solicitud era que me regresara al evento que ya estaba comprando.
    [¿?] La casilla del comprobante fiscal deberá permanecer deshabilitada y el cliente será el que habilite esta opción.
    [¿?] ¿Podemos hacer algo para distinguir el texto del evento con el evento de abajo? Se ve muy intuitivo, pero luego los profes son un poco complicados.

@extends('sm.global.base')


@section('contenido')





<!-- Intro Area
===================================== -->        

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <!--        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="sm/img/SIEI_10_carrusel.jpg" alt="...">
            <div class="carousel-caption">
                ...
            </div>
        </div>

        <!-- 
        <div class="item">
          <img src="..." alt="...">
          <div class="carousel-caption">
            ...
          </div>
        </div> 
        -->
    </div>
</div>

<!-- Grid Masonry-->
<div id="portfolioGrid" class="bg-gray pt30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="filters text-center  mt25 mb50">
                    <li><a class="active" data-filter="*">Todos los eventos</a></li>
                    @foreach ($categorias as $categoria )
                    <li><a class="" data-filter=".{{str_slug($categoria['categoria'], '-')}}">{{$categoria['categoria']}}</a></li>
                    @endforeach
                </ul>

                <div class="portfolio center-block">
                    @foreach ($eventos as  $key => $evento )

                    <div class="portfolio-item col-md-4 col-sm-12 col-xs-12 {{str_slug($evento->categoria, '-')}}" data-category="" style=" padding: 0 15px;height: 680px">
                        <img src="{{ $evento->imagen }}" alt="{{ $evento->tituloLargo }}" class="img-responsive center-block" style="height: auto !important">
                        <h3 class="text-center strong" style="height: 90px;">
                            {{ $evento->tituloLargo }}
                        </h3>
                        @if($evento->fechaInicioFormato('d') == $evento->fechaFinalFormato('d'))

                        <h4>
                            {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                        </h4>

                        @else

                        <h4>
                            {{ $evento->fechaInicioFormato('d') }} al {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                        </h4>
                        @endif
                        
                        <h4 class="color-primary"> {{ $evento->ubicacion }}</h4>
                        
                        <p class="text-center">
                            <a class="button-o button-sm button-primary hover-fade" href="{{ route('page.comprar',['id'=> $evento->id]) }}">Comprar $ {{ $evento->precioDescuento }} {{ $evento->moneda }}</a>
                        </p>
                    </div>

                    @endforeach

                </div>

            </div>
        </div>
    </div>
</div>

<!-- END Grid Masonry-->

<!-- Service Area
===================================== -->
<!--    <div id="eventos" class="pt75 pb25">

        <div class="container">

             title and short description start 
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="font-size-normal">
                        <small> Service</small>
                        Oferta formativa de Grupo SM
                        <small class="heading heading-solid center-block"></small>
                    </h1>
                </div>

                <div class="col-md-8 col-md-offset-2 text-center">
                    <p class="lead">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt voluptatem. 
                    </p>
                </div>
            </div>

             title and short description end 





            @foreach ($eventos as  $key => $evento )



            @if($key %  2 == 0)





             service one start 
            <div class="col-lg-3 mt75 mb35">
                <img src="{{ $evento->imagen }}" alt="{{ $evento->tituloLargo }}" class="img-responsive center-block">
                <h3 class="font-size-normal">
                    <small class="color-primary"> {{ $evento->ubicacion }}</small>
                    {{ $evento->tituloLargo }}
                </h3>

                @if($evento->fechaInicioFormato('d') == $evento->fechaFinalFormato('d'))

                <h4>
                    {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                </h4>

                @else

                <h4>
                    {{ $evento->fechaInicioFormato('d') }} al {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                </h4>
                @endif


                <p class="mt20">
                    {!! $evento->descripcionCorta !!}
                </p>


                 Go to www.addthis.com/dashboard to customize your tools 
                <div class="addthis_native_toolbox mb30"></div>






                <span class="shop-item-price pull-right">$ {{ $evento->precioDescuento }} {{ $evento->moneda }}</span>

                <p>
                    <a href="{{ route('page.evento',['slug'=> $evento->slug]) }}" class="button-o button-sm button-primary hover-fade">Ver más</a>

                    <a href="{{ route('page.comprar',['id'=> $evento->id]) }}" class="button-o button-sm button-primary hover-fade">Comprar</a>


                </p>   
            </div>
             service one end 
            @else



             service two start 
            <div class="col-lg-3 mt100 mb35">
                <img src="{{ $evento->imagen }}" alt="{{ $evento->tituloLargo }}" class="img-responsive center-block">
                <h3 class="font-size-normal">
                    <small class="color-success">{{ $evento->ubicacion }}</small>
                    {{ $evento->tituloLargo }}
                </h3>

                @if($evento->fechaInicioFormato('d') == $evento->fechaFinalFormato('d'))

                <h4>
                    {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                </h4>

                @else

                <h4>
                    {{ $evento->fechaInicioFormato('d') }} al {{ $evento->fechaFinalFormato('d') }} de {{ $evento->fechaFinalFormato('F') }} de {{ $evento->fechaFinalFormato('Y') }}
                </h4>
                @endif





                <p class="mt20 animated" data-animation="fadeIn" data-animation-delay="100">

                    {!! $evento->descripcionCorta !!}
                </p>

                 Go to www.addthis.com/dashboard to customize your tools 
                <div class="addthis_native_toolbox mb30"></div>







                <span class="shop-item-price pull-right">${{ $evento->precioDescuento }} {{ $evento->moneda }}</span>



                <p>
                    <a href="{{ route('page.evento',['slug'=> $evento->slug]) }}" class="button-o button-sm button-success hover-fade">Ver más</a>

                    <a href="{{ route('page.comprar',['id'=> $evento->id]) }}" class="button-o button-sm button-success hover-fade">Comprar</a>


                </p> 
            </div>
             service two end 
            @endif

            @endforeach




        </div> container end 
    </div> section service end -->



<!-- Info Area
===================================== -->
<!--
<div id="info-1" class="pt50 pb50 mt75 parallax-window" data-parallax="scroll" data-speed="0.5" data-image-src="sm/assets/img/bg/img-bg-2.jpg">
        <div class="container">
                <div class="row pt75">
                        <div class="col-md-12 text-center">
                                <h1 class="color-light">
                                        <small class="color-light">The best way to be success</small>
                                        Are you ready to be success with us?
                                </h1>
                                <a class="button button-md button-pasific hover-ripple-out mt25">Start Project</a>
                                <a class="button-o button-md button-green hover-fade mt25"><span class="color-light">Contact Us</span></a>
                        </div>   
                </div>
        </div>
</div>
-->


<!-- Newsletter Area
        =====================================-->
<!--
<div id="newsletter" class="bg-dark2 pt50 pb50">
        <div class="container">
                <div class="row">                    
                        <div class="col-md-2">
                                <h4 class="color-light">
                                        Newsletter
                                </h4>
                        </div>

                        <div class="col-md-10">
                                <form name="newsletter">
                                        <div class="input-newsletter-container">
                                                <input type="text" name="email" class="input-newsletter" placeholder="enter your email address">
                                        </div>
                                        <a href="#" class="button button-sm button-pasific hover-ripple-out">Subscribe<i class="fa fa-envelope"></i></a>
                                </form>
                        </div>
                </div> row end 
        </div> container end 
</div>-->


<!-- section newsletter end -->




<!-- Contact Us Area
=====================================-->
<div id="contacto" class="pt100 pb100 bg-grad-stellar">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <div class="row">

                    <!-- title start -->
                    <div class="col-md-12 mb50">
                        <h1 class="font-size-normal color-light">
                            <small class="color-light">Contacto</small>
                            Póngase en contacto con nosotros
                        </h1>               
                        <!--<h5 class="color-light" >Colonia del Valle C.P. 03100, Delegación: Benito Juárez México, Ciudad de México</h5>-->                        
                    </div>
                    <!-- title end -->

                    <!-- contact info start -->
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <span class="icon-map color-light el-icon2x"></span>
                        <h5 class="color-light"><strong>Dirección</strong></h5>
                        <p class="color-light">Magdalena No. 211 Colonia del Valle C.P. 03100</p>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <span class="icon-megaphone color-light el-icon2x"></span>
                        <h5 class="color-light"><strong>Teléfono</strong></h5>
                        <p class="color-light">1087-8455</p>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <span class="icon-envelope color-light el-icon2x"></span>
                        <h5 class="color-light"><strong>Email</strong></h5>
                        <p class="color-light">contacto@seminariointernacional.com.mx</p>
                    </div>
                    <!-- contact info end -->

                </div><!-- row left end -->
            </div><!-- col left end -->

            <div class="col-md-6">
                <div class="contact contact-us-one">
                    <div class="col-sm-12 col-xs-12 text-center mb20">
                        <h4 class="pb25 bb-solid-1 text-uppercase">
                            Enviar Mensaje
                            <small class="text">Por favor complete la información y nos pondremos en contacto con usted.</small>
                        </h4>
                    </div>


                    <form action="{{ route('page.contaco.form')}}" method="POST" name="contact">

                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <!-- fullname start -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="input-md input-rounded form-control" name="nombre" placeholder="Nombre" maxlength="100" required>
                            </div>                                               
                        </div>
                        <!-- fullname end -->

                        <!-- email start -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="email" class="input-md input-rounded form-control" name="email" placeholder="Email " maxlength="100" required>
                            </div>                                               
                        </div>
                        <!-- email end -->

                        <!-- website start -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="input-md input-rounded form-control" name="telefono" placeholder="Teléfono" maxlength="100">
                            </div>                                               
                        </div>
                        <!-- website end -->

                        <!-- security start -->
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="input-md input-rounded form-control" name="seguridad"  placeholder="8 + 9 = ?" maxlength="100" required>
                            </div>                                               
                        </div>
                        <!-- security end -->

                        <!-- textarea start -->
                        <div class="col-sm-12">
                            <textarea class="form-control" name="mensaje" rows="7" required=""></textarea>
                        </div>
                        <!-- textarea end -->

                        <!-- button start -->
                        <div class="col-sm-12 mt10 text-center">
                            <button type="submit" class="button-3d button-md button-block button-pasific hover-ripple-out">Enviar</button>
                        </div>
                        <!-- button end -->

                    </form>    
                </div><!-- div contact end -->
            </div><!-- col end -->

        </div><!-- row end -->
    </div><!-- container end -->            
</div><!-- section contact end -->









@stop


@section('js-lib')

<!-- Magnific Popup
=====================================-->
<script src="sm/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="sm/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

<!-- Progress Bars
=====================================-->
<script src="sm/assets/js/progress-bar/bootstrap-progressbar.js"></script>
<script src="sm/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

<!-- JQuery Main
=====================================-->
<script src="sm/assets/js/main/jquery.appear.js"></script>
<script src="sm/assets/js/main/isotope.pkgd.min.js"></script>
<script src="sm/assets/js/main/parallax.min.js"></script>
<script src="sm/assets/js/main/jquery.countTo.js"></script>
<script src="sm/assets/js/main/owl.carousel.min.js"></script>
<!--<script src="sm/assets/js/main/jquery.sticky.js"></script>-->
<script src="sm/assets/js/main/ion.rangeSlider.min.js"></script>



@stop



